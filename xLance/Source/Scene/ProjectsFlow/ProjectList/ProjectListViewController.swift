//
//  ProjectListViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator

protocol ProjectListViewProtocol {}

class ProjectListViewController: BaseViewController, StoryboardLoadable, ProjectListViewProtocol {
    
    static var storyboardName: Storyboards = .projectList
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView?
    fileprivate var refreshControl = UIRefreshControl()
    
    // MARK: Injections
    var presenter: ProjectListPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    }

}

// MARK: - ProjectListPresenterOutput
extension ProjectListViewController: ProjectListPresenterOutput {
    
    func reloadData() {
        guard let tableView = tableView else { return }
        refreshControl.endRefreshing()
        tableView.reloadData()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .bottom, offset: 30)
        ])
    }
    
    func reloadCell(row: Int) {
        let indexPath = IndexPath(row: row, section: 0)
        tableView?.reloadRows(at: [indexPath], with: .automatic)
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ProjectListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ProjectTableViewCell.self, for: indexPath)
        let project = presenter.projects[indexPath.row]
        cell.selectionStyle = .none
        cell.project = project
        return cell
    }
    
    // inf load
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            presenter.fetchProjects()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(indexPath.row)
    }
    
}

// MARK: - Private
fileprivate extension ProjectListViewController {
    
    func setupUI() {
        navigationItem.title = "HEADER_COMPONENT.PROJECTS".localized
        refreshControl.alpha = 0
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView?.refreshControl = refreshControl
        tableView?.contentInset = .init(top: 8, left: 0, bottom: 0, right: 0)
        tableView?.register(cellType: ProjectTableViewCell.self)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        presenter.fetchIndexProjects()
    }
    
}
