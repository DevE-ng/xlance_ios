//
//  ProjectListConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProjectListConfigurable {
}

class ProjectListConfigurator<T: ProjectListViewController>: BaseViewConfigurable, ProjectListConfigurable {
    
    private let networkProvider: Networking
    private let coordinator: ProjectListCoordinatable
    
    required init(networkProvider: Networking, coordinator: ProjectListCoordinatable) {
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: ProjectListConfigurable
    
    func create() -> ProjectListViewController {
        let viewController = ProjectListViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: ProjectListViewController) {
        
        let interactor = ProjectListInteractor()
        let presenter = ProjectListPresenter(output: viewController)
        
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
