//
//  ProjectListPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProjectListCoordinatable {
    var onUpdateProject: ((ProjectModel) -> Void)? { get set }
    func didSelectProject(_ project: ProjectModel)
}

protocol ProjectListPresenterInput: BasePresenterInput {
    var projects: [ProjectModel] { get set }
    func fetchIndexProjects()
    func fetchProjects()
    func didSelectRow(_ row: Int)
}

protocol ProjectListPresenterOutput: BasePresenterOutput {
    func reloadData()
    func reloadCell(row: Int)
}

class ProjectListPresenter {
    
    var projects: [ProjectModel] = []
    private var currentPage = 0
    
    //MARK: Injections
    var interactor: ProjectListInteractorInput!
    var coordinator: ProjectListCoordinatable! {
        didSet {
            if self.coordinator == nil { return }
            self.coordinator.onUpdateProject = { [weak self] project in
                guard
                    let `self` = self,
                    let index = self.projects.indexForElement(project) else { return }
                self.projects[index] = project
                self.output?.reloadCell(row: index)
            }
        }
    }
    private weak var output: ProjectListPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: ProjectListPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - ProjectListPresenterInput
extension ProjectListPresenter: ProjectListPresenterInput {
    
    func viewDidLoad() {
        fetchProjects()
    }
    
    func fetchIndexProjects() {
        currentPage = 0
        projects = []
        fetchProjects()
    }
    
    func fetchProjects() {
        output?.startLoading()
        interactor.fetchProjects(index: currentPage, size: 5)
    }
    
    func didSelectRow(_ row: Int) {
        let project = projects[row]
        coordinator.didSelectProject(project)
    }
    
}

// MARK: - ProfileInteractorOutput
extension ProjectListPresenter: ProjectListInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successFetchProjects(_ projects: [ProjectModel]) {
        output?.stopLoading()
        if projects.isEmpty { return }
        self.projects += projects
        currentPage += 1
        output?.reloadData()
    }
    
}

// MARK: - Private
fileprivate extension ProjectListPresenter {}

