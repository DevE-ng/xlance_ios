//
//  ProjectListInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProjectListInteractorInput: BaseInteractorInput {
    func fetchProjects(index: Int, size: Int)
}
protocol ProjectListInteractorOutput: BaseInteractorOutput {
    func successFetchProjects(_ projects: [ProjectModel])
    func errorHandler(_ error: String)
}

class ProjectListInteractor {
    
    var networkProvider: Networking!
    weak var presenter: ProjectListInteractorOutput?
    
    private lazy var projectsWorker: ProjectsWorker = {
        return ProjectsWorkerImp(networkProvider: self.networkProvider)
    }()
    
}

// MARK: - ProjectListPresenterInput
extension ProjectListInteractor: ProjectListInteractorInput {
    
    func fetchProjects(index: Int, size: Int) {
        projectsWorker.fetchProjects(index: index, size: size) { [weak self] in
            self?.handleFetchProjects($0)
        }
    }
    
}

// MARK: - Private
fileprivate extension ProjectListInteractor {
    
    func handleFetchProjects(_ result: Result<[ProjectModel]>) {
        switch result {
            
        case .Success(let projects):
            presenter?.successFetchProjects(projects)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

