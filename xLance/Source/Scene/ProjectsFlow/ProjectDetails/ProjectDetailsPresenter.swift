//
//  ProjectDetailsPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProjectDetailsCoordinatable {
    var onUpdateProject: CompletionProjectBlock? { get set }
    func sendProposalHandler(_ project: ProjectModel)
    func editTermsHandler(_ suggestion: SuggestionModel)
    func viewConditionHandler(_ suggestion: SuggestionModel)
    func declineHandler(_ project: ProjectModel)
}

protocol ProjectDetailsPresenterInput: BasePresenterInput {
    var project: ProjectModel! { get set }
    func sendProposalHandler()
    func viewConditionHandler()
    func editTermsHandler()
    func declineHandler()
}

protocol ProjectDetailsPresenterOutput: BasePresenterOutput {
    func reloadData()
}

class ProjectDetailsPresenter {
    
    //MARK: Injections
    var project: ProjectModel!
    var interactor: ProjectDetailsInteractorInput!
    var coordinator: ProjectDetailsCoordinatable! {
        didSet {
            if self.coordinator == nil { return }
            self.coordinator.onUpdateProject = { [weak self] newProject in
                self?.project = newProject
                self?.output?.reloadData()
            }
        }
    }
    private weak var output: ProjectDetailsPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: ProjectDetailsPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - ProjectDetailsPresenterInput
extension ProjectDetailsPresenter: ProjectDetailsPresenterInput {
    
    func viewDidLoad() {
    }
    
    func sendProposalHandler() {
        output?.startLoading()
        interactor.sendProposal(project)
    }
    
    func editTermsHandler() {
        var suggestion = SuggestionModel(model: project, freelancer: CurrentUser.profile!)
        suggestion.prepareForEdited()
        coordinator.editTermsHandler(suggestion)
    }
    
    func viewConditionHandler() {
        guard let suggestion = project.suggestion else {
            return
        }
        output?.startLoading()
        interactor.fetchSuggestion(id: suggestion.id) { [weak self] in
            self?.output?.stopLoading()
            self?.coordinator.viewConditionHandler($0)
        }
    }
    
    func declineHandler() {
        guard let suggestion = project.suggestion else {
            return
        }
        output?.startLoading()
        interactor.declineSuggestion(id: suggestion.id)
    }
    
}

extension ProjectDetailsPresenter: ProjectDetailsInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successSendProposalHandler(_ project: ProjectModel) {
        output?.stopLoading()
        self.project = project
        coordinator.sendProposalHandler(project)
        output?.reloadData()
    }
    
    func successDeclineHandler() {
        output?.stopLoading()
        project.suggestions.removeAll()
        coordinator.declineHandler(project)
        output?.reloadData()
    }
    
}

// MARK: - Private
fileprivate extension ProjectDetailsPresenter {
    
}

