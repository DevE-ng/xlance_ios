//
//  ProjectDetailsViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator

protocol ProjectDetailsViewProtocol: BaseViewProtocol {}

class ProjectDetailsViewController: BaseViewController, StoryboardLoadable, ProjectDetailsViewProtocol {
    
    static var storyboardName: Storyboards = .projectDetails
    
    fileprivate var cells: [UITableViewCell.Type] = []
    fileprivate var milestones: [MilestoneModel] = []
    fileprivate var tasks: [TaskModel] = []
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView!
    
    // MARK: Injections
    var presenter: ProjectDetailsPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Actions

}

// MARK: - ProjectDetailsPresenterOutput
extension ProjectDetailsViewController: ProjectDetailsPresenterOutput {

    func reloadData() {
        reloadCells()
        tableView.reloadData()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .bottom, offset: 30)
        ])
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ProjectDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = cells[indexPath.row]
        switch true {
        
        case cellType == ProjectTopTableViewCell.self:
            let topCell = tableView.dequeueReusableCell(with: ProjectTopTableViewCell.self, for: indexPath)
            configureTopCell(topCell)
            return topCell
            
        case cellType == MilestoneTableViewCell.self:
            let milestoneCell = tableView.dequeueReusableCell(with: MilestoneTableViewCell.self, for: indexPath)
            configureMilestoneCell(milestoneCell)
            return milestoneCell
            
        case cellType == TaskTableViewCell.self:
            let taskCell = tableView.dequeueReusableCell(with: TaskTableViewCell.self, for: indexPath)
            configureTaskCell(taskCell)
            return taskCell
            
        case cellType == ProjectBottomButtonsTableViewCell.self:
            let buttonsCell = tableView.dequeueReusableCell(with: ProjectBottomButtonsTableViewCell.self, for: indexPath)
            configureBottomCell(buttonsCell)
            return buttonsCell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

// MARK: - Private
fileprivate extension ProjectDetailsViewController {
    
    func setupUI() {
        navigationItem.title = presenter.project.masterJob.name
        tableView.register(cellType: ProjectTopTableViewCell.self)
        tableView.register(cellType: MilestoneTableViewCell.self)
        tableView.register(cellType: TaskTableViewCell.self)
        tableView.register(cellType: ProjectBottomButtonsTableViewCell.self)
        reloadCells()
    }
    
    func reloadCells() {
        milestones.removeAll()
        tasks.removeAll()
        cells.removeAll()
        cells.append(ProjectTopTableViewCell.self)
        
        if presenter.project.paymentMethod == .hourly {
            let _ = presenter.project.tasks.map({
                cells.append(TaskTableViewCell.self)
                tasks.append($0)
            })
            
        } else {
            let _ = presenter.project.milestones.map({ milestone in
                cells.append(MilestoneTableViewCell.self)
                milestones.append(milestone)
                let _ = milestone.tasks.map({
                    cells.append(TaskTableViewCell.self)
                    tasks.append($0)
                })
            })
        }
        
        if presenter.project.status != .accepted && CurrentUser.isLogged {
            cells.append(ProjectBottomButtonsTableViewCell.self)
        }
        
    }
    
    func configureTopCell(_ cell: ProjectTopTableViewCell) {
        cell.project = presenter.project
    }
    
    func configureMilestoneCell(_ cell: MilestoneTableViewCell) {
        cell.milestone = milestones.remove(at: 0)
    }
    
    func configureTaskCell(_ cell: TaskTableViewCell) {
        cell.task = tasks.remove(at: 0)
    }
    
    func configureBottomCell(_ cell: ProjectBottomButtonsTableViewCell) {
        cell.status = presenter.project.status
        
        cell.onSendProposal = { [weak self] in
            self?.presenter.sendProposalHandler()
        }
        
        cell.onEditTerms = { [weak self] in
            self?.presenter.editTermsHandler()
        }
        
        cell.onViewCondition = { [weak self] in
            self?.presenter.viewConditionHandler()
        }
        
        cell.onDecline = { [weak self] in
            self?.presenter.declineHandler()
        }
    }
    
}
