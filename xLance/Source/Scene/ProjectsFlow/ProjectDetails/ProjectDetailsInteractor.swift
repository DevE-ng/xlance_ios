//
//  ProjectDetailsInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProjectDetailsInteractorInput: BaseInteractorInput {
    func sendProposal(_ project: ProjectModel)
    func fetchSuggestion(id: Int, completionHandler: @escaping CompletionSuggestionBlock)
    func declineSuggestion(id: Int)
}
protocol ProjectDetailsInteractorOutput: BaseInteractorOutput {
    func errorHandler(_ error: String)
    func successSendProposalHandler(_ project: ProjectModel)
    func successDeclineHandler()
}

class ProjectDetailsInteractor {
    
    var networkProvider: Networking!
    weak var presenter: ProjectDetailsInteractorOutput?
    
    private lazy var suggestionsWorker: SuggestionWorker = {
        return SuggestionWorkerImp(networkProvider: self.networkProvider)
    }()
    
}

// MARK: - ProjectDetailsPresenterInput
extension ProjectDetailsInteractor: ProjectDetailsInteractorInput {
    
    func sendProposal(_ project: ProjectModel) {
        suggestionsWorker.sendProposal(project) { [weak self] in
            self?.handleSendProposal($0)
        }
    }
    
    func fetchSuggestion(id: Int, completionHandler: @escaping CompletionSuggestionBlock) {
        suggestionsWorker.fetchSuggestion(id: id) { [weak self] in
            self?.handleFetchSuggestion($0, completionHandler: completionHandler)
        }
    }
    
    func declineSuggestion(id: Int) {
        suggestionsWorker.declineSuggestion(id: id) { [weak self] in
            self?.handleDeclineSuggestion($0)
        }
    }
    
}

// MARK: - Private
fileprivate extension ProjectDetailsInteractor {
    
    func handleSendProposal(_ result: Result<ProjectModel>) {
        switch result {
            
        case .Success(let project):
            presenter?.successSendProposalHandler(project)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleFetchSuggestion(_ result: Result<SuggestionModel>, completionHandler: @escaping CompletionSuggestionBlock) {
        switch result {
            
        case .Success(let suggestion):
            completionHandler(suggestion)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleDeclineSuggestion(_ result: Result<Bool>) {
        switch result {
            
        case .Success(_):
            presenter?.successDeclineHandler()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

