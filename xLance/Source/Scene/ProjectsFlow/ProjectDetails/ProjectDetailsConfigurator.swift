//
//  ProjectDetailsConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProjectDetailsConfigurable {
}

class ProjectDetailsConfigurator<T: ProjectDetailsViewController>: BaseViewConfigurable, ProjectDetailsConfigurable {
    
    private let project: ProjectModel
    private let networkProvider: Networking
    private let coordinator: ProjectDetailsCoordinatable
    
    required init(project: ProjectModel, networkProvider: Networking, coordinator: ProjectDetailsCoordinatable) {
        self.project = project
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: ProjectDetailsConfigurable
    
    func create() -> ProjectDetailsViewController {
        let viewController = ProjectDetailsViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: ProjectDetailsViewController) {
        
        let interactor = ProjectDetailsInteractor()
        let presenter = ProjectDetailsPresenter(output: viewController)
        
        presenter.project = project
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
