//
//  MyProjectListPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol MyProjectListPresenterInput: BasePresenterInput {
    var projects: [ProjectModel] { get set }
    func didSelectRow(_ row: Int)
    func fetchMyProjects()
}

protocol MyProjectListPresenterOutput: BasePresenterOutput {
    func reloadData()
    func reloadCell(row: Int)
}

class MyProjectListPresenter {
    
    var projects = [ProjectModel]()
    
    //MARK: Injections
    var interactor: MyProjectListInteractorInput!
    var coordinator: ProjectListCoordinatable! {
        didSet {
            if self.coordinator == nil { return }
            self.coordinator.onUpdateProject = { [weak self] _ in
                self?.fetchMyProjects()
            }
        }
    }
    private weak var output: MyProjectListPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: MyProjectListPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - MyProjectListPresenterInput
extension MyProjectListPresenter: MyProjectListPresenterInput {
    
    func viewDidLoad() {
        fetchMyProjects()
    }
    
    func didSelectRow(_ row: Int) {
        let project = projects[row]
        coordinator.didSelectProject(project)
    }
    
    func fetchMyProjects() {
        output?.startLoading()
        interactor.fetchMyProjects()
    }
    
}

extension MyProjectListPresenter: MyProjectListInteractorOutput {
    
    func successfetchMyProjects(_ projects: [ProjectModel]) {
        self.projects = projects
        output?.stopLoading()
        output?.reloadData()
    }
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
}

// MARK: - Private
fileprivate extension MyProjectListPresenter {}
