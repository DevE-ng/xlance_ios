//
//  MyProjectListInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol MyProjectListInteractorInput: BaseInteractorInput {
    func fetchMyProjects()
}
protocol MyProjectListInteractorOutput: BaseInteractorOutput {
    func successfetchMyProjects(_ projects: [ProjectModel])
    func errorHandler(_ error: String)
}

class MyProjectListInteractor {
    
    var networkProvider: Networking!
    weak var presenter: MyProjectListInteractorOutput?
    
    private lazy var worker: MyProjectsWorker = {
        return MyProjectsWorkerImp(projectsWorker: ProjectsWorkerImp(networkProvider: self.networkProvider),
                                   suggestionsWorker: SuggestionWorkerImp(networkProvider: self.networkProvider))
    }()
    
    
    
}

// MARK: - MyProjectListPresenterInput
extension MyProjectListInteractor: MyProjectListInteractorInput {
    
    func fetchMyProjects() {
        worker.fetchMyProjects { [weak self] in
            self?.presenter?.successfetchMyProjects($0)
        }
    }
    
}

// MARK: - Private
fileprivate extension MyProjectListInteractor {}
