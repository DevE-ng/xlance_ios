//
//  MyProjectListViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator

protocol MyProjectListViewProtocol {}

class MyProjectListViewController: BaseViewController, StoryboardLoadable, MyProjectListViewProtocol {
    
    static var storyboardName: Storyboards = .myProjectList
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView?
    fileprivate var refreshControl = UIRefreshControl()
    
    // MARK: Injections
    var presenter: MyProjectListPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }

}

// MARK: - MyProjectListPresenterOutput
extension MyProjectListViewController: MyProjectListPresenterOutput {

    func reloadData() {
        guard let tableView = tableView else { return }
        tableView.reloadData()
        refreshControl.endRefreshing()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .bottom, offset: 30)
        ])
    }
    
    func reloadCell(row: Int) {
        let indexPath = IndexPath(row: row, section: 0)
        tableView?.reloadRows(at: [indexPath], with: .automatic)
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MyProjectListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ProjectTableViewCell.self, for: indexPath)
        let project = presenter.projects[indexPath.row]
        cell.selectionStyle = .none
        cell.project = project
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(indexPath.row)
    }
    
}

// MARK: - Private
fileprivate extension MyProjectListViewController {
    
    func setupUI() {
        navigationItem.title = "HEADER_COMPONENT.MY_PROJECTS".localized
        refreshControl.alpha = 0
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView?.refreshControl = refreshControl
        tableView?.contentInset = .init(top: 8, left: 0, bottom: 0, right: 0)
        tableView?.register(cellType: ProjectTableViewCell.self)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        presenter.fetchMyProjects()
    }
    
}

