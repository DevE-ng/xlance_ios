//
//  MyProjectListConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol MyProjectListConfigurable {
}

class MyProjectListConfigurator<T: MyProjectListViewController>: BaseViewConfigurable, MyProjectListConfigurable {
    
    private let networkProvider: Networking
    private let coordinator: ProjectListCoordinatable
    
    required init(networkProvider: Networking, coordinator: ProjectListCoordinatable) {
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: MyProjectListConfigurable
    
    func create() -> MyProjectListViewController {
        let viewController = MyProjectListViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: MyProjectListViewController) {
        
        let interactor = MyProjectListInteractor()
        let presenter = MyProjectListPresenter(output: viewController)
        
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
