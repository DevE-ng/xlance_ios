//
//  SwiftMessagesTableViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 10/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit
import SwiftMessages

class SelectTableViewController: UITableViewController {
    
    // Mark: - Injections
    var list: [String] = []
    var selectedItems: [Int] = []
    var onSelectItems: (([Int]) -> Void)?
    var allowMultiplySelection = false
    
    // MARK: - Init
    
    static func show(source: Presentable?,
                     list: [String],
                     onSelectItems: (([Int]) -> Void)?,
                     selectedItems: [Int] = [],
                     allowMultiplySelection: Bool = false) {
        guard let source = source?.toPresent else { return }
        let tableVC = SelectTableViewController()
        
        tableVC.list = list
        tableVC.onSelectItems = onSelectItems
        tableVC.selectedItems = selectedItems
        tableVC.allowMultiplySelection = allowMultiplySelection
        
        let segue = SelectTableSwiftMessagesSegue(identifier: nil, source: source, destination: tableVC)
        segue.perform()
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = allowMultiplySelection
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        
        if selectedItems.contains(indexPath.row) {
            cell.accessoryType = .checkmark
        }

        cell.textLabel?.text = list[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !allowMultiplySelection {
            selectedItems.forEach {
                tableView.cellForRow(at: .init(row: $0, section: 0))?.accessoryType = .none
            }
            selectedItems.removeAll()
        }
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedItems.append(indexPath.row)
        onSelectItems?(selectedItems)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if !allowMultiplySelection { return }
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
        selectedItems.removeAll(where: { $0 == indexPath.row })
        onSelectItems?(selectedItems)
    }

}
