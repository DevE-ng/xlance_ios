//
//  SelectTableSwiftMessagesSegue.swift
//  xlance
//
//  Created by Anton Lobanov on 10/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import SwiftMessages

class SelectTableSwiftMessagesSegue: SwiftMessagesSegue {
    
    override public init(identifier: String?, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
        configure(layout: .bottomCard)
        dimMode = .blur(style: .dark, alpha: 0.9, interactive: true)
        presentationStyle = .bottom
        messageView.configureNoDropShadow()
    }
    
}
