//
//  LaunchScreenInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol LaunchScreenInteractorInput: BaseInteractorInput {
    var isUserAnonymous: Bool { get }
    func performAuth()
}

protocol LaunchScreenInteractorOutput: BaseInteractorOutput {
    func successAuthHandler()
    func errorAuthHandler(_ error: String?)
}

class LaunchScreenInteractor: LaunchScreenInteractorInput {
    
    var networkProvider: Networking!
    weak var presenter: LaunchScreenInteractorOutput?
    
    var isUserAnonymous: Bool {
        return CurrentUser.isAnonymous
    }
    
    func performAuth() {
        guard
            let username = CurrentUser.username,
            let password = CurrentUser.password else {
            presenter?.errorAuthHandler(nil)
            return
        }
        networkProvider.login(name: username, password: password) { [weak self] in
            self?.handleLogin($0, name: username, password: password)
        }
    }
    
}

fileprivate extension LaunchScreenInteractor {
    
    func handleLogin(_ result: Result<LoginResponse>, name: String, password: String) {
        switch result {
        case .Success(let data):
            CurrentUser.login(data, name: name, pass: password)
            presenter?.successAuthHandler()
            
        case .Error(let error):
            presenter?.errorAuthHandler(error)
            
        }
    }
    
}
