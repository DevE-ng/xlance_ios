//
//  LaunchScreenViewController.swift
//  xlance
//
//  Created by alobanov11 on 04/08/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import UIKit

class LaunchScreenViewController: BaseViewController {
    
    // MARK: Outlets
    
    // MARK: Injections
    var presenter: LaunchScreenPresenterInput!
    
    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    }
    
}

// MARK: - LaunchScreenPresenterOutput
extension LaunchScreenViewController: LaunchScreenPresenterOutput {
    
}

// MARK: - Private
fileprivate extension LaunchScreenViewController {
    
    func setupUI() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}

// MARK: - StoryboardLoadable
extension LaunchScreenViewController: StoryboardLoadable {
    static var storyboardName: Storyboards = .launchScreen
}
