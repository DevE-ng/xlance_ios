//
//  LaunchScreenConfigurator.swift
//  xlance
//
//  Created by alobanov11 on 04/08/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import UIKit

protocol LaunchScreenConfigurable {
}

class LaunchScreenConfigurator<T: LaunchScreenViewController>: BaseViewConfigurable, LaunchScreenConfigurable {
    
    private let networkProvider: Networking
    private let coordinator: LaunchScreenCoordinatable
    
    required init(networkProvider: Networking, coordinator: LaunchScreenCoordinatable) {
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }

    //MARK: LaunchScreenConfigurable
    
    func create() -> LaunchScreenViewController {
        let viewController = LaunchScreenViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: LaunchScreenViewController) {
        
        let interactor = LaunchScreenInteractor()
        let presenter = LaunchScreenPresenter(output: viewController)
        
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
    }
}
