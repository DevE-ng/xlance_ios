//
//  LaunchScreenPresenter.swift
//  xlance
//
//  Created by alobanov11 on 04/08/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import Foundation

protocol LaunchScreenCoordinatable {
    func userIsAuthenticated()
    func userIsAnonymous()
    func userNeedsToAuthenticate()
}

protocol LaunchScreenPresenterInput: BasePresenterInput {}
protocol LaunchScreenPresenterOutput: BasePresenterOutput {}

class LaunchScreenPresenter {
    
    //MARK: Injections
    var interactor: LaunchScreenInteractorInput!
    var coordinator: LaunchScreenCoordinatable!
    private weak var output: LaunchScreenPresenterOutput?
    
    //MARK: LifeCycle
    init(output: LaunchScreenPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - LaunchScreenPresenterInput
extension LaunchScreenPresenter: LaunchScreenPresenterInput {
    
    func viewDidLoad() {
        if interactor.isUserAnonymous {
            coordinator.userIsAnonymous()
        } else {
            output?.startLoading()
            interactor.performAuth()
        }
    }
    
}

// MARK: - LaunchScreenInteractorOutput
extension LaunchScreenPresenter: LaunchScreenInteractorOutput {
    
    func successAuthHandler() {
        output?.stopLoading()
        coordinator.userIsAuthenticated()
    }
    
    func errorAuthHandler(_ error: String?) {
        if let error = error {
            output?.showErrorMessage(error)
        }
        output?.stopLoading()
        coordinator.userNeedsToAuthenticate()
    }
    
}





