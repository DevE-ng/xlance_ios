//
//  ProfilePresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProfileCoordinatable {
    func changePasswordHandler()
    func userPerformedLogout()
}

protocol ProfilePresenterInput: BasePresenterInput {
    var profile: ProfileModel? { get set }
    func changePasswordHandler()
    func logoutHandler()
}

protocol ProfilePresenterOutput: BasePresenterOutput {}

class ProfilePresenter {
    
    //MARK: Injections
    var profile: ProfileModel?
    var interactor: ProfileInteractorInput!
    var coordinator: ProfileCoordinatable!
    private weak var output: ProfilePresenterOutput?
    
    //MARK: LifeCycle 
    init(output: ProfilePresenterOutput) {
        self.output = output
    }
    
}

// MARK: - ProfilePresenterInput
extension ProfilePresenter: ProfilePresenterInput {
    
    func viewDidLoad() {
        
    }
    
    func changePasswordHandler() {
        coordinator.changePasswordHandler()
    }
    
    func logoutHandler() {
        interactor.logout()
        coordinator.userPerformedLogout()
    }
    
}

// MARK: - ProfileInteractorOutput
extension ProfilePresenter: ProfileInteractorOutput {
    
}

// MARK: - Private
fileprivate extension ProfilePresenter {
    
    
    
}

