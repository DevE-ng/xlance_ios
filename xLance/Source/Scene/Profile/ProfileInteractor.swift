//
//  ProfileInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProfileInteractorInput: BaseInteractorInput {
    func logout()
}
protocol ProfileInteractorOutput: BaseInteractorOutput {}

class ProfileInteractor {
    
    var networkProvider: Networking!
    weak var presenter: ProfileInteractorOutput?
    
}

// MARK: - ProfilePresenterInput
extension ProfileInteractor: ProfileInteractorInput {
    func logout() {
        CurrentUser.logout()
        networkProvider.logout { _ in () }
    }
}

// MARK: - Private
fileprivate extension ProfileInteractor {}

