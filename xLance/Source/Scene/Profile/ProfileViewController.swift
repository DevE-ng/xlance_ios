//
//  ProfileViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProfileViewProtocol {}

class ProfileViewController: BaseViewController, StoryboardLoadable, ProfileViewProtocol {
    
    static var storyboardName: Storyboards = .profile
    
    // MARK: Outlets
    @IBOutlet fileprivate var scrollView: UIScrollView!
    @IBOutlet fileprivate var containerView: UIView!
    @IBOutlet fileprivate var avatarImageView: UIImageView!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var emailLabel: UILabel!
    
    @IBOutlet fileprivate var notAnonymousViews: [UIView]!
    
    @IBOutlet fileprivate var changePasswordButton: UIButton!
    @IBOutlet fileprivate var logoutButton: UIButton!
    
    // MARK: Injections
    var presenter: ProfilePresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Actions
    
    @IBAction fileprivate func changePasswordButtonTapHandler(_ sender: Any) {
        presenter.changePasswordHandler()
    }
    
    @IBAction fileprivate func logoutButtonTapHandler(_ sender: Any) {
        presenter.logoutHandler()
    }
    
    
}

// MARK: - ProfilePresenterOutput
extension ProfileViewController: ProfilePresenterOutput {

}

// MARK: - Private
fileprivate extension ProfileViewController {
    
    func setupUI() {
        if presenter.profile == nil {
            let _ = notAnonymousViews.map({ $0.isHidden = true })
        } else {
            navigationItem.title = "EDIT_PROFILE_COMPONENT.TAB_EDIT_PROFILE".localized
        }
        
        changePasswordButton.setTitle("FORGOT_COMPONENT.RESET_PASSWORD".localized, for: .normal)
        logoutButton.setTitle("HEADER_COMPONENT.EXIT".localized, for: .normal)
        
        containerView.layer.addShadow(opacity: 0.2,
                                      offset: .init(width: 8, height: 10),
                                      radius: 12, shadowColor: .black, cornerRadius: 22)
        nameLabel.text = (presenter.profile?.fullName ?? "") + "\(presenter.profile != nil ? " (\(presenter.profile!.role.localized.lowercased()))" : "")"
        emailLabel.text = presenter.profile?.email
    }
    
}
