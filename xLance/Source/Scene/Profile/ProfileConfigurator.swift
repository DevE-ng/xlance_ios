//
//  ProfileConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProfileConfigurable {
}

class ProfileConfigurator<T: ProfileViewController>: BaseViewConfigurable, ProfileConfigurable {
    
    private let profile: ProfileModel?
    private let networkProvider: Networking
    private let coordinator: ProfileCoordinatable
    
    required init(profile: ProfileModel?, networkProvider: Networking, coordinator: ProfileCoordinatable) {
        self.profile = profile
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: ProfileConfigurable
    
    func create() -> ProfileViewController {
        let viewController = ProfileViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: ProfileViewController) {
        
        let interactor = ProfileInteractor()
        let presenter = ProfilePresenter(output: viewController)
        
        presenter.profile = profile
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
