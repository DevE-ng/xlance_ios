//
//  MainPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 08/07/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import Foundation

protocol MainCoordinatable {}
protocol MainPresenterInput: BasePresenterInput {}
protocol MainPresenterOutput: BasePresenterOutput {}

class MainPresenter {
    
    
    //MARK: Injections
    var interactor: MainInteractorInput!
    var coordinator: MainCoordinatable!
    private weak var output: MainPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: MainPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - MainPresenterInput
extension MainPresenter: MainPresenterInput {
    
    func viewDidLoad() {
    }
    
}

// MARK: - MainInteractorOutput
extension MainPresenter: MainInteractorOutput {
    
}

fileprivate extension MainPresenter {
    
    
}

