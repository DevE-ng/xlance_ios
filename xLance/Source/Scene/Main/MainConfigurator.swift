//
//  MainConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol MainConfigurable {
}

class MainConfigurator<T: MainViewController>: BaseViewConfigurable, MainConfigurable {
    
    private let coordinator: MainCoordinatable
    
    required init(coordinator: MainCoordinatable) {
        self.coordinator = coordinator
    }
    
    //MARK: MainConfigurable
    
    func create() -> MainViewController {
        let viewController = MainViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: MainViewController) {
        
        let interactor = MainInteractor()
        let presenter = MainPresenter(output: viewController)
        
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
