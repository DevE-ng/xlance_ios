//
//  MainInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol MainInteractorInput: BaseInteractorInput {}
protocol MainInteractorOutput: BaseInteractorOutput {}

class MainInteractor {
    
    weak var presenter: MainInteractorOutput?
    
}

// MARK: - MainPresenterInput
extension MainInteractor: MainInteractorInput {}

// MARK: - Private
fileprivate extension MainInteractor {}

