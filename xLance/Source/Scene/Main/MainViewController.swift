//
//  MainViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 08/07/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .main
    
    // MARK: Outlets
    
    // MARK: Injections
    var presenter: MainPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }

}

// MARK: - MainPresenterOutput
extension MainViewController: MainPresenterOutput {

}

fileprivate extension MainViewController {
    
}
