//
//  MilestoneListViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 19/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

class MilestoneListViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .milestoneList
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView!
    
    fileprivate let cellID = "milestoneCell"
    
    // MARK: Injections
    var presenter: MilestoneListPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    }

}

// MARK: - MilestoneListPresenterOutput
extension MilestoneListViewController: MilestoneListPresenterOutput {

}

extension MilestoneListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.milestones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        let milestone = presenter.milestones[indexPath.row]
        cell.selectionStyle = .none
        cell.textLabel?.text = milestone.name
        if presenter.selectedMilestoneId == milestone.id {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let milestone = presenter.milestones[indexPath.row]
        cell?.accessoryType = .checkmark
        presenter.selectMilestoneHandler(milestone)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .none
    }
    
}

// MARK: - Private
fileprivate extension MilestoneListViewController {
    
    func setupUI() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)

    }
    
}
