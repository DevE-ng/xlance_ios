//
//  MilestoneListConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 19/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol MilestoneListConfigurable {
}

class MilestoneListConfigurator<T: MilestoneListViewController>: BaseViewConfigurable, MilestoneListConfigurable {
    
    private let milestones: [MilestoneModel]
    private let selectedMilestoneId: Int?
    private let coordinator: MilestoneListCoordinatable
    
    required init(milestones: [MilestoneModel], selectedMilestoneId: Int?, coordinator: MilestoneListCoordinatable) {
        self.milestones = milestones
        self.selectedMilestoneId = selectedMilestoneId
        self.coordinator = coordinator
    }

    //MARK: MilestoneListConfigurable
    
    func create() -> MilestoneListViewController {
        let viewController = MilestoneListViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: MilestoneListViewController) {
        
        let presenter = MilestoneListPresenter(output: viewController)
        
        presenter.coordinator = coordinator
        presenter.milestones = milestones
        presenter.selectedMilestoneId = selectedMilestoneId
        
        viewController.presenter = presenter

    }
}
