//
//  MilestoneListPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 19/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol MilestoneListCoordinatable {
    func didSelectMilestone(_ milestone: MilestoneModel)
}

protocol MilestoneListPresenterInput: BasePresenterInput {
    var milestones: [MilestoneModel] { get set }
    var selectedMilestoneId: Int? { get set }
    func selectMilestoneHandler(_ milestone: MilestoneModel)
}

protocol MilestoneListPresenterOutput: BasePresenterOutput {}

class MilestoneListPresenter {
    
    var milestones: [MilestoneModel] = []
    var selectedMilestoneId: Int?
    
    //MARK: Injections
    var coordinator: MilestoneListCoordinatable!
    private weak var output: MilestoneListPresenterOutput?
    
    
    //MARK: LifeCycle 
    init(output: MilestoneListPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - MilestoneListPresenterInput
extension MilestoneListPresenter: MilestoneListPresenterInput {
    
    func viewDidLoad() {
        
    }
    
    func selectMilestoneHandler(_ milestone: MilestoneModel) {
        coordinator.didSelectMilestone(milestone)
    }
    
}

// MARK: - Private
fileprivate extension MilestoneListPresenter {
    
    
    
}

