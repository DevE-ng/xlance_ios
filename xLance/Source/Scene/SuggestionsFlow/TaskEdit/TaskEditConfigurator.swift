//
//  TaskEditConfigurator.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol TaskEditConfigurable {
}

class TaskEditConfigurator<T: TaskEditViewController>: BaseViewConfigurable, TaskEditConfigurable {
    
    private let task: TaskModel
    private let project: ProjectModel
    private let networkProvider: Networking
    private let coordinator: TaskEditCoordinatable
    
    required init(task: TaskModel, project: ProjectModel,
                  networkProvider: Networking, coordinator: TaskEditCoordinatable) {
        self.task = task
        self.project = project
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }

    //MARK: TaskEditConfigurable
    
    func create() -> TaskEditViewController {
        let viewController = TaskEditViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: TaskEditViewController) {
        
        let presenter = TaskEditPresenter(output: viewController)
        
        presenter.coordinator = coordinator
        presenter.project = project
        presenter.task = task
        
        viewController.presenter = presenter

    }
}
