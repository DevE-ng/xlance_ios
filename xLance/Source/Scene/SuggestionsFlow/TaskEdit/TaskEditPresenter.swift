//
//  TaskEditPresenter.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol TaskEditCoordinatable {
    var onUpdateMilestone: CompletionMilestoneBlock? { get set }
    func didUpdateProject(_ project: ProjectModel)
    func selectMilestoneHandler(milestones: [MilestoneModel], selectedMilestoneId: Int?)
}

protocol TaskEditPresenterInput: BasePresenterInput {
    
    var project: ProjectModel! { get set }
    var task: TaskModel! { get set }
    var taskHistory: TaskHistoryModel { get set }
    
    func submitHandler()
    func selectMilestoneHandler()
    
}

protocol TaskEditPresenterOutput: BasePresenterOutput {
    func updateMilestone()
}

class TaskEditPresenter {
    
    //MARK: Injections
    var project: ProjectModel!
    var task: TaskModel!
    var taskHistory: TaskHistoryModel {
        get {
            return task.actualHistory
        }
        set {
            task.actualHistory = newValue
        }
    }
    var coordinator: TaskEditCoordinatable! {
        didSet {
            if self.coordinator == nil { return }
            
            self.coordinator.onUpdateMilestone = { [weak self] milestone in
                self?.taskHistory.milestoneId = milestone.id
                self?.output?.updateMilestone()
            }
            
        }
    }
    private weak var output: TaskEditPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: TaskEditPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - TaskEditPresenterInput
extension TaskEditPresenter: TaskEditPresenterInput {
    
    func viewDidLoad() {
        
    }
    
    func submitHandler() {
        if !project.isValidTask(task) {
            output?.showInfoMessage("TASK_DIALOG_COMPONENT.DURATION".localized)
            return
        }
        project.updateOrAddTask(task)
        coordinator.didUpdateProject(project)
    }
    
    func selectMilestoneHandler() {
        coordinator.selectMilestoneHandler(milestones: project.milestones, selectedMilestoneId: task.milestoneId)
    }
    
}

// MARK: - Private
fileprivate extension TaskEditPresenter {
    
}

