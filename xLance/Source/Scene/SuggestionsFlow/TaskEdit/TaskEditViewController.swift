//
//  TaskEditViewController.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

class TaskEditViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .taskEdit
    
    // MARK: Outlets
    @IBOutlet fileprivate var nameTextField: ModyfingTextField!
    @IBOutlet fileprivate var durationTextField: ModyfingTextField!
    @IBOutlet fileprivate var milestoneTextField: ModyfingTextField!
    @IBOutlet fileprivate var descTextView: UITextView!
    @IBOutlet fileprivate var submitButton: UIButton!
    
    // MARK: Injections
    var presenter: TaskEditPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }
    
    @IBAction fileprivate func valueChangedHandler(_ sender: UITextField) {
        switch sender {
            
        case nameTextField:
            presenter.taskHistory.name = sender.text ?? ""
            
        case durationTextField:
            presenter.taskHistory.duration = sender.text?.int ?? 0
            
        default: ()
        }
    }
    
    @IBAction fileprivate func milestoneTextFieldEditingBeginHandler(_ sender: UITextField) {
        presenter.selectMilestoneHandler()
        milestoneTextField.resignFirstResponder()
    }
    
    @IBAction fileprivate func submitButtonTapHandler(_ sender: UIButton) {
        presenter.submitHandler()
    }

}

// MARK: - TaskEditPresenterOutput
extension TaskEditViewController: TaskEditPresenterOutput {
    
    func updateMilestone() {
        if let milestoneName = presenter.project.milestoneNameById(presenter.taskHistory.milestoneId) {
            milestoneTextField.text = milestoneName
        } else if presenter.project.paymentMethod == .fixed {
            milestoneTextField.text = nil
        } else {
            milestoneTextField.isHidden = true
        }
    }

}

extension TaskEditViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        presenter.taskHistory.description = textView.text
    }
    
}

// MARK: - Private
fileprivate extension TaskEditViewController {
    
    func setupUI() {
        nameTextField.placeholder = "TASK_DIALOG_COMPONENT.NAME".localized
        durationTextField.placeholder = "TASK_COMPONENT.DURATION".localized
        milestoneTextField.placeholder = "COMMON.MILESTONE".localized
        descTextView.text = "EDITPROFILE_COMPONENT.DESCRIPTION".localized
        submitButton.setTitle("COMMON.SAVE".localized, for: .normal)
        if presenter.taskHistory.status == .added {
            navigationItem.title = "TASK_DIALOG_COMPONENT.ADD_TASK".localized
        } else {
            navigationItem.title = "TASK_DIALOG_COMPONENT.EDIT_TASK".localized
        }
        addTapGestureWithEndEditing()
        configure()
    }
    
    func configure() {
        nameTextField.text = presenter.taskHistory.name.isEmpty ? nil : presenter.taskHistory.name
        durationTextField.text = presenter.taskHistory.duration == 0 ? nil : presenter.taskHistory.duration.string
        descTextView.text = presenter.taskHistory.description.isEmpty ? nil : presenter.taskHistory.description
        updateMilestone()
    }
    
}
