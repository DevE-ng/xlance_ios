//
//  SuggestionEditInteractor.swift
//  xlance
//
//  Created by alobanov11 on 04/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol SuggestionEditInteractorInput: BaseInteractorInput {
    func createSuggestion(_ suggestion: SuggestionModel)
    func editSuggestion(_ suggestion: SuggestionModel)
}
protocol SuggestionEditInteractorOutput: BaseInteractorOutput {
    func successSendSuggestion(_ suggestion: SuggestionModel)
    func errorHandler(_ error: String)
}

class SuggestionEditInteractor {
    
    var networkProvider: Networking!
    weak var presenter: SuggestionEditInteractorOutput?
    
}

// MARK: - SuggestionEditPresenterInput
extension SuggestionEditInteractor: SuggestionEditInteractorInput {
    
    func createSuggestion(_ suggestion: SuggestionModel) {
        networkProvider.createSuggestion(suggestion) { [weak self] in
            self?.handleSendSuggestion($0)
        }
    }
    
    func editSuggestion(_ suggestion: SuggestionModel) {
        networkProvider.editSuggestion(suggestion) { [weak self] in
            self?.handleSendSuggestion($0)
        }
    }
    
}

// MARK: - Private
fileprivate extension SuggestionEditInteractor {
    
    func handleSendSuggestion(_ result: Result<SuggestionModel>) {
        switch result {
            
        case .Success(let suggestion):
            presenter?.successSendSuggestion(suggestion)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

