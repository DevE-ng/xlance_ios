//
//  SuggestionEditConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol SuggestionEditConfigurable {
}

class SuggestionEditConfigurator<T: SuggestionEditViewController>: BaseViewConfigurable, SuggestionEditConfigurable {
    
    private let suggestion: SuggestionModel
    private let networkProvider: Networking
    private let coordinator: SuggestionEditCoordinatable
    
    required init(suggestion: SuggestionModel, networkProvider: Networking,
                  coordinator: SuggestionEditCoordinatable) {
        self.suggestion = suggestion
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: SuggestionEditConfigurable
    
    func create() -> SuggestionEditViewController {
        let viewController = SuggestionEditViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: SuggestionEditViewController) {
        
        let interactor = SuggestionEditInteractor()
        let presenter = SuggestionEditPresenter(output: viewController)
        
        presenter.suggestion = suggestion
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        viewController.presenter = presenter
        
    }
}
