//
//  SuggestionEditViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator

class SuggestionEditViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .suggestionEdit
    
    // MARK: Outlets
    fileprivate var cells: [UITableViewCell.Type] = []
    fileprivate var milestones: [MilestoneModel] = []
    fileprivate var tasks: [TaskModel] = []
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView!
    
    // MARK: Injections
    var presenter: SuggestionEditPresenterInput!
    
    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }
    
    // MARK: Actions
    
    @IBAction fileprivate func addButtonTapHandler(_ sender: Any) {
        showAddAlertController()
    }
    
}

// MARK: - SuggestionDetailsPresenterOutput
extension SuggestionEditViewController: SuggestionEditPresenterOutput {
    
    func reloadData() {
        reloadCells()
        tableView.reloadData()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .bottom, offset: 30)
            ])
    }
    
}

extension SuggestionEditViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = cells[indexPath.row]
        switch true {
            
        case cellType == SuggestionTopEditTableViewCell.self:
            let topCell = tableView.dequeueReusableCell(with: SuggestionTopEditTableViewCell.self, for: indexPath)
            configureTopCell(topCell)
            return topCell
            
        case cellType == MilestoneTableViewCell.self:
            let milestoneCell = tableView.dequeueReusableCell(with: MilestoneTableViewCell.self, for: indexPath)
            configureMilestoneCell(milestoneCell)
            return milestoneCell
            
        case cellType == TaskTableViewCell.self:
            let taskCell = tableView.dequeueReusableCell(with: TaskTableViewCell.self, for: indexPath)
            configureTaskCell(taskCell)
            return taskCell
            
        case cellType == SuggestionBottomEditButtonsTableViewCell.self:
            let buttonsCell = tableView.dequeueReusableCell(with: SuggestionBottomEditButtonsTableViewCell.self, for: indexPath)
            configureBottomCell(buttonsCell)
            return buttonsCell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

// MARK: - Private
fileprivate extension SuggestionEditViewController {
    
    func setupUI() {
        addTapGestureWithEndEditing()
        navigationItem.title = "SUGGESTION_DIALOG_COMPONENT.EDIT_TERMS".localized
        tableView.contentInset = .init(top: 8, left: 0, bottom: 0, right: 0)
        let cellTypes = [ SuggestionTopEditTableViewCell.self,
                          MilestoneTableViewCell.self,
                          TaskTableViewCell.self,
                          SuggestionBottomEditButtonsTableViewCell.self]
        tableView.register(cellTypes: cellTypes)
        reloadCells()
    }
    
    func reloadCells() {
        milestones.removeAll()
        tasks.removeAll()
        cells.removeAll()
        cells.append(SuggestionTopEditTableViewCell.self)
        
        if presenter.project.paymentMethod == .hourly {
            let _ = presenter.project.tasks.map({
                cells.append(TaskTableViewCell.self)
                tasks.append($0)
            })
            
        } else {
            let _ = presenter.project.milestones.map({ milestone in
                cells.append(MilestoneTableViewCell.self)
                milestones.append(milestone)
                let _ = milestone.tasks.map({
                    cells.append(TaskTableViewCell.self)
                    tasks.append($0)
                })
            })
        }
        
        cells.append(SuggestionBottomEditButtonsTableViewCell.self)
        
    }
    
    func configureTopCell(_ cell: SuggestionTopEditTableViewCell) {
        cell.suggestion = presenter.suggestion
        cell.onSuggestionEdit = { [weak self] suggestion in
            self?.presenter.suggestionEditHandler(suggestion)
        }
    }
    
    func configureMilestoneCell(_ cell: MilestoneTableViewCell) {
        let milestone = milestones.remove(at: 0)
        cell.edit = true
        cell.milestone = milestone
        cell.onEdit = { [weak self] in
            self?.presenter.editMilestoneHandler(milestone)
        }
        cell.onDelete = { [weak self] in
            self?.presenter.deleteMilestoneHandler(milestone)
        }
    }
    
    func configureTaskCell(_ cell: TaskTableViewCell) {
        let task = tasks.remove(at: 0)
        cell.edit = true
        cell.task = task
        cell.onEdit = { [weak self] in
            self?.presenter.editTaskHandler(task)
        }
        cell.onDelete = { [weak self] in
            self?.presenter.deleteTaskHandler(task)
        }
    }
    
    func configureBottomCell(_ cell: SuggestionBottomEditButtonsTableViewCell) {
        cell.suggestion = presenter.suggestion
        cell.onSendProposal = { [weak self] in
            self?.presenter.sendProposalHandler()
        }
        cell.onRestoreToDefault = { [weak self] in
            self?.presenter.restoreToDefaultHandler()
        }
    }
    
    func showAddAlertController() {
        let alertController = UIAlertController(title: "SELECT_MODEL".localized,
                                                message: nil,
                                                preferredStyle: .actionSheet)

        let addTaskAction = UIAlertAction(title: "SUGGESTION_DIFF.ADD_TASK".localized, style: .default) { [weak self] _ in
            self?.presenter.addTaskHandler()
        }
        
        let addMilestoneAction = UIAlertAction(title: "TASK_LIST_COMPONENT.ADD_MILESTONE".localized, style: .default) { [weak self] _ in
            self?.presenter.addMilestoneHandler()
        }
        
        let cancelAction = UIAlertAction(title: "FREELANCER_SUGGESTIONS_LIST_COMPONENT.CANCEL".localized, style: .cancel) { _ in }
        
        if presenter.project.paymentMethod == .fixed {
            alertController.addAction(addMilestoneAction)
        }
        
        alertController.addAction(addTaskAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
