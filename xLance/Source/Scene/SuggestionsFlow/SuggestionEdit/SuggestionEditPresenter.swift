//
//  SuggestionEditPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol SuggestionEditCoordinatable {
    func addTaskHandler(_ task: TaskModel)
    func editTaskHandler(_ task: TaskModel)
    
    func addMilestoneHandler(_ milestone: MilestoneModel)
    func editMilestoneHandler(_ milestone: MilestoneModel)
    
    func sendProposalHandler(_ suggestion: SuggestionModel)
    func resetToDefaultHandler()
    
    var onUpdateProject: CompletionProjectBlock? { get set }
    var onResetToDefault: CompletionSuggestionBlock? { get set }
}

protocol SuggestionEditPresenterInput: BasePresenterInput {
    var project: ProjectModel { get }
    var suggestion: SuggestionModel! { get set }
    
    func suggestionEditHandler(_ suggestion: SuggestionModel)
    
    func addTaskHandler()
    func editTaskHandler(_ task: TaskModel)
    func deleteTaskHandler(_ task: TaskModel)
    
    func addMilestoneHandler()
    func editMilestoneHandler(_ milestone: MilestoneModel)
    func deleteMilestoneHandler(_ milestone: MilestoneModel)
    
    func sendProposalHandler()
    func restoreToDefaultHandler()
}

protocol SuggestionEditPresenterOutput: BasePresenterOutput {
    func reloadData()
}

class SuggestionEditPresenter {
    
    var project: ProjectModel {
        return suggestion.project
    }
    
    //MARK: Injections
    var suggestion: SuggestionModel!
    var interactor: SuggestionEditInteractorInput!
    var coordinator: SuggestionEditCoordinatable! {
        didSet {
            if self.coordinator == nil { return }
            
            self.coordinator.onResetToDefault = { [weak self] suggestion in
                var newSuggestion = suggestion
                newSuggestion.prepareForEdited()
                self?.suggestion = newSuggestion
                self?.output?.reloadData()
            }
            
            self.coordinator.onUpdateProject = { [weak self] project in
                self?.suggestion.project = project
                self?.output?.reloadData()
            }
            
        }
    }
    private weak var output: SuggestionEditPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: SuggestionEditPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - SuggestionEditPresenterInput
extension SuggestionEditPresenter: SuggestionEditPresenterInput {

    func viewDidLoad() {
    }
    
    func sendProposalHandler() {
        suggestion.prepareForSend()
        if suggestion.id <= -1 {
            sendCreateSuggestion()
        } else {
            sendEditedSuggestion()
        }
    }
    
    func restoreToDefaultHandler() {
        coordinator.resetToDefaultHandler()
    }
    
    // Task hadnlers
    
    func suggestionEditHandler(_ suggestion: SuggestionModel) {
        self.suggestion = suggestion
    }
    
    func addTaskHandler() {
        let task = TaskModel(projectId: project.id)
        coordinator.addTaskHandler(task)
    }
    
    func editTaskHandler(_ task: TaskModel) {
        coordinator.editTaskHandler(task)
    }
    
    func deleteTaskHandler(_ task: TaskModel) {
        suggestion.project.deleteTask(task)
        output?.reloadData()
    }
    
    // Milestone handlers
    
    func addMilestoneHandler() {
        let milestone = MilestoneModel(index: project.milestones.count+1, projectId: project.id)
        coordinator.addMilestoneHandler(milestone)
    }
    
    func editMilestoneHandler(_ milestone: MilestoneModel) {
        coordinator.editMilestoneHandler(milestone)
    }
    
    func deleteMilestoneHandler(_ milestone: MilestoneModel) {
        suggestion.project.deleteMilestone(milestone)
        output?.reloadData()
    }
    
}

// MARK: - SuggestionEditInteractorOutput
extension SuggestionEditPresenter: SuggestionEditInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successSendSuggestion(_ suggestion: SuggestionModel) {
        output?.stopLoading()
        coordinator.sendProposalHandler(suggestion)
    }
    
}

// MARK: - Private
fileprivate extension SuggestionEditPresenter {
    
    func sendCreateSuggestion() {
        output?.startLoading()
        interactor.createSuggestion(suggestion)
    }
    
    func sendEditedSuggestion() {
        output?.startLoading()
        interactor.editSuggestion(suggestion)
    }
    
}

