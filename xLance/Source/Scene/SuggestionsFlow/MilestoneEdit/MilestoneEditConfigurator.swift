//
//  MilestoneEditConfigurator.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol MilestoneEditConfigurable {
}

class MilestoneEditConfigurator<T: MilestoneEditViewController>: BaseViewConfigurable, MilestoneEditConfigurable {
    
    let milestone: MilestoneModel
    let project: ProjectModel
    let networkProvider: Networking
    let coordinator: MilestoneEditCoordinatable
    
    required init(milestone: MilestoneModel, project: ProjectModel,
                  networkProvider: Networking, coordinator: MilestoneEditCoordinatable) {
        self.milestone = milestone
        self.project = project
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }

    //MARK: MilestoneEditConfigurable
    
    func create() -> MilestoneEditViewController {
        let viewController = MilestoneEditViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: MilestoneEditViewController) {
        
        let presenter = MilestoneEditPresenter(output: viewController)
        
        presenter.coordinator = coordinator
        presenter.project = project
        presenter.milestone = milestone
        
        viewController.presenter = presenter

    }
}
