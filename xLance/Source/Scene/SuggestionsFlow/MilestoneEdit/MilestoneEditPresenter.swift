//
//  MilestoneEditPresenter.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol MilestoneEditCoordinatable {
    func didUpdateProject(_ project: ProjectModel)
}

protocol MilestoneEditPresenterInput: BasePresenterInput {
    var project: ProjectModel! { get set }
    var milestone: MilestoneModel! { get set }
    var milestoneHistory: MilestoneHistoryModel { get set }    
    func onSubmit()
}

protocol MilestoneEditPresenterOutput: BasePresenterOutput {}

class MilestoneEditPresenter {
    
    //MARK: Injections
    var project: ProjectModel!
    var milestone: MilestoneModel!
    var milestoneHistory: MilestoneHistoryModel {
        get {
            return milestone.actualHistory
        }
        set {
            milestone.actualHistory = newValue
        }
    }
    var coordinator: MilestoneEditCoordinatable!
    private weak var output: MilestoneEditPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: MilestoneEditPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - MilestoneEditPresenterInput
extension MilestoneEditPresenter: MilestoneEditPresenterInput {
    
    func viewDidLoad() {
        
    }
    
    func onSubmit() {
        if !project.isValidMilestone(milestone) {
            output?.showInfoMessage("MILESTONE_DIALOG_COMPONENT.DAYS_FOR_REVIEW".localized)
            return
        }
        project.updateOrAddMilestone(milestone)
        coordinator.didUpdateProject(project)
    }
    
}

// MARK: - Private
fileprivate extension MilestoneEditPresenter {
    
}

