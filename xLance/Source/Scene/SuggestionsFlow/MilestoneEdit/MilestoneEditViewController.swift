//
//  MilestoneEditViewController.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

class MilestoneEditViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .milestoneEdit
    
    // MARK: Outlets
    @IBOutlet fileprivate var startDateTextField: ModyfingTextField!
    @IBOutlet fileprivate var tillDateTextField: ModyfingTextField!
    @IBOutlet fileprivate var daysForReviewTextField: ModyfingTextField!
    @IBOutlet fileprivate var submitButton: UIButton!
    
    fileprivate lazy var startDatePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.date = presenter.milestone.startDate
        picker.datePickerMode = .date
        return picker
    }()
    
    fileprivate lazy var tillDatePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.date = presenter.milestone.tillDate
        picker.datePickerMode = .date
        return picker
    }()
    
    // MARK: Injections
    var presenter: MilestoneEditPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    }
    
    // MARK: Actions
    
    @IBAction fileprivate func valueChangedHandler(_ sender: UITextField) {
        switch sender {
            
        case daysForReviewTextField:
            presenter.milestoneHistory.daysForReview = sender.text?.int ?? 0
            
        default: ()
        }
    }
    
    @IBAction fileprivate func submitButtonTapHandler(_ sender: UIButton) {
        presenter.onSubmit()
    }

}

// MARK: - MilestoneEditPresenterOutput
extension MilestoneEditViewController: MilestoneEditPresenterOutput {

}

// MARK: - Private
fileprivate extension MilestoneEditViewController {
    
    func setupUI() {
        startDateTextField.placeholder = "COMMON.START_DATE".localized
        tillDateTextField.placeholder = "COMMON.DATE_TILL".localized
        daysForReviewTextField.placeholder = "MILESTONE_DIALOG_COMPONENT.DAYS_FOR_REVIEW".localized
        submitButton.setTitle("COMMON.SAVE".localized, for: .normal)
        if presenter.milestoneHistory.status == .added {
            navigationItem.title = "TASK_LIST_COMPONENT.ADD_MILESTONE".localized
        } else {
            navigationItem.title = "TASK_LIST_COMPONENT.EDIT_MILESTONE".localized
        }
        addTapGestureWithEndEditing()
        startDatePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        startDateTextField.inputView = startDatePicker
        tillDateTextField.inputView = tillDatePicker
        tillDatePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        configure()
    }
    
    func configure() {
        daysForReviewTextField.text = presenter.milestoneHistory.daysForReview.string
        startDateTextField.text = presenter.milestoneHistory.startDate.shortWithSeparator
        tillDateTextField.text = presenter.milestoneHistory.tillDate.shortWithSeparator
    }
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        let date = datePicker.date
        switch datePicker {
            
        case startDatePicker:
            presenter.milestoneHistory.startDate = date
            startDateTextField.text = date.shortWithSeparator
            
        case tillDatePicker:
            presenter.milestoneHistory.tillDate = date
            tillDateTextField.text = date.shortWithSeparator
            
        default: ()
        }
    }
    
}
