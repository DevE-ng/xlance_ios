//
//  SuggestionDetailsInteractor.swift
//  xlance
//
//  Created by alobanov11 on 04/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol SuggestionDetailsInteractorInput: BaseInteractorInput {
    func acceptSuggestion(_ suggestion: SuggestionModel)
    func declineSuggestion(_ suggestion: SuggestionModel)
}
protocol SuggestionDetailsInteractorOutput: BaseInteractorOutput {
    func successAcceptSuggestion(project: ProjectModel)
    func successDeclineSuggestion()
    func errorHandler(_ error: String)
}

class SuggestionDetailsInteractor {
    
    var networkProvider: Networking!
    weak var presenter: SuggestionDetailsInteractorOutput?
    
}

// MARK: - SuggestionDetailsPresenterInput
extension SuggestionDetailsInteractor: SuggestionDetailsInteractorInput {
    
    func acceptSuggestion(_ suggestion: SuggestionModel) {
        networkProvider.acceptSuggestion(suggestion) { [weak self] in
            self?.handleAcceptSuggestion($0)
        }
    }
    
    func declineSuggestion(_ suggestion: SuggestionModel) {
        networkProvider.declineSuggestion(suggestion.id) { [weak self] in
            self?.handleDeclineSuggestion($0)
        }
    }
    
}

// MARK: - Private
fileprivate extension SuggestionDetailsInteractor {
    
    func handleAcceptSuggestion(_ result: Result<ProjectModel>) {
        switch result {
        case .Success(let project):
            presenter?.successAcceptSuggestion(project: project)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleDeclineSuggestion(_ result: Result<Bool>) {
        switch result {
        case .Success(_):
            presenter?.successDeclineSuggestion()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

