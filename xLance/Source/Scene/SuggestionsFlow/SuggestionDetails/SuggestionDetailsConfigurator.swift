//
//  SuggestionDetailsConfigurator.swift
//  xlance
//
//  Created by alobanov11 on 04/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol SuggestionDetailsConfigurable {}

class SuggestionDetailsConfigurator<T: SuggestionDetailsViewController>: BaseViewConfigurable, SuggestionDetailsConfigurable {
    
    private let suggestion: SuggestionModel
    private let networkProvider: Networking
    private let coordinator: SuggestionDetailsCoordinatable
    
    required init(suggestion: SuggestionModel,
                  networkProvider: Networking,
                  coordinator: SuggestionDetailsCoordinatable) {
        self.suggestion = suggestion
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: SuggestionDetailsConfigurable
    
    func create() -> SuggestionDetailsViewController {
        let viewController = SuggestionDetailsViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: SuggestionDetailsViewController) {
        
        let interactor = SuggestionDetailsInteractor()
        let presenter = SuggestionDetailsPresenter(output: viewController)
        
        presenter.suggestion = suggestion
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.presenter = presenter
        interactor.networkProvider = networkProvider
        
        viewController.presenter = presenter
        
    }
}
