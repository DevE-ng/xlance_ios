//
//  SuggestionDetailsViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator

class SuggestionDetailsViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .suggestionDetails
    
    // MARK: Outlets
    fileprivate var cells: [UITableViewCell.Type] = []
    fileprivate var milestones: [MilestoneModel] = []
    fileprivate var tasks: [TaskModel] = []
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView!
    
    // MARK: Injections
    var presenter: SuggestionDetailsPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }

}

// MARK: - SuggestionDetailsPresenterOutput
extension SuggestionDetailsViewController: SuggestionDetailsPresenterOutput {
    
    func reloadData() {
        reloadCells()
        tableView.reloadData()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .bottom, offset: 30)
        ])
    }
    
}

extension SuggestionDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = cells[indexPath.row]
        switch true {
            
        case cellType == SuggestionTopTableViewCell.self:
            let topCell = tableView.dequeueReusableCell(with: SuggestionTopTableViewCell.self, for: indexPath)
            configureTopCell(topCell)
            return topCell
            
        case cellType == MilestoneTableViewCell.self:
            let milestoneCell = tableView.dequeueReusableCell(with: MilestoneTableViewCell.self, for: indexPath)
            configureMilestoneCell(milestoneCell)
            return milestoneCell
            
        case cellType == TaskTableViewCell.self:
            let taskCell = tableView.dequeueReusableCell(with: TaskTableViewCell.self, for: indexPath)
            configureTaskCell(taskCell)
            return taskCell
            
        case cellType == SuggestionBottomButtonsTableViewCell.self:
            let buttonsCell = tableView.dequeueReusableCell(with: SuggestionBottomButtonsTableViewCell.self, for: indexPath)
            configureBottomCell(buttonsCell)
            return buttonsCell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

// MARK: - Private
fileprivate extension SuggestionDetailsViewController {
    
    func setupUI() {
        navigationItem.title = "SUGGESTION_DIALOG_COMPONENT.VIEW_STEP_\(CurrentUser.roleSuffix)".localized
        tableView.contentInset = .init(top: 8, left: 0, bottom: 0, right: 0)
        tableView.register(cellType: SuggestionTopTableViewCell.self)
        tableView.register(cellType: MilestoneTableViewCell.self)
        tableView.register(cellType: TaskTableViewCell.self)
        tableView.register(cellType: SuggestionBottomButtonsTableViewCell.self)
        reloadCells()
    }
    
    func reloadCells() {
        milestones.removeAll()
        tasks.removeAll()
        cells.removeAll()
        cells.append(SuggestionTopTableViewCell.self)
        
        if presenter.project.paymentMethod == .hourly {
            let _ = presenter.project.tasks.map({
                cells.append(TaskTableViewCell.self)
                tasks.append($0)
            })
            
        } else {
            let _ = presenter.project.milestones.map({ milestone in
                cells.append(MilestoneTableViewCell.self)
                milestones.append(milestone)
                let _ = milestone.tasks.map({
                    cells.append(TaskTableViewCell.self)
                    tasks.append($0)
                })
            })
        }
        
        cells.append(SuggestionBottomButtonsTableViewCell.self)
        
    }
    
    func configureTopCell(_ cell: SuggestionTopTableViewCell) {
        cell.suggestion = presenter.suggestion
    }
    
    func configureMilestoneCell(_ cell: MilestoneTableViewCell) {
        cell.milestone = milestones.remove(at: 0)
    }
    
    func configureTaskCell(_ cell: TaskTableViewCell) {
        cell.task = tasks.remove(at: 0)
    }
    
    func configureBottomCell(_ cell: SuggestionBottomButtonsTableViewCell) {
        cell.suggestion = presenter.suggestion
        cell.onDecline = { [weak self] in
            self?.presenter.declineHandler()
        }
        cell.onEditOffer = { [weak self] in
            self?.presenter.editOfferHandler()
        }
        cell.onAcceptNewTerms = { [weak self] in
            self?.presenter.acceptNewTermsHandler()
        }
    }
    
}
