//
//  SuggestionDetailsPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol SuggestionDetailsCoordinatable {
    func acceptNewTermsHandler(_ project: ProjectModel)
    func editOfferHandler(_ suggestion: SuggestionModel)
    func declineHandler(_ project: ProjectModel)
}

protocol SuggestionDetailsPresenterInput: BasePresenterInput {
    var project: ProjectModel { get }
    var suggestion: SuggestionModel! { get set }
    func acceptNewTermsHandler()
    func editOfferHandler()
    func declineHandler()
}

protocol SuggestionDetailsPresenterOutput: BasePresenterOutput {
    func reloadData()
}

class SuggestionDetailsPresenter {
    
    var project: ProjectModel {
        return suggestion.project
    }
    
    private var originalSuggestion: SuggestionModel!
    
    //MARK: Injections
    var suggestion: SuggestionModel! {
        didSet {
            originalSuggestion = suggestion
        }
    }
    var interactor: SuggestionDetailsInteractorInput!
    var coordinator: SuggestionDetailsCoordinatable!
    private weak var output: SuggestionDetailsPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: SuggestionDetailsPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - SuggestionDetailsPresenterInput
extension SuggestionDetailsPresenter: SuggestionDetailsPresenterInput {
    
    func viewDidLoad() {
        suggestion.prepareForView()
    }
    
    func acceptNewTermsHandler() {
        originalSuggestion.prepareForSend()
        output?.startLoading()
        interactor.acceptSuggestion(suggestion)
    }
    
    func editOfferHandler() {
        var newSuggestion: SuggestionModel = originalSuggestion
        newSuggestion.prepareForEdited()
        coordinator.editOfferHandler(newSuggestion)
    }
    
    func declineHandler() {
        output?.startLoading()
        interactor.declineSuggestion(suggestion)
    }
    
}

// MARK: - SuggestionDetailsInteractorOutput
extension SuggestionDetailsPresenter: SuggestionDetailsInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successAcceptSuggestion(project: ProjectModel) {
        output?.stopLoading()
        coordinator.acceptNewTermsHandler(project)
    }
    
    func successDeclineSuggestion() {
        self.output?.stopLoading()
        coordinator.declineHandler(suggestion.project)
    }
    
}

// MARK: - Private
fileprivate extension SuggestionDetailsPresenter {
    
}

