//
//  CallPresenter.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import UIKit
import AVKit

protocol CallCoordinatable {
    func didFinishCall()
}

protocol CallPresenterInput: BasePresenterInput {
    var user: ProfileModel! { get set }
    func finishCall()
    func toggleVideo()
    func toggleAudio()
    func toggleSound()
}

protocol CallPresenterOutput: BasePresenterOutput, CallingDelegate {
    func videoOn()
    func videoOff()
    func microOn()
    func microOff()
    func soundOn()
    func soundOff()
}

class CallPresenter {
    
    //MARK: Injections
    var user: ProfileModel!
    var interactor: CallInteractorInput!
    var coordinator: CallCoordinatable!
    private weak var output: CallPresenterOutput?
    
    //MARK: LifeCycle
    init(output: CallPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - CallPresenterInput
extension CallPresenter: CallPresenterInput {
    
    func viewDidLoad() {
        requestCameraAccess { [unowned self] answer in
            if answer {
                DispatchQueue.main.async {
                    self.interactor.call(self.user.rabbitmqHash.string)
                }
            } else {
                DispatchQueue.main.async {
                    self.coordinator.didFinishCall()
                }
            }
        }
    }
    
    func finishCall() {
        interactor.finishCall()
        coordinator.didFinishCall()
    }
    
    func toggleVideo() {
        guard let isToggle = interactor.toggleVideo() else { return }
        if isToggle {
            output?.videoOn()
        } else {
            output?.videoOff()
        }
    }
    
    func toggleAudio() {
        guard let isToggle = interactor.toggleAudio() else { return }
        if isToggle {
            output?.microOn()
        } else {
            output?.microOff()
        }
    }
    
    func toggleSound() {
        guard let isToggle = interactor.toggleSound() else { return }
        if isToggle {
            output?.soundOn()
        } else {
            output?.soundOff()
        }
    }
    
}

extension CallPresenter: CallInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.showInfoMessage(error)
    }
    
    func successCall() {
        output?.showInfoMessage("RTC.STATUS.CONNECTING".localized)
    }
    
}


// MARK: - Private
fileprivate extension CallPresenter {
    
    func requestCameraAccess(completionHandler: @escaping (Bool) -> Void) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            completionHandler(response)
        }
    }
    
}
