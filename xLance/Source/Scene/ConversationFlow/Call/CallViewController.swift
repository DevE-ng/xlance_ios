//
//  CallController.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright (c) 2019 Engineers. All rights reserved.
//

import UIKit
import WebRTC


class CallViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .call
    
    // MARK: Outlets
    @IBOutlet fileprivate var remoteVideoStackView: UIStackView!
    @IBOutlet fileprivate var localVideoView: UIView!
    @IBOutlet fileprivate var finishButton: UIButton!
    @IBOutlet fileprivate var soundButton: UIButton!
    @IBOutlet fileprivate var microButton: UIButton!
    @IBOutlet fileprivate var videoButton: UIButton!
    
    // MARK: Injections
    var presenter: CallPresenterInput!
    
    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    @IBAction fileprivate func soundButtonTapHandler(_ sender: Any) {
        presenter.toggleSound()
    }
    
    @IBAction fileprivate func videoButtonTapHandler(_ sender: Any) {
        presenter.toggleVideo()
    }
    
    @IBAction fileprivate func microButtonTapHandler(_ sender: Any) {
        presenter.toggleAudio()
    }
    
    @IBAction fileprivate func finishButtonTapHandler(_ sender: Any) {
        presenter.finishCall()
    }
    
}

// MARK: - CallPresenterOutput
extension CallViewController: CallPresenterOutput {
        
    func videoOn() {
        localVideoView.isHidden = false
        videoButton.alpha = 1
    }
    
    func videoOff() {
        localVideoView.isHidden = true
        videoButton.alpha = Theme.disabledAlpha
    }
    
    func microOn() {
        microButton.alpha = Theme.disabledAlpha
    }
    
    func microOff() {
        microButton.alpha = 1
    }
    
    func soundOn() {
        soundButton.alpha = 1
    }
    
    func soundOff() {
        soundButton.alpha = Theme.disabledAlpha
    }
    
}

// MARK: - CallingDelegate
extension CallViewController: CallingDelegate {
    
    func disconnect() {
        presenter.finishCall()
    }
    
    func localVideoSize() -> CGRect {
        return localVideoView.bounds
    }
    
    func countRemoteViews() -> Int {
        return remoteVideoStackView.subviews.count
    }
    
    func createRemoteView() -> (index: Int, view: UIView) {
        let view = UIView()
        view.backgroundColor = .black
        remoteVideoStackView.addArrangedSubview(view)
        self.view.layoutIfNeeded()
        return (view: view, index: remoteVideoStackView.subviews.count - 1)
    }
    
    func addLocalView(view: UIView) {
        localVideoView.addSubview(view)
    }
    
}
