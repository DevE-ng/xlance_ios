//
//  CallConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol CallConfigurable {
}

class CallConfigurator<T: CallViewController>: BaseViewConfigurable, CallConfigurable {
    
    private let user: ProfileModel!
    private let callProvider: Calling
    private let networkProvider: Networking
    private let coordinator: CallCoordinatable
    
    required init(user: ProfileModel, callProvider: Calling, networkProvider: Networking, coordinator: CallCoordinatable) {
        self.user = user
        self.callProvider = callProvider
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: CallConfigurable
    
    func create() -> CallViewController {
        let viewController = CallViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: CallViewController) {
        
        let interactor = CallInteractor()
        let presenter = CallPresenter(output: viewController)
        
        presenter.user = user
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.callProvider = callProvider
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        callProvider.delegate = viewController
        viewController.presenter = presenter
        
    }
}
