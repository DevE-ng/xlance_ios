//
//  CallInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol CallInteractorInput: BaseInteractorInput {
    func call(_ user: String)
    func finishCall()
    func toggleVideo() -> Bool?
    func toggleAudio() -> Bool?
    func toggleSound() -> Bool?
}
protocol CallInteractorOutput: BaseInteractorOutput {
    func errorHandler(_ error: String)
    func successCall()
}

class CallInteractor {
    
    var networkProvider: Networking!
    var callProvider: Calling!
    weak var presenter: CallInteractorOutput?
    
}

// MARK: - CallPresenterInput
extension CallInteractor: CallInteractorInput {
    
    func call(_ user: String) {
        callProvider.call(user: user) { [weak self] in
            self?.handleCall($0)
        }
    }
    
    func finishCall() {
        callProvider.finishCall()
    }
    
    func toggleVideo() -> Bool? {
        return callProvider.toggleVideo()
    }
    
    func toggleAudio() -> Bool? {
        return callProvider.toggleAudio()
    }
    
    func toggleSound() -> Bool? {
        return callProvider.toggleSound()
    }
    
}

// MARK: - Private
fileprivate extension CallInteractor {
    
    func handleCall(_ result: Result<Bool>) {
        switch result {
            
        case .Success(_):
            presenter?.successCall()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

