//
//  ChatDetailsInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ChatDetailsInteractorInput: BaseInteractorInput {
    func fetchMessages(conversationId: Int)
    func sendMessage(_ message: String, project: ProjectModel)
}
protocol ChatDetailsInteractorOutput: BaseInteractorOutput {
    func errorHandler(_ error: String)
    func successFetchMessages(_ messages: [MessageModel])
    func newMessageHandler(_ message: MessageModel)
}

class ChatDetailsInteractor {
    
    var project: ProjectModel!
    var chatProvider: Chating! {
        didSet {
            if chatProvider == nil { return }
            chatProvider.subscribeOn(project: project, delegate: self)
        }
    }
    var networkProvider: Networking!
    weak var presenter: ChatDetailsInteractorOutput?
    
    deinit {
        chatProvider.subscribeOff(project: project)
    }
    
}

// MARK: - ChatDetailsPresenterInput
extension ChatDetailsInteractor: ChatDetailsInteractorInput {
    
    func fetchMessages(conversationId: Int) {
        networkProvider.fetchMessages(conversationId: conversationId) { [weak self] in
            self?.handleFetchMessages($0)
        }
    }
    
    func sendMessage(_ message: String, project: ProjectModel) {
        chatProvider.sendMessage(text: message, project: project)
    }
    
    func subscribeOn(_ project: ProjectModel, delegate: ChattingDelegate) {
        chatProvider.subscribeOn(project: project, delegate: delegate)
    }
    
    func subscribeOff(_ project: ProjectModel) {
        chatProvider.subscribeOff(project: project)
    }
    
}

// MARK: - ChattingDelegate
extension ChatDetailsInteractor: ChattingDelegate {
    func didReceiveMessage(_ message: MessageSimpleModel) {
        presenter?.newMessageHandler(MessageModel.makeFrom(message))
    }
}

// MARK: - Private
fileprivate extension ChatDetailsInteractor {
    
    func handleFetchMessages(_ result: Result<[MessageModel]>) {
        switch result {
            
        case .Success(let messages):
            presenter?.successFetchMessages(messages)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

