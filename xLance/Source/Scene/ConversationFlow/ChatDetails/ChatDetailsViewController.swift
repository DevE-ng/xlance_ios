//
//  ChatDetailsViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator
import MobileCoreServices

class ChatDetailsViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .chatDetails
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView?
    @IBOutlet fileprivate var messageTextField: UITextField!
    @IBOutlet fileprivate var textViewBottomAnchor: NSLayoutConstraint!
    
    // MARK: Injections
    var presenter: ChatDetailsPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    }
    
    deinit {
        stopAvoidingKeyboard()
    }
    
    // MARK: Actions
    @IBAction fileprivate func callButtonTapHandler(_ sender: Any) {
        presenter.callHandler()
    }
    
    @IBAction fileprivate func sendButtonTapHandler(_ sender: Any) {
        if let message = messageTextField.text {
            messageTextField.text = ""
            presenter.sendMessage(message)
        }
    }
    
    @IBAction fileprivate func attachButtonTapHandler(_ sender: Any) {
        let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet]
        let importMenu = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        
        if #available(iOS 11.0, *) {
            importMenu.allowsMultipleSelection = true
        }
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        
        present(importMenu, animated: true)
    }
    
}

// MARK: - ChatDetailsPresenterOutput
extension ChatDetailsViewController: ChatDetailsPresenterOutput {
    func reloadData() {
        guard let tableView = tableView else { return }
        tableView.reloadData()
        tableView.scrollToTop()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .top, offset: 30)
        ])
    }
    func reloadDataFirst() {
        guard let tableView = tableView else { return }
        tableView.reloadData()
        tableView.scrollToTop()
        if let view = tableView.cellForRow(at: .init(row: 0, section: 0)) {
            UIView.animate(views: [view], animations: [
                AnimationType.from(direction: .top, offset: 30)
            ])
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ChatDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = presenter.messages[indexPath.row]
        var cell: UITableViewCell
        switch true {
            
        case message.file != nil:
            let fileCell = tableView.dequeueReusableCell(with: FileTableViewCell.self, for: indexPath)
            fileCell.message = message
            fileCell.onSelect = { [weak self] in
                self?.openFile(by: message)
            }
            cell = fileCell
            
        default:
            let msgCell = tableView.dequeueReusableCell(with: MessageTableViewCell.self, for: indexPath)
            msgCell.message = message
            cell = msgCell
            
        }
        
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        return cell
    }
}

// MARK: - UIDocumentPickerDelegate
extension ChatDetailsViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Private
fileprivate extension ChatDetailsViewController {
    
    func setupUI() {
        messageTextField.placeholder = "CHAT_COMPONENT.MESSAGE".localized
        navigationItem.title = presenter.project.companion?.fullName
        tableView?.register(cellType: MessageTableViewCell.self)
        tableView?.register(cellType: FileTableViewCell.self)
        tableView?.contentInset = .init(top: 8, left: 0, bottom: 8, right: 0)
        tableView?.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi))
        tableView?.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: view.bounds.size.width - 8.0)
        addTapGestureWithEndEditing()
        startAvoidingKeyboard()
    }
    
    func openFile(by message: MessageModel) {
        guard
            let file = message.file,
            let id = message.fileId,
            let url = URL(string: "\(CONSTANTS.xlance_full_url_with_port)/api/files/\(id)/\(file)") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}
