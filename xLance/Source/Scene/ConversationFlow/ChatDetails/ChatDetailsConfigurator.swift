//
//  ChatDetailsConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ChatDetailsConfigurable {
}

class ChatDetailsConfigurator<T: ChatDetailsViewController>: BaseViewConfigurable, ChatDetailsConfigurable {
    
    private let project: ProjectModel
    private let chatProvider: Chating
    private let networkProvider: Networking
    private let coordinator: ChatDetailsCoordinatable
    
    required init(project: ProjectModel, chatProvider: Chating, networkProvider: Networking, coordinator: ChatDetailsCoordinatable) {
        self.project = project
        self.chatProvider = chatProvider
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: ChatDetailsConfigurable
    
    func create() -> ChatDetailsViewController {
        let viewController = ChatDetailsViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: ChatDetailsViewController) {
        
        let interactor = ChatDetailsInteractor()
        let presenter = ChatDetailsPresenter(output: viewController)
                
        presenter.project = project
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.project = project
        interactor.chatProvider = chatProvider
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
