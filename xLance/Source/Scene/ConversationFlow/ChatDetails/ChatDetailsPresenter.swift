//
//  ChatDetailsPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ChatDetailsCoordinatable {
    func callHandler(user: ProfileModel)
}

protocol ChatDetailsPresenterInput: BasePresenterInput {
    var messages: [MessageModel] { get set }
    var project: ProjectModel! { get set }
    func callHandler()
    func sendMessage(_ message: String)
}

protocol ChatDetailsPresenterOutput: BasePresenterOutput {
    func reloadData()
    func reloadDataFirst()
}

class ChatDetailsPresenter {
    
    var messages: [MessageModel] = []
    
    //MARK: Injections
    var project: ProjectModel!
    var interactor: ChatDetailsInteractorInput!
    var coordinator: ChatDetailsCoordinatable!
    private weak var output: ChatDetailsPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: ChatDetailsPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - ChatDetailsPresenterInput
extension ChatDetailsPresenter: ChatDetailsPresenterInput {
    
    func viewDidLoad() {
        fetchMessages()
    }
    
    func fetchMessages() {
        output?.startLoading()
        interactor.fetchMessages(conversationId: project.conversationId!)
    }
    
    func callHandler() {
        coordinator.callHandler(user: project.companion!)
    }
    
    func sendMessage(_ message: String) {
        interactor.sendMessage(message, project: project)
    }
    
}

// MARK: - ChatDetailsInteractorOutput
extension ChatDetailsPresenter: ChatDetailsInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successFetchMessages(_ messages: [MessageModel]) {
        output?.stopLoading()
        self.messages = messages
        output?.reloadData()
    }
    
    func newMessageHandler(_ message: MessageModel) {
        messages.insert(message, at: 0)
        output?.reloadDataFirst()
    }
    
}

// MARK: - Private
fileprivate extension ChatDetailsPresenter {}

