//
//  ChatListViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit
import ViewAnimator

protocol ChatListViewProtocol {}

class ChatListViewController: BaseViewController, StoryboardLoadable, ChatListViewProtocol {
    
    static var storyboardName: Storyboards = .chatList
    
    // MARK: Outlets
    @IBOutlet fileprivate var tableView: UITableView?
    fileprivate var refreshControl = UIRefreshControl()
    
    // MARK: Injections
    var presenter: ChatListPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.viewDidLoad()
    }

}

// MARK: - ChatListPresenterOutput
extension ChatListViewController: ChatListPresenterOutput {
    func reloadData() {
        guard let tableView = tableView else { return }
        refreshControl.endRefreshing()
        tableView.reloadData()
        UIView.animate(views: tableView.visibleCells, animations: [
            AnimationType.from(direction: .bottom, offset: 30)
        ])
    }
    
    func reloadCell(row: Int) {
        let indexPath = IndexPath(row: row, section: 0)
        tableView?.reloadRows(at: [indexPath], with: .automatic)
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ChatListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ChatTableViewCell.self, for: indexPath)
        cell.project = presenter.projects[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(indexPath.row)
    }
}

// MARK: - Private
fileprivate extension ChatListViewController {
    
    func setupUI() {
        navigationItem.title = "CHAT_LIST".localized
        refreshControl.alpha = 0
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView?.refreshControl = refreshControl
        tableView?.contentInset = .init(top: 8, left: 0, bottom: 0, right: 0)
        tableView?.register(cellType: ChatTableViewCell.self)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        presenter.fetchProjects()
    }
    
}
