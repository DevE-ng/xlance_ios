//
//  ChatListPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ChatListCoordinatable {
    func didSelectProject(_ project: ProjectModel)
}

protocol ChatListPresenterInput: BasePresenterInput {
    var projects: [ProjectModel] { get set }
    func fetchProjects()
    func didSelectRow(_ row: Int)
}

protocol ChatListPresenterOutput: BasePresenterOutput {
    func reloadData()
    func reloadCell(row: Int)
}

class ChatListPresenter {
    
    var projects: [ProjectModel] = []
    
    //MARK: Injections
    var interactor: ChatListInteractorInput!
    var coordinator: ChatListCoordinatable!
    private weak var output: ChatListPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: ChatListPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - ChatListPresenterInput
extension ChatListPresenter: ChatListPresenterInput {
    
    func viewDidLoad() {
        fetchProjects()
    }
    
    func fetchProjects() {
        output?.startLoading()
        interactor.fetchProjects()
    }
    
    func didSelectRow(_ row: Int) {
        let project = projects[row]
        coordinator.didSelectProject(project)
    }
    
}

// MARK: - ChatListInteractorOutput
extension ChatListPresenter: ChatListInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successFetchProjects(_ projects: [ProjectModel]) {
        output?.stopLoading()
        self.projects = projects
        output?.reloadData()
    }
    
    func updateStatuses(_ statuses: [StatusModel]) {
        for status in statuses {
            for (index, project) in projects.enumerated() {
                if status.profileId != project.companion?.id { continue }
                projects[index].online = status.online
                DispatchQueue.main.async {
                    self.output?.reloadCell(row: index)
                }
            }
        }
    }
}

// MARK: - Private
fileprivate extension ChatListPresenter {

}

