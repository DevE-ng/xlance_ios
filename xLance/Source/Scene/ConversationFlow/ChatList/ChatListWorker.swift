//
//  ChatListWorker.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ChatListWorker {
    var onUpdateStatuses: ( ([StatusModel]) -> Void )? { get set }
    func startUpdateStatuses(in projects: [ProjectModel])
}

class ChatListWorkerImp {
    
    var onUpdateStatuses: (([StatusModel]) -> Void)?
    let myProjectsWorker: MyProjectsWorker
    
    private var projects = [ProjectModel]()
    private let updateStatusesTime: TimeInterval = 15
    private var fetchStatusesTimer: Timer?
    
    init(myProjectsWorker: MyProjectsWorker) {
        self.myProjectsWorker = myProjectsWorker
    }
    
    deinit {
        fetchStatusesTimer?.invalidate()
    }
    
}

// MARK: - ChatListWorker
extension ChatListWorkerImp: ChatListWorker {
    
    func startUpdateStatuses(in projects: [ProjectModel]) {
        self.projects = projects
        fetchStatusesTimer?.invalidate()
        fetchStatusesTimer = Timer.scheduledTimer(withTimeInterval: updateStatusesTime, repeats: true) { [weak self] (_) in
            DispatchQueue.global(qos: .userInteractive).async {
                self?.fetchAndUpdateStatuses()
            }
        }
        fetchAndUpdateStatuses()
    }
    
}

// MARK: - Private
fileprivate extension ChatListWorkerImp {
    
    func fetchAndUpdateStatuses() {
        myProjectsWorker.fetchProjectsStatuses(projects) { [weak self] in
            guard let statuses = $0.value else { return }
            self?.onUpdateStatuses?(statuses)
        }
    }
    
}
