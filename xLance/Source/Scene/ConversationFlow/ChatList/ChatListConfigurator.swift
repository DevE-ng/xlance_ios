//
//  ChatListConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ChatListConfigurable {
}

class ChatListConfigurator<T: ChatListViewController>: BaseViewConfigurable, ChatListConfigurable {
    
    private let networkProvider: Networking
    private let coordinator: ChatListCoordinatable
    
    required init(networkProvider: Networking, coordinator: ChatListCoordinatable) {
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: ChatListConfigurable
    
    func create() -> ChatListViewController {
        let viewController = ChatListViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: ChatListViewController) {
        
        let interactor = ChatListInteractor()
        let presenter = ChatListPresenter(output: viewController)
        
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
