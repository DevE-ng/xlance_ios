//
//  ChatListInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ChatListInteractorInput: BaseInteractorInput {
    func fetchProjects()
}
protocol ChatListInteractorOutput: BaseInteractorOutput {
    func errorHandler(_ error: String)
    func successFetchProjects(_ projects: [ProjectModel])
    func updateStatuses(_ statuses: [StatusModel])
}

class ChatListInteractor {
    
    var networkProvider: Networking!
    weak var presenter: ChatListInteractorOutput?
    
    private lazy var myProjectsWorker: MyProjectsWorker = {
        return MyProjectsWorkerImp(projectsWorker: ProjectsWorkerImp(networkProvider: networkProvider),
                                   suggestionsWorker: SuggestionWorkerImp(networkProvider: networkProvider))
    }()
    
    private lazy var worker: ChatListWorker = {
        let worker = ChatListWorkerImp(myProjectsWorker: myProjectsWorker)
        
        worker.onUpdateStatuses = { [weak self] in
            self?.presenter?.updateStatuses($0)
        }
        
        return worker
    }()
    
}

// MARK: - ChatListPresenterInput
extension ChatListInteractor: ChatListInteractorInput {
    
    func fetchProjects() {
        myProjectsWorker.fetchMyProjects { [weak self] in
            self?.presenter?.successFetchProjects($0)
            self?.worker.startUpdateStatuses(in: $0)
        }
    }
    
}

// MARK: - Private
fileprivate extension ChatListInteractor {}

