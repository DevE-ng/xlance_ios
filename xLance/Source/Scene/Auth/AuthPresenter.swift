//
//  AuthPresenter.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

enum AuthScene: String {
    
    case login = "Login"
    case register = "Register"
    case forgot = "Forgot"
    case changePassword = "Update password"
    
}

protocol AuthCoordinatable {
    
    func userPerformedAuthentication()
    func userPerformedAuthentication(isAnonymous: Bool)
    func userPerformedChangePassword()
    
}

protocol AuthPresenterInput: BasePresenterInput {
    
    var authData: AuthModel { get set }
    
    func anonymousHandler()
    func loginHandler(_ data: AuthModel)
    func registerHandler(_ data: AuthModel)
    func forgotHandler(_ data: AuthModel)
    func changePasswordHandler(_ data: AuthModel)
    
    func categoriesHandler()
    func roleHandler()
    
}

protocol AuthPresenterOutput: BasePresenterOutput {
    
    func loadScene(_ action: AuthScene)
    func loadScene(_ action: AuthScene, hideBar: Bool)
    func reloadData()
    
}

class AuthPresenter {
    
    var authData: AuthModel = AuthModel()
    
    //MARK: Injections
    var interactor: AuthInteractorInput!
    var coordinator: AuthCoordinatable!
    var scene: AuthScene!
    private weak var output: AuthPresenterOutput?
    
    //MARK: LifeCycle 
    init(output: AuthPresenterOutput) {
        self.output = output
    }
    
}

// MARK: - AuthPresenterInput
extension AuthPresenter: AuthPresenterInput {
    
    func viewDidLoad() {
        output?.loadScene(scene, hideBar: scene == .changePassword)
    }
    
    func anonymousHandler() {
        interactor.anonymous()
    }
    
    func loginHandler(_ data: AuthModel) {
        authData = data
        switch scene {
        case .login?:
            output?.startLoading()
            interactor.login(authData)
            
        case .register?, .forgot?:
            scene = .login
            output?.loadScene(.login)
            
        default: ()
        }
    }
    
    func registerHandler(_ data: AuthModel) {
        authData = data
        switch scene {
        case .register?:
            output?.startLoading()
            interactor.register(authData)
            
        case .login?, .forgot?:
            scene = .register
            output?.loadScene(.register)
            
        default: ()
        }
    }
    
    func forgotHandler(_ data: AuthModel) {
        authData = data
        switch scene {
        case .forgot?:
            output?.startLoading()
            interactor.forgot(authData)
            
        case .login?, .register?:
            scene = .forgot
            output?.loadScene(.forgot)
            
        default: ()
        }
    }
    
    func changePasswordHandler(_ data: AuthModel) {
        authData = data
        output?.startLoading()
        interactor.changePassword(authData)
    }
    
    func categoriesHandler() {
        interactor.fetchCategories { [unowned self] categories in
            SelectTableViewController.show(source: self.output,
                                           list: categories.map({ $0.code.localized }),
                                           onSelectItems: { [weak self] indicies in
                                                self?.authData.categories = categories.selectedItems(indicies: indicies)
                                                self?.output?.reloadData()
                                           },
                                           selectedItems: categories.selectedItems(from: self.authData.categories),
                                           allowMultiplySelection: true)
        }
        
        
    }
    
    func roleHandler() {
        interactor.fetchRoles { [unowned self] roles in
            SelectTableViewController.show(source: self.output,
                                           list: roles.map({ $0.localized }),
                                           onSelectItems: { [weak self] indicies in
                                                if let role = roles.selectedItems(indicies: indicies).first {
                                                    self?.authData.role = role
                                                }
                                                self?.output?.reloadData()
                                           },
                                           selectedItems: roles.selectedItems(from: [self.authData.role]),
                                           allowMultiplySelection: false)
        }
    }
    
}

// MARK: - AuthInteractorOutput
extension AuthPresenter: AuthInteractorOutput {
    
    func errorHandler(_ error: String) {
        output?.stopLoading()
        output?.showErrorMessage(error)
    }
    
    func successAnonymousHandler() {
        output?.stopLoading()
        coordinator.userPerformedAuthentication(isAnonymous: true)
    }
    
    func successLoginHandler() {
        output?.stopLoading()
        coordinator.userPerformedAuthentication()
    }
    
    func successRegisterHandler() {
        output?.stopLoading()
        output?.showInfoMessage("FORGOT_COMPONENT.EMAIL_SENT".localized)
        loginHandler(authData)
    }
    
    func successForgotHandler() {
        output?.stopLoading()
        output?.showInfoMessage("FORGOT_COMPONENT.EMAIL_SENT".localized)
        loginHandler(authData)
    }
    
    func successChangePasswordHandler() {
        output?.stopLoading()
        output?.showInfoMessage("FORGOT_COMPONENT.CHANGED_SUCCESS".localized)
        coordinator.userPerformedChangePassword()
    }
    
}

// MARK: - Private
fileprivate extension AuthPresenter {
}

