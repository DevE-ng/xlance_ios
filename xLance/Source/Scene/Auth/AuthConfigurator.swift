//
//  AuthConfigurator.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol AuthConfigurable {
}

class AuthConfigurator<T: AuthViewController>: BaseViewConfigurable, AuthConfigurable {
    
    private let scene: AuthScene
    private let networkProvider: Networking
    private let coordinator: AuthCoordinatable
    
    required init(scene: AuthScene, networkProvider: Networking, coordinator: AuthCoordinatable) {
        self.scene = scene
        self.networkProvider = networkProvider
        self.coordinator = coordinator
    }
    
    //MARK: AuthConfigurable
    
    func create() -> AuthViewController {
        let viewController = AuthViewController.create()
        configure(viewController: viewController)
        return viewController
    }
    
    func configure(viewController: AuthViewController) {
        
        let interactor = AuthInteractor()
        let presenter = AuthPresenter(output: viewController)
        
        presenter.scene = scene
        presenter.interactor = interactor
        presenter.coordinator = coordinator
        
        interactor.networkProvider = networkProvider
        interactor.presenter = presenter
        
        viewController.presenter = presenter
        
    }
}
