//
//  AuthViewController.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import UIKit

class AuthViewController: BaseViewController, StoryboardLoadable {
    
    static var storyboardName: Storyboards = .auth
    
    fileprivate var scenes = [AuthScene: AuthViewable]()
    fileprivate var currentScene: AuthViewable!
    
    // MARK: Outlets
    @IBOutlet fileprivate var sceneView: UIView!
    
    
    // MARK: Injections
    var presenter: AuthPresenterInput!

    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupUI()
    }
    
    deinit {
        stopAvoidingKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction fileprivate func anonymousButtonTapHandler(_ sender: Any) {
        presenter.anonymousHandler()
    }
    

}

// MARK: - AuthPresenterOutput
extension AuthViewController: AuthPresenterOutput {
    
    func reloadData() {
        currentScene.data = presenter.authData
    }
    
    func loadScene(_ action: AuthScene) {
        loadScene(action, hideBar: false)
    }

    func loadScene(_ action: AuthScene, hideBar: Bool) {
        let scene = scenes[action] != nil ? scenes[action]! : createScene(action)
        if currentScene != nil { currentScene.removeFromSuperview() }
        
        navigationItem.title = scene.title.localized
        if hideBar { navigationItem.setRightBarButtonItems(nil, animated: false) }
        
        currentScene = scene
        currentScene.data = presenter.authData
        currentScene.frame = .init(x: 0, y: 0, width: sceneView.frame.width, height: sceneView.frame.height)
        
        currentScene.onLogin = { [weak self] data in
            self?.presenter.loginHandler(data)
        }
        currentScene.onRegister = { [weak self] data in
            self?.presenter.registerHandler(data)
        }
        currentScene.onForgot = { [weak self] data in
            self?.presenter.forgotHandler(data)
        }
        currentScene.onChangePassword = { [weak self] data in
            self?.presenter.changePasswordHandler(data)
        }
        
        sceneView.addSubview(scene)
    }
    
}

// MARK: - Private
fileprivate extension AuthViewController {
    
    func setupUI() {
        startAvoidingKeyboard()
        addTapGestureWithEndEditing()
    }
    
    func createScene(_ scene: AuthScene) -> AuthViewable {
        switch scene {
        case .login:            return createLoginScene()
        case .register:         return createRegisterScene()
        case .forgot:           return createForgotScene()
        case .changePassword:   return createChangePasswordScene()
        }
    }
    
    func createLoginScene() -> AuthViewable {
        return LoginView.create()
    }
    
    func createRegisterScene() -> AuthViewable {
        let view = RegisterView.create()
        view.onCategories = { [weak self] in
            self?.presenter.categoriesHandler()
        }
        view.onRole = { [weak self] in
            self?.presenter.roleHandler()
        }
        return view
    }
    
    func createForgotScene() -> AuthViewable {
        return ForgotView.create()
    }
    
    func createChangePasswordScene() -> AuthViewable {
        return ChangePasswordView.create()
    }
    
}
