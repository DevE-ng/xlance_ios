//
//  AuthInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright (c) 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol AuthInteractorInput: BaseInteractorInput {
    func login(_ data: AuthModel)
    func register(_ data: AuthModel)
    func forgot(_ data: AuthModel)
    func changePassword(_ data: AuthModel)
    func anonymous()
    func fetchCategories(completionHandler: @escaping ([CategoryModel]) -> Void)
    func fetchRoles(completionHandler: ([ProfileModel.Role]) -> Void)
}
protocol AuthInteractorOutput: BaseInteractorOutput {
    func successAnonymousHandler()
    func successLoginHandler()
    func successRegisterHandler()
    func successForgotHandler()
    func successChangePasswordHandler()
    func errorHandler(_ error: String)
}

class AuthInteractor {
    
    var networkProvider: Networking!
    weak var presenter: AuthInteractorOutput?
    
}

// MARK: - AuthPresenterInput
extension AuthInteractor: AuthInteractorInput {
    
    func login(_ data: AuthModel) {
        networkProvider.login(name: data.email, password: data.password) { [unowned self] in
            self.handleLogin($0, name: data.email, password: data.password)
        }
    }

    func register(_ data: AuthModel) {
        networkProvider.register(data: data) { [unowned self] in
            self.handleRegister($0)
        }
    }

    func forgot(_ data: AuthModel) {
        networkProvider.forgot(email: data.email) { [unowned self] in
            self.handleForgot($0)
        }
    }

    func changePassword(_ data: AuthModel) {
        networkProvider.changePassword(id: CurrentUser.profile!.id,
                                       old: data.password,
                                       new: data.newPassword,
                                       confirm: data.confirmPassword) { [weak self] in
            self?.handleChangePassword($0, password: data.newPassword)
        }
    }

    func anonymous() {
        CurrentUser.loginAnonymous()
        presenter?.successAnonymousHandler()
    }
    
    func fetchCategories(completionHandler: @escaping ([CategoryModel]) -> Void) {
        networkProvider.fetchCategories { [weak self] in
            self?.handleFetchCategories($0, completionHandler: completionHandler)
        }
    }
    
    func fetchRoles(completionHandler: ([ProfileModel.Role]) -> Void) {
        completionHandler(ProfileModel.Role.allCases)
    }
    
}

// MARK: - Private
fileprivate extension AuthInteractor {
    
    func handleLogin(_ result: Result<LoginResponse>, name: String, password: String) {
        
        switch result {
        
        case .Success(let data):
            CurrentUser.login(data, name: name, pass: password)
            presenter?.successLoginHandler()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleRegister(_ result: Result<Bool>) {
        switch result {
            
        case .Success(_):
            presenter?.successRegisterHandler()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleForgot(_ result: Result<Bool>) {
        switch result {
            
        case .Success(_):
            presenter?.successForgotHandler()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleChangePassword(_ result: Result<Bool>, password: String) {
        switch result {
            
        case .Success(_):
            CurrentUser.password = password
            presenter?.successChangePasswordHandler()
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
    func handleFetchCategories(_ result: Result<[CategoryModel]>, completionHandler: ([CategoryModel]) -> Void) {
        switch result {
            
        case .Success(let data):
            completionHandler(data)
            
        case .Error(let error):
            presenter?.errorHandler(error)
            
        }
    }
    
}

