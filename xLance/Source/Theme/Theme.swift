//
//  Theme.swift
//
//  Created by alobanov11 on 29/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import UIKit

struct Theme {
    
    static let blackColor: UIColor = #colorLiteral(red: 0.1176470588, green: 0.1176470588, blue: 0.1176470588, alpha: 1)
    static let warningColor: UIColor = #colorLiteral(red: 0.937254902, green: 0.7803921569, blue: 0.3333333333, alpha: 1)
    static let dangerColor: UIColor = #colorLiteral(red: 0.9450980392, green: 0.2941176471, blue: 0.3137254902, alpha: 1)
    static let successColor: UIColor = #colorLiteral(red: 0.2588235294, green: 0.5921568627, blue: 0.5882352941, alpha: 1)
    static let greyColor: UIColor = #colorLiteral(red: 0.7360000014, green: 0.7360000014, blue: 0.7360000014, alpha: 1)
    static let primaryColor: UIColor = #colorLiteral(red: 0.1137254902, green: 0.5333333333, blue: 0.8980392157, alpha: 1)
    static let purpleColor: UIColor = #colorLiteral(red: 0.6823529412, green: 0.431372549, blue: 0.9176470588, alpha: 1)
    static let messageColor: UIColor = #colorLiteral(red: 0.9215686275, green: 0.9803921569, blue: 0.9960784314, alpha: 1)
    
    static let disabledAlpha: CGFloat = 0.6
    static let margin: CGFloat = 16
    
    static let headlineFont: UIFont = .systemFont(ofSize: 18, weight: .medium)
    static let cornerRadius: CGFloat = 12
    
}

extension Theme {
    
    static func applyAppearanceStyles() {
        let navigationBar = UINavigationBar.appearance()
        navigationBar.barTintColor = .white
        navigationBar.shadowImage = UIImage()
        navigationBar.largeTitleTextAttributes =
            [.font: UIFont(name: "AvenirNext-Bold", size: 34)!]
        navigationBar.titleTextAttributes =
            [.font: UIFont(name: "AvenirNext-Bold", size: 17)!]
        
        let tabBar = UITabBar.appearance()
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
    }
    
}
