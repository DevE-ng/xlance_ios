//
//  MilestoneTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class MilestoneTableViewCell: UITableViewCell {
    
    var edit = false
    var onEdit: CompletionBlock?
    var onDelete: CompletionBlock?
    
    var milestone: MilestoneModel! {
        didSet {
            configure()
        }
    }

    // MARK: Outlets
    @IBOutlet fileprivate var paymentDateTitleLabel: UILabel!
    @IBOutlet fileprivate var editButton: UIButton!
    @IBOutlet fileprivate var deleteButton: UIButton!
    
    @IBOutlet fileprivate var nameLabel: ModyfingLabel!
    @IBOutlet fileprivate var paymentDateLabel: ModyfingLabel!
    @IBOutlet fileprivate var buttonsStackView: UIStackView!
    
    
    @IBAction fileprivate func editButtonTapHandler(_ sender: Any) {
        onEdit?()
    }
    
    @IBAction fileprivate func deleteButtonTapHandler(_ sender: Any) {
        onDelete?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectionStyle = .none
    }
    
}

fileprivate extension MilestoneTableViewCell {
    
    func configure() {
        localize()
        nameLabel.text = milestone.name
        if edit || !milestone.histories.isEmpty { configureEditing() }
        else { configureDetails() }
        configureButtons()
        updateModifing()
    }
    
    func configureDetails() {
        paymentDateLabel.text = milestone.paymentDate.shortWithSeparator
        buttonsStackView.isHidden = true
    }
    
    func configureEditing() {
        let actualHistory = milestone.actualHistory
        paymentDateLabel.text = actualHistory.paymentDate.shortWithSeparator
        buttonsStackView.isHidden = false
    }
    
    func configureButtons() {
        if edit && !milestone.isDeleted {
            buttonsStackView.isHidden = false
        } else {
            buttonsStackView.isHidden = true
        }
    }
    
    func updateModifing() {
        updateModifingByTwoHistories()
        updateModifingByActualHistory()
    }
    
    func updateModifingByTwoHistories() {
        if milestone.histories.count <= 1 { return }
        let firstHistory = milestone.firstHistory
        let secondHistory = milestone.secondHistory
        paymentDateLabel.updateModifyStatus(old: firstHistory.paymentDate, new: secondHistory.paymentDate)
    }
    
    func updateModifingByActualHistory() {
        if milestone.histories.count == 0 { return }
        let actualHistory = milestone.actualHistory
        switch actualHistory.status {
        case .added: nameLabel.updateModifyStatus(.created)
        case .deleted: nameLabel.updateModifyStatus(.deleted)
        default: nameLabel.updateModifyStatus(.none)
        }
    }
    
    func localize() {
        paymentDateTitleLabel.text = "COMMON.PAYMENT_DATE".localized
        editButton.setTitle("COMMON.EDIT".localized, for: .normal)
        deleteButton.setTitle("COMMON.DELETE".localized, for: .normal)
    }
    
}
