//
//  MessageTableViewCell.swift
//  xlance
//
//  Created by alobanov11 on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    var message: MessageModel! {
        didSet {
            configure()
        }
    }
    
    @IBOutlet fileprivate var stackView: UIStackView!
    @IBOutlet fileprivate var bgView: UIView!
    @IBOutlet fileprivate var messageLabel: UILabel!
    @IBOutlet fileprivate var dateLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupCompanion()
    }
    
}

fileprivate extension MessageTableViewCell {
    func configure() {
        messageLabel.text = message.message
        dateLabel.text = message.sentAt.timeAgo
    }
    func setupCompanion() {
        bgView.layoutIfNeeded()
        if message.sentFrom.id == CurrentUser.profile?.id {
            stackView.alignment = .trailing
            bgView.backgroundColor = Theme.successColor
            UILabel.setTextColor([messageLabel, dateLabel], color: .white)
            bgView.applyCornerRadius(corners: [.topLeft, .topRight, .bottomLeft], radius: Theme.cornerRadius*1.5)
        } else {
            stackView.alignment = .leading
            bgView.backgroundColor = Theme.messageColor
            UILabel.setTextColor([messageLabel, dateLabel], color: .black)
            bgView.applyCornerRadius(corners: [.topLeft, .topRight, .bottomRight], radius: Theme.cornerRadius*1.5)
        }
    }
}
