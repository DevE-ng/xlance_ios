//
//  FileTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class FileTableViewCell: UITableViewCell {

    var message: MessageModel! {
        didSet {
            configure()
        }
    }
    
    var onSelect: CompletionBlock? {
        didSet {
            _ = tapGesture
        }
    }
    
    @IBOutlet fileprivate var stackView: UIStackView!
    @IBOutlet fileprivate var bgView: UIView!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var typeLabel: UILabel!
    @IBOutlet fileprivate var sizeLabel: UILabel!
    @IBOutlet fileprivate var dateLabel: UILabel!
    
    fileprivate lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(selectHandler))
        bgView.addGestureRecognizer(gesture)
        return gesture
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupCompanion()
    }
    
}

fileprivate extension FileTableViewCell {
    func configure() {
        nameLabel.text = message.file?.uppercased()
        typeLabel.text = message.file?.fileExt.uppercased()
        dateLabel.text = message.sentAt.timeAgo
    }
    func setupCompanion() {
        bgView.layoutIfNeeded()
        if message.sentFrom.id == CurrentUser.profile?.id {
            stackView.alignment = .trailing
            bgView.backgroundColor = Theme.successColor
            bgView.applyCornerRadius(corners: [.topLeft, .topRight, .bottomLeft], radius: Theme.cornerRadius*1.5)
            UILabel.setTextColor([nameLabel, typeLabel, sizeLabel, dateLabel], color: .white)
        } else {
            stackView.alignment = .trailing
            bgView.backgroundColor = Theme.messageColor
            bgView.applyCornerRadius(corners: [.topLeft, .topRight, .bottomRight], radius: Theme.cornerRadius*1.5)
            UILabel.setTextColor([nameLabel, typeLabel, sizeLabel, dateLabel], color: .black)
        }
    }
    @objc func selectHandler() {
        onSelect?()
    }
}
