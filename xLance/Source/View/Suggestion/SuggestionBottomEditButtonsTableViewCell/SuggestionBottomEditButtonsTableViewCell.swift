//
//  SuggestionBottomEditButtonsTableViewCell.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class SuggestionBottomEditButtonsTableViewCell: UITableViewCell {

    var suggestion: SuggestionModel! {
        didSet {
            configure()
        }
    }
    
    var onSendProposal:         CompletionBlock?
    var onRestoreToDefault:     CompletionBlock?
    
    var status: ProjectStatus! {
        didSet {
            configure()
        }
    }
    
    @IBOutlet fileprivate var sendProposalButton: UIButton!
    @IBOutlet fileprivate var restoreToDefaultButton: UIButton!
    
    @IBAction fileprivate func buttonTapHandler(_ sender: UIButton) {
        switch sender {
            
        case sendProposalButton:
            onSendProposal?()
            
        case restoreToDefaultButton:
            onRestoreToDefault?()
            
        default:
            ()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectionStyle = .none
    }
    
}

fileprivate extension SuggestionBottomEditButtonsTableViewCell {
    
    func configure() {
        localize()
    }
    
    func localize() {
        sendProposalButton.setTitle("CREATE_SUGGESTION.SEND_FL".localized, for: .normal)
        restoreToDefaultButton.setTitle("CREATE_SUGGESTION.RESET_TO_DEFAULT".localized, for: .normal)
    }
    
}
