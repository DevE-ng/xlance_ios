//
//  SuggestionBottomButtonsTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class SuggestionBottomButtonsTableViewCell: UITableViewCell {
    
    var suggestion: SuggestionModel! {
        didSet {
            configure()
        }
    }
    
    var onAcceptNewTerms:     CompletionBlock?
    var onEditOffer:        CompletionBlock?
    var onDecline:          CompletionBlock?
    
    var status: ProjectStatus! {
        didSet {
            configure()
        }
    }
    
    @IBOutlet fileprivate var acceptNewTermsButton: UIButton!
    @IBOutlet fileprivate var editOfferButton: UIButton!
    @IBOutlet fileprivate var declineButton: UIButton!
    
    @IBAction fileprivate func buttonTapHandler(_ sender: UIButton) {
        switch sender {
            
        case acceptNewTermsButton:
            onAcceptNewTerms?()
            
        case editOfferButton:
            onEditOffer?()
            
        case declineButton:
            onDecline?()
            
        default:
            ()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectionStyle = .none
    }
    
}

fileprivate extension SuggestionBottomButtonsTableViewCell {
    
    func configure() {
        localize()
    }
    
    func localize() {
        acceptNewTermsButton.setTitle("SUGGESTION_DIALOG_COMPONENT.SEND_FL".localized, for: .normal)
        editOfferButton.setTitle("SUGGESTION_DIALOG_COMPONENT.EDIT_FL".localized, for: .normal)
        declineButton.setTitle("COMMON.DECLINE".localized, for: .normal)
    }
    
}
