//
//  SuggestionTopTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class SuggestionTopTableViewCell: UITableViewCell {
    
    var suggestion: SuggestionModel! {
        didSet {
            configure()
        }
    }
    
    @IBOutlet fileprivate var startDateTitleLabel: UILabel!
    @IBOutlet fileprivate var tillDateTitleLabel: UILabel!
    @IBOutlet fileprivate var budgetTitleLabel: UILabel!

    @IBOutlet fileprivate var startDateLabel: ModyfingLabel!
    @IBOutlet fileprivate var tillDateLabel: ModyfingLabel!
    @IBOutlet fileprivate var budgetLabel: ModyfingLabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectionStyle = .none
    }
    
}

fileprivate extension SuggestionTopTableViewCell {
    
    func configure() {
        localize()
        startDateLabel.text = suggestion.actualHistory.startDate.shortWithSeparator
        tillDateLabel.text = suggestion.actualHistory.tillDate.shortWithSeparator
        budgetLabel.text = "\(suggestion.actualHistory.budget.string) \(suggestion.currency.code)"
        if suggestion.histories.count == 2 {
            updateModifingByTwoHistories()
        }
    }
    
    func updateModifingByTwoHistories() {
        let firstHistory = suggestion.firstHistory
        let secondHistory = suggestion.secondHistory
        
        startDateLabel.updateModifyStatus(old: firstHistory.startDate,
                                          new: secondHistory.startDate)
        
        tillDateLabel.updateModifyStatus(old: firstHistory.tillDate,
                                         new: secondHistory.tillDate)
        
        budgetLabel.updateModifyStatus(old: firstHistory.budget,
                                       new: secondHistory.budget)
    }
    
    func localize() {
        startDateTitleLabel.text = "COMMON.START_DATE".localized
        tillDateTitleLabel.text = "COMMON.DATE_TILL".localized
        budgetTitleLabel.text = "PROJECT_DIALOG_COMPONENT.BUDGET".localized
    }
    
}
