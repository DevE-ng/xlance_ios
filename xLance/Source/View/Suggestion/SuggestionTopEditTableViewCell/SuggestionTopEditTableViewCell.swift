//
//  SuggestionTopEditTableViewCell.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SuggestionTopEditTableViewCell: UITableViewCell {
    
    // MARK: PUBLIC VARS
    
    var onSuggestionEdit: CompletionSuggestionBlock?

    var suggestion: SuggestionModel! {
        didSet {
            configure()
        }
    }
    
    // MARK: PRIVATE VARS

    fileprivate lazy var startDatePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.date = suggestion.startDate
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        return picker
    }()
    
    fileprivate lazy var tillDatePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.date = suggestion.tillDate
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        return picker
    }()
    
    // MARK: OUTLETS
    
    @IBOutlet fileprivate var startDateTextField: ModyfingTextField!
    @IBOutlet fileprivate var tillDateTextField: ModyfingTextField!
    @IBOutlet fileprivate var budgetTextField: ModyfingTextField!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }
    
    // MARK: ACTIONS
    
    @IBAction fileprivate func valueChangedHandler(_ sender: UITextField) {
        switch sender {

        case budgetTextField:
            guard let budget = sender.text?.decimal, budget > 0  else { return }
            suggestion.actualHistory.budget = budget

        default: ()
        }
//
        updateModifingByTwoHistories()
        onSuggestionEdit?(suggestion)
    }
    
    @objc fileprivate func handleDatePicker(_ datePicker: UIDatePicker) {
        let date = datePicker.date
        
        switch datePicker {
            
        case startDatePicker:
            suggestion.actualHistory.startDate = date
            startDateTextField.text = date.shortWithSeparator
            
        case tillDatePicker:
            suggestion.actualHistory.tillDate = date
            tillDateTextField.text = date.shortWithSeparator
            
        default: ()
        }
        
        updateModifingByTwoHistories()
        onSuggestionEdit?(suggestion)
    }
    
}

fileprivate extension SuggestionTopEditTableViewCell {
    
    func setupUI() {
        selectionStyle = .none
        startDateTextField.inputView = startDatePicker
        tillDateTextField.inputView = tillDatePicker
    }
    
    func configure() {
        localize()
        
        let actualHistory = suggestion.actualHistory
        
        startDateTextField.text = actualHistory.startDate.shortWithSeparator
        tillDateTextField.text = actualHistory.tillDate.shortWithSeparator
        
        budgetTextField.placeholder = "TOTAL BUDGET, \(suggestion.currency.code)"
        budgetTextField.text = actualHistory.budget.string
        
        updateModifingByTwoHistories()
    }
    
    
    func updateModifingByTwoHistories() {
        if suggestion.histories.count != 2 { return }
        let firstHistory = suggestion.firstHistory
        let secondHistory = suggestion.secondHistory
        
        startDateTextField.updateModifyStatus(old: firstHistory.startDate,
                                              new: secondHistory.startDate)
        
        tillDateTextField.updateModifyStatus(old: firstHistory.tillDate,
                                             new: secondHistory.tillDate)
        
        budgetTextField.updateModifyStatus(old: firstHistory.budget,
                                           new: secondHistory.budget)
    }
    
    func localize() {
        startDateTextField.placeholder = "COMMON.START_DATE".localized
        tillDateTextField.placeholder = "COMMON.DATE_TILL".localized
        budgetTextField.placeholder = "PROJECT_DIALOG_COMPONENT.BUDGET".localized
    }
    
    
}
