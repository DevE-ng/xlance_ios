//
//  TaskTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    var edit = false
    var onEdit: CompletionBlock?
    var onDelete: CompletionBlock?
    
    var task: TaskModel! {
        didSet {
            configure()
        }
    }
    
    // MARK: Outlets
    @IBOutlet fileprivate var editButton: UIButton!
    @IBOutlet fileprivate var deleteButton: UIButton!
    
    @IBOutlet fileprivate var nameLabel: ModyfingLabel!
    @IBOutlet fileprivate var durationLabel: ModyfingLabel!
    @IBOutlet fileprivate var buttonsStackView: UIStackView!
    
    
    @IBAction fileprivate func editButtonTapHandler(_ sender: Any) {
        onEdit?()
    }
    
    @IBAction fileprivate func deleteButtonTapHandler(_ sender: Any) {
        onDelete?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectionStyle = .none
    }
    
}

fileprivate extension TaskTableViewCell {
    
    func configure() {
        localize()
        if edit || !task.histories.isEmpty { configureEditing() }
        else { configureDetails() }
        configureButtons()
        updateModifing()
    }
    
    func configureDetails() {
        nameLabel.text = task.name
        durationLabel.text = task.duration.string + "TASK_COMPONENT.H".localized
        buttonsStackView.isHidden = true
    }
    
    func configureEditing() {
        let actualHistory = task.actualHistory
        nameLabel.text = actualHistory.name
        durationLabel.text = actualHistory.duration.string + "TASK_COMPONENT.H".localized
        buttonsStackView.isHidden = false
    }
    
    func configureButtons() {
        if edit && !task.isDeleted {
            buttonsStackView.isHidden = false
        } else {
            buttonsStackView.isHidden = true
        }
    }
    
    func updateModifing() {
        updateModifingByTwoHistories()
        updateModifingByActualHistory()
    }
    
    func updateModifingByTwoHistories() {
        if task.histories.count != 2 { return }
        let firstHistory = task.firstHistory
        let secondHistory = task.secondHistory
        durationLabel.updateModifyStatus(old: firstHistory.duration, new: secondHistory.duration)
    }
    
    func updateModifingByActualHistory() {
        if task.histories.count == 0 { return }
        switch task.actualHistory.status {
        case .added: nameLabel.updateModifyStatus(.created)
        case .deleted: nameLabel.updateModifyStatus(.deleted)
        default: ()
        }
        if task.histories.count == 2, task.firstHistory.milestoneId != task.secondHistory.milestoneId {
            nameLabel.updateModifyStatus(.moved)
        }
    }
    
    func localize() {
        editButton.setTitle("COMMON.EDIT".localized, for: .normal)
        deleteButton.setTitle("COMMON.DELETE".localized, for: .normal)
    }
    
}
