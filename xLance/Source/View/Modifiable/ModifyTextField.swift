//
//  ModyfingTextField.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ModyfingTextField: SkyFloatingLabelTextField {
    
    var savedDefaultColor: UIColor?
    
    func setModifyColor(_ color: UIColor) {
        textColor = color
    }
    
    func saveDefaultColor() {
        if savedDefaultColor == nil { savedDefaultColor = textColor }
    }
    
}

extension ModyfingTextField: ModifiableView {}
