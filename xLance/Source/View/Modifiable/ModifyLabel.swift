//
//  ModyfingLabel.swift
//  xlance
//
//  Created by alobanov11 on 13/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class ModyfingLabel: UILabel {
    
    var savedDefaultColor: UIColor?
    
    func setModifyColor(_ color: UIColor) {
        textColor = color
    }
    
    func saveDefaultColor() {
        if savedDefaultColor == nil { savedDefaultColor = textColor }
    }
    
}

extension ModyfingLabel: ModifiableView {}
