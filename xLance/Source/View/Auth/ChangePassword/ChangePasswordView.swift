//
//  ChangePasswordView.swift
//  xlance
//
//  Created by Anton Lobanov on 08/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class ChangePasswordView: AuthView {
    
    override var title: String {
        return "FORGOT_COMPONENT.CHANGE_PASSWORD"
    }
    
    override var scrollView: UIScrollView! {
        get {
            return privateScrollView
        }
        set {
            
        }
    }
    
    @IBOutlet fileprivate var changePasswordButton: UIButton!
    @IBOutlet fileprivate var privateScrollView: UIScrollView!
    @IBOutlet fileprivate var currentPasswordTextField: UITextField!
    @IBOutlet fileprivate var newPasswordTextField: UITextField!
    @IBOutlet fileprivate var confirmPasswordTextField: UITextField!
    
    override func configure() {
        currentPasswordTextField.text = data.password
        newPasswordTextField.text = data.newPassword
        confirmPasswordTextField.text = data.confirmPassword
    }
    
    @IBAction fileprivate func changePasswordButtonTapHandler(_ sender: Any) {
        onChangePassword?(data)
    }
    
    @IBAction fileprivate func currentPasswordTextFieldEditingChanged(_ sender: UITextField) {
        data.password = sender.text ?? ""
    }
    
    @IBAction fileprivate func newPasswordTextFieldEditingChanged(_ sender: UITextField) {
        data.newPassword = sender.text ?? ""
    }
    
    @IBAction fileprivate func confirmPasswordTextFieldEditingChanged(_ sender: UITextField) {
        data.confirmPassword = sender.text ?? ""
    }
    
}

fileprivate extension ChangePasswordView {
    
    func localize() {
        currentPasswordTextField.placeholder = "EDIT_PROFILE_COMPONENT.SECURITY.CURRENT_PASSWORD".localized
        newPasswordTextField.placeholder = "EDIT_PROFILE_COMPONENT.SECURITY.NEW_PASSWORD".localized
        confirmPasswordTextField.placeholder = "EDIT_PROFILE_COMPONENT.SECURITY.CONFIRM_NEW_PASSWORD".localized
        changePasswordButton.setTitle("FORGOT_COMPONENT.RESET_PASSWORD".localized, for: .normal)
    }
    
}
