//
//  RegisterView.swift
//  xlance
//
//  Created by Anton Lobanov on 08/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class RegisterView: AuthView {
    
    override var title: String {
        return "REGISTER_COMPONENT.REGISTER"
    }
    
    override var scrollView: UIScrollView! {
        get {
            return privateScrollView
        }
        set {
            
        }
    }
    
    var onCategories: CompletionBlock?
    var onRole: CompletionBlock?
    
    @IBOutlet fileprivate var privateScrollView: UIScrollView!
    
    @IBOutlet fileprivate var signInButton: UIButton!
    @IBOutlet fileprivate var signUpButton: UIButton!
    @IBOutlet fileprivate var forgotPasswordButton: UIButton!
    
    @IBOutlet fileprivate var nameTextField: UITextField!
    @IBOutlet fileprivate var lastNameTextField: UITextField!
    @IBOutlet fileprivate var categoriesTextField: UITextField!
    @IBOutlet fileprivate var nickTextField: UITextField!
    @IBOutlet fileprivate var cityTextField: UITextField!
    @IBOutlet fileprivate var emailTextField: UITextField!
    @IBOutlet fileprivate var roleTextField: UITextField!
    @IBOutlet fileprivate var passwordTextField: UITextField!
    
    override func configure() {
        localize()
        nameTextField.text = data.name
        lastNameTextField.text = data.lastName
        categoriesTextField.text = data.categories.map({ $0.code.localized }).joined(separator: ", ")
        nickTextField.text = data.nick
        cityTextField.text = data.city
        roleTextField.text = data.role.localized
        emailTextField.text = data.email
        passwordTextField.text = data.password
    }
    
    @IBAction fileprivate func loginButtonTapHandler(_ sender: Any) {
        onLogin?(data)
    }
    
    @IBAction fileprivate func forgotButtonTapHandler(_ sender: Any) {
        onForgot?(data)
    }
    
    @IBAction fileprivate func registerButtonTapHandler(_ sender: Any) {
        onRegister?(data)
    }
    
    @IBAction fileprivate func textFieldValueHandler(_ sender: UITextField) {
        switch sender {
        case nameTextField:         data.name = sender.text ?? ""
        case lastNameTextField:     data.lastName = sender.text ?? ""
        case categoriesTextField:
            categoriesTextField.resignFirstResponder()
            onCategories?()
        case cityTextField:         data.city = sender.text ?? ""
        case emailTextField:        data.email = sender.text ?? ""
        case roleTextField:
            roleTextField.resignFirstResponder()
            onRole?()
        case passwordTextField:     data.password = sender.text ?? ""
        default: ()
        }
    }
    
}

fileprivate extension RegisterView {
    
    func localize() {
        
        nameTextField.placeholder = "REGISTER_COMPONENT.FIRST_NAME".localized
        lastNameTextField.placeholder = "REGISTER_COMPONENT.LAST_NAME".localized
        categoriesTextField.placeholder = "REGISTER_COMPONENT.CATEGORIES".localized
        nickTextField.placeholder = "REGISTER_COMPONENT.NICK_NAME".localized
        cityTextField.placeholder = "REGISTER_COMPONENT.CITY".localized
        roleTextField.placeholder = "REGISTER_COMPONENT.ROLE".localized
        emailTextField.placeholder = "LOGIN_COMPONENT.EMAIL".localized
        passwordTextField.placeholder = "LOGIN_COMPONENT.PASSWORD".localized
        
        signInButton.setTitle("HEADER_COMPONENT.LOGIN".localized, for: .normal)
        signUpButton.setTitle("HEADER_COMPONENT.REGISTER".localized, for: .normal)
        forgotPasswordButton.setTitle("LOGIN_COMPONENT.FORGOT_PASSWORD".localized, for: .normal)
    }
    
}
