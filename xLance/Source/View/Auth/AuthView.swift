//
//  AuthView.swift
//  xlance
//
//  Created by Anton Lobanov on 08/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol AuthViewable where Self: UIView {
    
    var title: String { get }
    
    var data: AuthModel! { get set }
    var scrollView: UIScrollView! { get set }
    
    var onLogin: CompletionAuthBlock? { get set }
    var onForgot: CompletionAuthBlock? { get set }
    var onRegister: CompletionAuthBlock? { get set }
    var onChangePassword: CompletionAuthBlock? { get set }
    
    func configure()
    
}

class AuthView: UIView, AuthViewable, XibLoadable {
    
    var title: String {
        return ""
    }
    
    var data: AuthModel! {
        didSet {
            configure()
        }
    }
    var scrollView: UIScrollView!
    
    var onLogin: CompletionAuthBlock?
    var onForgot: CompletionAuthBlock?
    var onRegister: CompletionAuthBlock?
    var onChangePassword: CompletionAuthBlock?
    
    func configure() {
        
    }
    
}
