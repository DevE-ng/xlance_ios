//
//  ForgotView.swift
//  xlance
//
//  Created by Anton Lobanov on 08/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class ForgotView: AuthView {
    
    override var title: String {
        return "FORGOT_COMPONENT.FORGOT_PASSWORD"
    }
    
    override var scrollView: UIScrollView! {
        get {
            return privateScrollView
        }
        set {
            
        }
    }
    
    @IBOutlet fileprivate var signInButton: UIButton!
    @IBOutlet fileprivate var signUpButton: UIButton!
    @IBOutlet fileprivate var forgotPasswordButton: UIButton!
    
    @IBOutlet fileprivate var privateScrollView: UIScrollView!
    @IBOutlet fileprivate var emailTextField: UITextField!
    
    override func configure() {
        localize()
        emailTextField.text = data.email
    }
    
    @IBAction fileprivate func loginButtonTapHandler(_ sender: Any) {
        onLogin?(data)
    }
    
    @IBAction fileprivate func forgotButtonTapHandler(_ sender: Any) {
        onForgot?(data)
    }
    
    @IBAction fileprivate func registerButtonTapHandler(_ sender: Any) {
        onRegister?(data)
    }
    
    @IBAction fileprivate func emailTextFieldEditingChanged(_ sender: Any) {
        data.email = emailTextField.text ?? ""
    }
    
}

fileprivate extension ForgotView {
    
    func localize() {
        emailTextField.placeholder = "LOGIN_COMPONENT.EMAIL".localized
        signInButton.setTitle("HEADER_COMPONENT.LOGIN".localized, for: .normal)
        signUpButton.setTitle("HEADER_COMPONENT.REGISTER".localized, for: .normal)
        forgotPasswordButton.setTitle("FORGOT_COMPONENT.RESET_PASSWORD".localized, for: .normal)
    }
    
}
