//
//  ChatTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    var project: ProjectModel! {
        didSet {
            configure()
        }
    }
    
    @IBOutlet fileprivate var avatarImageView: UIImageView!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var descLabel: UILabel!
    @IBOutlet fileprivate var notifyLabel: UILabel!
    @IBOutlet fileprivate var onlineView: UIView!
    
    
}

fileprivate extension ChatTableViewCell {
    
    func configure() {
        selectionStyle = .none
        nameLabel.text = project.companion?.fullName
        descLabel.text = project.name
        setupOnline()
        setupNotify()
        
    }
    
    func setupOnline() {
        if project.online {
            onlineView.backgroundColor = Theme.successColor
        } else {
            onlineView.backgroundColor = Theme.greyColor
        }
    }
    
    func setupNotify() {
        if project.unreadMessagesCount != 0 {
            notifyLabel.isHidden = false
            notifyLabel.text = project.unreadMessagesCount.string
        } else {
            notifyLabel.isHidden = true
        }
    }
    
}
