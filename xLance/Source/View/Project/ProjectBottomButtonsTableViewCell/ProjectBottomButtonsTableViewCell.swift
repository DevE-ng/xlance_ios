//
//  ProjectBottomButtonsTableViewCell.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

class ProjectBottomButtonsTableViewCell: UITableViewCell {


    var onSendProposal:     CompletionBlock?
    var onEditTerms:        CompletionBlock?
    var onViewCondition:    CompletionBlock?
    var onDecline:          CompletionBlock?
    
    var status: ProjectStatus! {
        didSet {
            configure()
        }
    }
    
    @IBOutlet fileprivate var sendProposalButton: UIButton!
    @IBOutlet fileprivate var editTermsButton: UIButton!
    @IBOutlet fileprivate var viewConditionButton: UIButton!
    @IBOutlet fileprivate var declineButton: UIButton!
    
    @IBAction fileprivate func buttonTapHandler(_ sender: UIButton) {
        switch sender {
        case sendProposalButton:
            onSendProposal?()
        case editTermsButton:
            onEditTerms?()
        case viewConditionButton:
            onViewCondition?()
        case declineButton:
            onDecline?()
        default:
            ()
        }
    }
    
}

fileprivate extension ProjectBottomButtonsTableViewCell {
    
    func configure() {
        localize()
        
        switch status {
            
        case .waitingForAgreement?:
            sendProposalButton.isHidden = true
            editTermsButton.isHidden = true
            viewConditionButton.isHidden = true
            declineButton.isHidden = false
            
        case .waitingForAccept?:
            sendProposalButton.isHidden = true
            editTermsButton.isHidden = true
            viewConditionButton.isHidden = false
            declineButton.isHidden = false
            
        default:
            sendProposalButton.isHidden = false
            editTermsButton.isHidden = false
            viewConditionButton.isHidden = true
            declineButton.isHidden = true
            
        }
    }
    
    func localize() {
        sendProposalButton.setTitle("PROJECT_COMPONENT.SEND_PROPOSAL".localized, for: .normal)
        editTermsButton.setTitle("SUGGESTION_DIALOG_COMPONENT.EDIT_TERMS".localized, for: .normal)
        viewConditionButton.setTitle("COMMON.VIEW_AGREEMENT_CONDITION".localized, for: .normal)
        declineButton.setTitle("COMMON.DECLINE".localized, for: .normal)
    }
    
}
