//
//  ProjectTableViewCell.swift
//  xlance
//
//  Created by alobanov11 on 17/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell, XibLoadable {

    var project: ProjectModel! {
        didSet {
            configure()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cardView.layer.addShadow(opacity: 0.2,
                                 offset: .init(width: 8, height: 10),
                                 radius: 12, shadowColor: .black, cornerRadius: 22)
    }
    
    // MARK: Outlets
    @IBOutlet fileprivate var cardView: UIView!
    @IBOutlet fileprivate var statusLineView: UIView!
    @IBOutlet fileprivate var statusTextLabel: UILabel!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var budgetLabel: UILabel!
    @IBOutlet fileprivate var descLabel: UILabel!
    @IBOutlet fileprivate var dateLabel: UILabel!
    @IBOutlet fileprivate var payLabel: UILabel!
    @IBOutlet fileprivate var categoryLabel: UILabel!
    
}

fileprivate extension ProjectTableViewCell {
    
    func configure() {
        dateLabel.text = "\(project.startDate.shortWithMonth) - \(project.tillDate.shortWithMonth)"
        nameLabel.text = project.masterJob.name
        if let category = project.mainCategory {
            categoryLabel.text = category.code.localized
        } else {
            categoryLabel.isHidden = true
        }
        descLabel.text = project.name
        budgetLabel.text = "\("CREATE_JOB_DIALOG_COMPONENT.TOTAL_BUDGET".localized): \(project.budget.string) \(project.currency.code)"
        payLabel.text = project.paymentMethod.localized
        configureStatus()
    }
    
    func configureStatus() {
        switch project.status {
        case .accepted:
            updateStatus(color: Theme.successColor)

        case .waitingForAgreement, .waitingForAccept:
            updateStatus(color: Theme.warningColor)

        default:
            statusLineView.isHidden = true
            statusTextLabel.isHidden = true
        }
    }

    func updateStatus(color: UIColor) {
        statusLineView.isHidden = false
        statusTextLabel.isHidden = false
        statusLineView.backgroundColor = color
        statusTextLabel.text = project.status.localized
        statusTextLabel.textColor = color
    }
    
}
