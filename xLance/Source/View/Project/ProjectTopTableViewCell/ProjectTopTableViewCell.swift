//
//  ProjectTableViewCell.swift
//  xlance
//
//  Created by alobanov11 on 17/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import UIKit

class ProjectTopTableViewCell: UITableViewCell, XibLoadable {

    var project: ProjectModel! {
        didSet {
            configure()
        }
    }
    
    // MARK: Outlets
    @IBOutlet fileprivate var statusLineView: UIView!
    @IBOutlet fileprivate var statusTextLabel: UILabel!
    @IBOutlet fileprivate var budgetLabel: UILabel!
    @IBOutlet fileprivate var descLabel: UILabel!
    @IBOutlet fileprivate var dateLabel: UILabel!
    @IBOutlet fileprivate var payLabel: UILabel!
    @IBOutlet fileprivate var categoryLabel: UILabel!
    
}

fileprivate extension ProjectTopTableViewCell {
    
    func configure() {
        dateLabel.text = "\(project.startDate.shortWithMonth) - \(project.tillDate.shortWithMonth)"
        if let category = project.mainCategory {
            categoryLabel.text = category.code.localized
        } else {
            categoryLabel.isHidden = true
        }
        descLabel.text = project.name
        budgetLabel.text = "\("CREATE_JOB_DIALOG_COMPONENT.TOTAL_BUDGET".localized): \(project.budget.string) \(project.currency.code)"
        payLabel.text = project.paymentMethod.localized
        configureStatus()
    }
    
    func configureStatus() {
        switch project.status {
        case .accepted:
            updateStatus(color: Theme.successColor)
            
        case .waitingForAgreement, .waitingForAccept:
            updateStatus(color: Theme.warningColor)
            
        default:
            statusLineView.isHidden = true
            statusTextLabel.isHidden = true
        }
    }
    
    func updateStatus(color: UIColor) {
        statusLineView.isHidden = false
        statusTextLabel.isHidden = false
        statusLineView.backgroundColor = color
        statusTextLabel.text = project.status.localized
        statusTextLabel.textColor = color
    }
    
}
