//
//  XlanceOpenVidu.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import UIKit
import WebRTC
import SwiftyJSON

class CallService: Calling {
    
    var token: String? {
        didSet {
            if token == oldValue { return }
            start()
        }
    }
    weak var delegate: CallingDelegate?
    
    private var socketXlanceRTC: XlanceRTCable?
    private var socketOpenVidu: OpenViduSocketable?
    private var peersManager: RTCPeersManager?
    
    deinit {
        print("#CallService deinit")
    }
    
    // MARK: - Public api
    
    func call(user: String, completionHandler: @escaping (Result<Bool>) -> Void) {
        start()
        socketXlanceRTC?.createRoom { [weak self] result in
            switch result {
            case .Success(let room):
                self?.successCreateRoomHandler(user: user, room: room)
                DispatchQueue.main.async {
                    completionHandler(.Success(true))
                }
                
            case .Error(_): ()
            }
        }
    }
    
    func finishCall() {
        end()
    }
    
    func toggleVideo() -> Bool? {
        return peersManager?.toggleLocalVideo()
    }
    
    
    func toggleAudio() -> Bool? {
        return peersManager?.toggleLocalAudio()
    }
    
    func toggleSound() -> Bool? {
        return peersManager?.toggleRemoteAudio()
    }
    
}

// MARK: - Private
fileprivate extension CallService {
    
    func start() {
        guard let token = token else {
            socketXlanceRTC = nil
            return
        }
        socketXlanceRTC = XlanceRTC(url: CONSTANTS.xlance_socket_url, token: token)
        
        socketXlanceRTC?.onDisconnect = { [weak self] _ in
            self?.delegate?.disconnect()
        }
        
        socketXlanceRTC?.onInvite = { [weak self] success in
            if success { return }
            self?.delegate?.disconnect()
        }
        
    }
    
    func end() {
        socketOpenVidu?.disconnect()
        socketOpenVidu = nil
        peersManager = nil
    }
    
    func successCreateRoomHandler(user: String, room: RoomResponse) {
        socketXlanceRTC?.initeUserInRoom(user: user, session: room.session)
        socketOpenVidu = OpenViduSocket(token: room.token, session: room.session)
        peersManager = RTCPeersManager()
        setupOpenViduSocketHandlers()
        setupPeerManagerHandlers()
    }
    
    /*------------------------------------------
     MARK: Set handlers
     ------------------------------------------*/
    
    func setupOpenViduSocketHandlers() {
        socketOpenVidu?.onConnect = { [weak self] result in
            print("#CallService setupSocketHandlers onConnect")
            switch result {
            case .Success(_):
                self?.peersManager?.createLocalPeerConnection()
                self?.createAndStartLocalVideo()
            case .Error(_): ()
            }
        }
        
        socketOpenVidu?.onUpdateAnswer = { [weak self] json in
            print("#CallService setupSocketHandlers onUpdateAnswer")
            self?.peersManager?.setRemoteDescription(json)
        }
        
        socketOpenVidu?.onUpdateSession = { [weak self] json in
            print("#CallService setupSocketHandlers onUpdateSession")
            if let userID = json[OV_JSON.Id]?.string {
                self?.peersManager?.localUserId = userID
            }
        }
        
        socketOpenVidu?.onIceCandidate = { [weak self] json in
            print("#CallService setupSocketHandlers onIceCandidate")
            self?.peersManager?.addIceCandidate(json)
        }
        
        socketOpenVidu?.onUserJoined = { [weak self] json in
            self?.peersManager?.createPeerUser(json)
        }
        
        socketOpenVidu?.onUserPublished = { [weak self] json in
            self?.peersManager?.setLocalDescription(json)
        }
        
        socketOpenVidu?.onUserLeft = { [weak self] json in
            self?.delegate?.disconnect()
        }
    }
    
    func setupPeerManagerHandlers() {
        peersManager?.onLocalOffer = { [weak self] params in
            print("#CallService setupPeerManagerHandlers onLocalOffer")
            self?.socketOpenVidu?.sendMessage(method: "publishVideo", params: params)
        }
        
        peersManager?.onIceCandidate = { [weak self] candidateParams in
            print("#CallService setupPeerManagerHandlers onIceCandidate")
            self?.socketOpenVidu?.sendMessage(method: "onIceCandidate", params: candidateParams)
        }
        
        peersManager?.onAddIceCandidate = { [weak self] candidateParams in
            print("#CallService setupPeerManagerHandlers onAddIceCandidate")
            self?.socketOpenVidu?.addIceCandidate(candidateParams)
        }
        
        peersManager?.onSetRemoteDescription = { [weak self] in
            self?.updatePeerUsers()
        }
        
        peersManager?.onRemoteOffer = { [weak self] params in
            self?.socketOpenVidu?.sendMessage(method: "receiveVideoFrom", params: params)
        }
    }
    
    /*------------------------------------------
     MARK: Other
     ------------------------------------------*/
    
    func createAndStartLocalVideo() {
        guard let bounds = delegate?.localVideoSize() else { return }
        
        #if arch(arm64)
        let renderer = RTCMTLVideoView(frame: bounds)
        #else
        let renderer = RTCEAGLVideoView(frame: bounds)
        #endif
        print("#CallService createAndStartLocalVideo")
        delegate?.addLocalView(view: renderer)
        peersManager?.createLocalMedia(renderer: renderer)
    }
    
    func updatePeerUsers() {
        guard
            let peersManager = peersManager,
            let participantCount = delegate?.countRemoteViews() else { return }
        if peersManager.countRemoteStreams() < participantCount { return }
        
        guard let video = delegate?.createRemoteView() else { return }
        
        #if arch(arm64)
        let renderer = RTCMTLVideoView(frame: video.view.frame)
        #else
        let renderer = RTCEAGLVideoView(frame:  video.view.frame)
        #endif
        
        video.view.addSubview(renderer)
        peersManager.updatePeerUser(renderer: renderer, index: video.index)
    }
    
}

