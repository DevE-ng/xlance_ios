//
//  XlanceSocket.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import Starscream
import SwiftyJSON

class XlanceRTC: XlanceRTCable, WebSocketDelegate {
    
    // MARK: XlanceMainSocketable
    
    var onDisconnect: ((String?) -> Void)?
    var onInvite: ((Bool) -> Void)?
    
    // MARK: init
    
    private let url: String
    private let token: String
    private let socket: WebSocket
    
    private var isRegistered = false
    private var onCreateRoom: ((Result<RoomResponse>) -> Void)?
    
    init(url: String, token: String) {
        self.url = url
        self.token = token
        
        var request = URLRequest(url: URL(string: url + token)!)
        request.setValue(CONSTANTS.xlance_full_url_without_port, forHTTPHeaderField: "origin")
        self.socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
    
    deinit {
        socket.disconnect()
    }
    
    func createRoom(completionHandler: @escaping (Result<RoomResponse>) -> Void) {
        onCreateRoom = completionHandler
        if isRegistered { createRoom() }
    }
    
    func initeUserInRoom(user: String, session: String) {
        socket.write(string: "{\"id\":\"inviteUser\",\"to\":\"\(user)\",\"room\":\"\(session)\"}")
    }
    
    
}


// MARK: - Private

fileprivate extension XlanceRTC {
    
    func register() {
        socket.write(string: "{\"id\":\"register\"}")
    }
    
    func createRoom() {
        socket.write(string: "{\"id\":\"createRoom\"}")
    }
    
    func responseHandler(id: String, response: JSON) {
        switch id {
        
        case "registerResponse":
            if onCreateRoom != nil { createRoom() }
            isRegistered = true
            
        case "createRoomResponse":
            onCreateRoomHandler(response: response)
            
        case "inviteResponse":
            guard let response = response["response"].string else { return }
            onInvite?(response == "accepted")
            
        case "userLeaveRoom":
            onDisconnect?(nil)
            
        default: ()
        }
    }
    
    func onCreateRoomHandler(response: JSON) {
        guard
            let ovToken = response["ovToken"].string,
            let ovSessionId = response["ovSessionId"].string
            else { return }
        onCreateRoom?(.Success(RoomResponse(token: ovToken, session: ovSessionId)))
        onCreateRoom = nil // remove handler
    }
    
}


// MARK: WebSocketDelegate

extension XlanceRTC {
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("#XlanceRTC connect")
        register()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("#XlanceRTC disonnect \(String(describing: error))")
        onDisconnect?(error?.localizedDescription)
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("#XlanceRTC response \(text)")
        let json = JSON(parseJSON: text)
        guard let responseId = json["id"].string else { return }
        responseHandler(id: responseId, response: json)
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("#XlanceRTC receive data")
    }
    
}

