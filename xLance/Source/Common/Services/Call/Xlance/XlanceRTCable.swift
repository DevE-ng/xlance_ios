//
//  XlanceSocketable.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

protocol XlanceRTCable: class {
    var onDisconnect: ((String?) -> Void)? { get set }
    var onInvite: ((Bool) -> Void)? { get set }
    func createRoom(completionHandler: @escaping (Result<RoomResponse>) -> Void)
    func initeUserInRoom(user: String, session: String)
}
