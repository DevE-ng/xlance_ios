//
//  XlanceOpenVidu.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import Starscream
import SwiftyJSON

class OpenViduSocket: OpenViduSocketable, WebSocketDelegate {
    
    // callbacks
    var onConnect:          ( (Result<Bool>) -> Void )?
    var onUpdateAnswer:     ( ([String : JSON]) -> Void )?
    var onUpdateSession:    ( ([String : JSON]) -> Void )?
    var onIceCandidate:     ( ([String : JSON]) -> Void )?
    var onUserJoined:       ( ([String : JSON]) -> Void )?
    var onUserPublished:    ( ([String : JSON]) -> Void )?
    var onUserLeft:         ( ([String : JSON]) -> Void )?
    
    // public vars
    var maxID: Int = 0
    
    // private let
    private let url = CONSTANTS.openvidu_socket_url
    private let token: String
    private let session: String
    private let socket: WebSocket
    
    // private vars
    private var pingTimer: Timer?
    private var iceCandidates: [[String:String]]?
    
    init(token: String, session: String) {
        self.token = token
        self.session = session
        self.socket = WebSocket(url: URL(string: url)!)
        socket.disableSSLCertValidation = true
        socket.delegate = self
        socket.connect()
    }
    
    deinit {
        print("#OpenViduSocket deinit")
        disconnect()
    }
    
    func disconnect() {
        socket.disconnect()
        pingTimer?.invalidate()
    }
    
    func sendMessage(method: String, params: [String: String]) {
        debugPrint("#OpenViduSocket send message \(method)", params)
        let json: NSMutableDictionary = NSMutableDictionary()
        
        json.setValue(OV_JSON.JsonRPC_VERSION, forKey: OV_JSON.JsonRPC)
        json.setValue(method, forKey: OV_JSON.Method)
        json.setValue(params, forKey: OV_JSON.Params)
        json.setValue(maxID, forKey: OV_JSON.Id)
        
        guard
            let jsonData = try? JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions()) as NSData,
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as String?
            else { return }
        
        socket.write(string: jsonString)
        maxID += 1
    }
    
    func addIceCandidate(_ candidate: [String : String]) {
        iceCandidates?.append(candidate)
    }
    
}

fileprivate extension OpenViduSocket {
    
    func startPing() {
        pingTimer = Timer.scheduledTimer(timeInterval: 5,
                                         target: self,
                                         selector: #selector(pingHandler),
                                         userInfo: nil,
                                         repeats: true)
        pingHandler()
    }
    
    @objc func pingHandler() {
        var params: [String: String] = [:]
        params["interval"] = "5000"
        sendMessage(method: "ping", params: params)
        socket.write(ping: Data())
    }
    
    func joinRoom() {
        var params: [String: String] = [:]
        params["token"] = token
        params["session"] = session
        params["platform"] = "iOS"
        params["metadata"] = ""
        params["secret"] = ""
        params["recorder"] = "false"
        sendMessage(method: "joinRoom", params: params)
    }
    
    func websocketResultHandler(_ result: [String: JSON]) {
        if result[OV_JSON.SdpAnswer]?.string != nil {
            onUpdateAnswer?(result)
        } else if result[OV_JSON.SessionId]?.string != nil {
            onUpdateSession?(result)
            updateIceCandidates()
        }
    }
    
    func updateIceCandidates() {
        let _ = iceCandidates?.compactMap {
            sendMessage(method: "onIceCandidate", params: $0)
        }
    }
    
    func websocketMessageHandler(_ json: JSON) {
        guard
            let method = json[OV_JSON.Method].string,
            let params = json[OV_JSON.Params].dictionary else { return }
        
        switch method {
            
        case OV_JSON.IceCandidate:             onIceCandidate?(params)
        case OV_JSON.ParticipantJoined:        onUserJoined?(params)
        case OV_JSON.ParticipantPublished:     onUserPublished?(params)
        case OV_JSON.ParticipantLeft:          onUserLeft?(params)
            
        default:
            print("Error")
        }
    }
    
}


// MARK: WebSocketDelegate

extension OpenViduSocket {
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("#OpenViduSocket connect")
        startPing()
        joinRoom()
        onConnect?(.Success(true))
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("#OpenViduSocket disconnect")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        debugPrint("#OpenViduSocket response", text)
        let json = JSON(parseJSON: text)
        if let result = json[OV_JSON.Result].dictionary {
            websocketResultHandler(result)
        } else {
            websocketMessageHandler(json)
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        //
    }
    
}

