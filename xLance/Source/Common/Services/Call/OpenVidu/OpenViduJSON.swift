//
//  OpenViduConstants.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

enum OV_JSON {
    static let Value = "value"
    static let Params = "params"
    static let Method = "method"
    static let Id = "id"
    static let Result = "result"
    static let IceCandidate = "iceCandidate"
    static let ParticipantJoined = "participantJoined"
    static let ParticipantPublished = "participantPublished"
    static let ParticipantLeft = "participantLeft"
    static let SessionId = "sessionId"
    static let SdpAnswer = "sdpAnswer"
    static let SdpOffer = "sdpOffer"
    static let JoinRoom = "joinRoom"
    static let Metadata = "metadata"
    static let JsonRPC = "jsonrpc"
    static let JsonRPC_VERSION = "2.0"
    static let Endpoint = "endpointName"
}
