//
//  OpenViduSocketable.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol OpenViduSocketable: class {
    
    var onConnect:           ( (Result<Bool>) -> Void )? { get set }
    var onUpdateAnswer:      ( ([String: JSON]) -> Void )? { get set }
    var onUpdateSession:     ( ([String: JSON]) -> Void )? { get set }
    var onIceCandidate:      ( ([String: JSON]) -> Void )? { get set }
    var onUserJoined:        ( ([String: JSON]) -> Void )? { get set }
    var onUserPublished:     ( ([String: JSON]) -> Void )? { get set }
    var onUserLeft:          ( ([String: JSON]) -> Void )? { get set }
    
    var maxID: Int { get set }
    
    func sendMessage(method: String, params: [String: String])
    func addIceCandidate(_ candidate: [String: String])
    func disconnect()
    
}
