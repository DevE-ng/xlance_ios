//
//  Participiant.swift
//  xlanceWebRTC
//
//  Created by alobanov11 on 07/06/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import Foundation
import WebRTC

final class RTCPeerUser {
    
    var id: String!
    var index: Int!
    var participantName: String!
    var peerConnection: RTCPeerConnection!
    
    deinit {
        print("#RTCPeerUser deinit")
    }
    
}
