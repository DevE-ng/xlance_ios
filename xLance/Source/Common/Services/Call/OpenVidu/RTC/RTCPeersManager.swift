//
//  PeersManager.swift
//  xlanceWebRTC
//
//  Created by alobanov11 on 07/06/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import Foundation
import WebRTC
import SwiftyJSON

class RTCPeersManager: NSObject, RTCPeerConnectionDelegate {
    
    // callbacks
    var onIceCandidate:         ( (_ candidate: [String: String]) -> Void )?
    var onAddIceCandidate:      ( (_ candidate: [String: String]) -> Void )?
    var onLocalOffer:           ( (_ params: [String:String]) -> Void )?
    var onRemoteOffer:          ( (_ params: [String:String]) -> Void )?
    var onSetRemoteDescription: ( () -> Void )?
    
    var localUserId: String? {
        didSet {
            print("#RTCPeersManager localUserId \(String(describing: localUserId))")
        }
    }
    
    deinit {
        print("#RTCPeersManager deinit")
    }
    
    // vars
    private var localPeer: RTCPeerConnection!
    private var localCapturer: RTCCameraVideoCapturer!
    private var remotePeerUser: RTCPeerUser!
    private var remoteStreams: [RTCMediaStream] = []
    
    // default
    private let peerConnectionFactory = RTCPeerConnectionFactory(encoderFactory: RTCDefaultVideoEncoderFactory(),
                                                         decoderFactory: RTCDefaultVideoDecoderFactory())
    
    func countRemoteStreams() -> Int {
        return remoteStreams.count
    }
    
    func addIceCandidate(_ json: [String: JSON]) {
        guard
            let sdp = json["candidate"]?.string,
            let sdpMLineIndex = json["sdpMLineIndex"]?.int32,
            let sdpMid = json["sdpMid"]?.string else { return }
        
        let iceCandidate = RTCIceCandidate(sdp: sdp, sdpMLineIndex: sdpMLineIndex, sdpMid: sdpMid)
        
        if json[OV_JSON.Endpoint]?.string == localUserId {
            localPeer.add(iceCandidate)
        } else {
            remotePeerUser.peerConnection.add(iceCandidate)
        }
    }
    
    func createPeerUser(_ json: [String: JSON]) {
        print("#RTCPeersManager createPeerUser")
        
        guard
            let userID = json[OV_JSON.Id]?.string,
            let metaString = json[OV_JSON.Metadata]?.string,
            let meta = JSON(parseJSON: metaString).dictionary,
            let email = meta["userEmail"]?.string else { return }
        
        let peerUser = RTCPeerUser()
        peerUser.id = userID
        peerUser.participantName = email
        
        remotePeerUser = peerUser
        createRemotePeerConnection()
    }
    
    func updatePeerUser(renderer: RTCVideoRenderer, index: Int) {
        guard let videoTrack = remoteStreams.last?.videoTracks[0] else { return }
        videoTrack.add(renderer)
        remotePeerUser.index = index
    }
    
    /*------------------------------------------
     MARK: LOCAL
     ------------------------------------------*/
    
    func createLocalPeerConnection() {
        print("#RTCPeersManager createLocalPeerConnection")
        let mandatoryConstraints = [
            "OfferToReceiveAudio": "true",
            "OfferToReceiveVideo": "true"
        ]
        let sdpConstraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints,
                                                 optionalConstraints: nil)
        let config = createConfiguration()
        
        localPeer = peerConnectionFactory.peerConnection(with: config,
                                                         constraints: sdpConstraints,
                                                         delegate: nil)
        localPeer.delegate = self
    }
    
    func createLocalMedia(renderer: RTCVideoRenderer) {
        print("#RTCPeersManager createLocalMedia")
        createLocalCapturer(renderer: renderer)
        startLocalCaptureVideo()
        createLocalOffer()
    }
    
    func createLocalCapturer(renderer: RTCVideoRenderer) {
        print("#RTCPeersManager createLocalCapturer")
        
        let streamId = "streamLocal"
        let stream = peerConnectionFactory.mediaStream(withStreamId: streamId)
        
        // Audio
        let audioConstrains = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let audioSource = peerConnectionFactory.audioSource(with: audioConstrains)
        let audioTrack = peerConnectionFactory.audioTrack(with: audioSource, trackId: "audioLocal")
        stream.addAudioTrack(audioTrack)
        
        // Video
        let videoSource = peerConnectionFactory.videoSource()
        localCapturer = RTCCameraVideoCapturer(delegate: videoSource)
        let videoTrack = peerConnectionFactory.videoTrack(with: videoSource, trackId: "videoLocal")
        videoTrack.add(renderer)
        stream.addVideoTrack(videoTrack)

        localPeer.add(stream)
    }
    
    func startLocalCaptureVideo() {
        guard
            
            let frontCamera = (RTCCameraVideoCapturer.captureDevices().first { $0.position == .front }),
            
            // choose highest res
            let format = (RTCCameraVideoCapturer.supportedFormats(for: frontCamera).sorted { (f1, f2) -> Bool in
                let width1 = CMVideoFormatDescriptionGetDimensions(f1.formatDescription).width
                let width2 = CMVideoFormatDescriptionGetDimensions(f2.formatDescription).width
                return width1 < width2
            }).last,
            
            // choose highest fps
            let fps = (format.videoSupportedFrameRateRanges.sorted { return $0.maxFrameRate < $1.maxFrameRate }.last) else {
                return
        }
        
        localCapturer.startCapture(with: frontCamera,
                              format: format,
                              fps: Int(fps.maxFrameRate)) { _ in
            print("#RTCPeersManager startLocalCaptureVideo startCapture \(frontCamera) \(format) \(fps)")
        }
        
        print("#RTCPeersManager startLocalCaptureVideo")
    }
    
    func createLocalOffer() {
        print("#RTCPeersManager createLocalOffer")
        
        let mandatoryConstraints = [
            "OfferToReceiveAudio": "true",
            "OfferToReceiveVideo": "true"
        ]
        let mediaConstraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        
        localPeer.offer(for: mediaConstraints) { [weak self] (sessionDescription, error) in
            guard let `self` = self else { return }
            
            self.localPeer.setLocalDescription(sessionDescription!) { error in
                print("Local Peer local Description set: error \(String(describing: error))")
            }
            
            var localOfferParams: [String:String] = [:]
            localOfferParams[OV_JSON.SdpOffer] = sessionDescription!.sdp
            localOfferParams["doLoopback"] = "false"
            localOfferParams["hasAudio"] = "true"
            localOfferParams["hasVideo"] = "true"
            localOfferParams["audioActive"] = "true"
            localOfferParams["videoActive"] = "true"
            localOfferParams["typeOfVideo"] = "CAMERA"
            localOfferParams["frameRate"] = "30"
            localOfferParams["videoDimesions"] = "{\"width\":640, \"height\":480}"
            
            self.onLocalOffer?(localOfferParams)
        }
    }
    
    func toggleLocalVideo() -> Bool? {
        guard
            let stream = localPeer.localStreams.first,
            let videoTrack = stream.videoTracks.first else {
                return nil
        }
        let newValue = !videoTrack.isEnabled
        videoTrack.isEnabled = newValue
        print("#RTCPeersManager toggleLocalVideo \(newValue)")
        return newValue
    }
    
    func toggleLocalAudio() -> Bool? {
        guard
            let stream = localPeer.localStreams.first,
            let audioTrack = stream.audioTracks.first else {
                return nil
        }
        let newValue = !audioTrack.isEnabled
        audioTrack.isEnabled = newValue
        print("#RTCPeersManager toggleLocalAudio \(newValue)")
        return newValue
    }
    
    /*------------------------------------------
     MARK: REMOTE
     ------------------------------------------*/
    
    func createRemotePeerConnection() {
        let mandatoryConstraints = [ "OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true" ]
        let sdpConstraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        let config = createConfiguration()
        let remotePeer = peerConnectionFactory.peerConnection(with: config, constraints: sdpConstraints, delegate: nil)
        remotePeer.delegate = self
        remotePeerUser.peerConnection = remotePeer
    }
    
    func setRemoteDescription(_ json: [String: JSON]) {
        guard let sdp = json[OV_JSON.SdpAnswer]?.string else { return }
        let sessionDescription = RTCSessionDescription(type: RTCSdpType.answer, sdp: sdp)
        
        if localPeer.remoteDescription != nil {
            remotePeerUser.peerConnection.setRemoteDescription(sessionDescription) { [weak self] error in
                print("Remote Peer Remote Description set: error \(String(describing: error))")
                DispatchQueue.main.async {
                    self?.onSetRemoteDescription?()
                }
            }
        } else {
            localPeer.setRemoteDescription(sessionDescription) { error in
                print("Local Peer Remote Description set: error \(String(describing: error))")
            }
        }
    }
    
    func setLocalDescription(_ json: [String: JSON]) {
        let mandatoryConstraints = ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"]
        let sdpConstraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        
        remotePeerUser.peerConnection.offer(for: sdpConstraints) { [weak self] (sessionDescription, error) in
            guard let `self` = self else { return }
            
            self.remotePeerUser.peerConnection.setLocalDescription(sessionDescription!) { error in
                print("Remote Peer Local Description set: error \(String(describing: error))")
            }
            
            var remoteOfferParams:  [String: String] = [:]
            remoteOfferParams[OV_JSON.SdpOffer] = sessionDescription!.sdp
            remoteOfferParams["sender"] = self.remotePeerUser.id + "_webcam"
            
            self.onRemoteOffer?(remoteOfferParams)
        }
    }
    
    func toggleRemoteAudio() -> Bool? {
        guard
            let stream = remoteStreams.first,
            let audioTrack = stream.audioTracks.first else {
                return nil
        }
        let newValue = !audioTrack.isEnabled
        audioTrack.isEnabled = newValue
        print("#RTCPeersManager toggleRemoteAudio \(newValue)")
        return newValue
    }
    
    
    /*------------------------------------------
     MARK: RTCPeerConnectionDelegate
     ------------------------------------------*/
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        var status = ""
        
        switch stateChanged {
        case .stable: status = "stable"
        case .closed: status = "closed"
        case .haveLocalOffer: status = "have local offer"
        case .haveLocalPrAnswer: status = "have local pr answer"
        case .haveRemoteOffer: status = "have remote offer"
        case .haveRemotePrAnswer: status = "have remove pr answer"
        default: ()
        }
        
        print("#RTCPeersManager peerConnection didChange stateChanged: \(status)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        print("#RTCPeersManager didAdd stream \(stream.streamId)")
        if peerConnection != localPeer {
            remoteStreams.append(stream)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        print("#RTCPeersManager didRemove stream")
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        print("#RTCPeersManager peerConnection negotiate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        print("#RTCPeersManager peerConnection new connection state: \(newState.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        print("#RTCPeersManager peerConnection new gathering state: \(newState.rawValue)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        var iceCandidateParams: [String: String] = [:]
        
        iceCandidateParams["sdpMid"] = candidate.sdpMid
        iceCandidateParams["sdpMLineIndex"] = String(candidate.sdpMLineIndex)
        iceCandidateParams["candidate"] = String(candidate.sdp)
        
        if peerConnection == localPeer {
            if localUserId != nil {
                iceCandidateParams["endpointName"] = localUserId
                print("#RTCPeersManager didGenerate onIceCandidate")
                onIceCandidate?(iceCandidateParams)
            } else {
                onAddIceCandidate?(iceCandidateParams)
            }
        } else {
            iceCandidateParams["endpointName"] = remotePeerUser.id
            onIceCandidate?(iceCandidateParams)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        print("#RTCPeersManager didRemove candidates")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        print("#RTCPeersManager didOpen dataChannel")
    }
    
}


/*------------------------------------------
 MARK: Private
 ------------------------------------------*/

fileprivate extension RTCPeersManager {
    
    func createConfiguration() -> RTCConfiguration {
        let config = RTCConfiguration()
        config.iceServers = [
            RTCIceServer(urlStrings: [CONSTANTS.turn_url],
                         username: CONSTANTS.turn_name,
                         credential: CONSTANTS.turn_password),
        ]
        
        config.bundlePolicy = .maxCompat
        config.rtcpMuxPolicy = .require
        return config
    }
    
}
