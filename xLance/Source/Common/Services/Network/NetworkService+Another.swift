//
//  NetworkService+Another.swift
//  xlance
//
//  Created by Anton Lobanov on 10/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

extension NetworkService {
    
    func fetchCategories(completionHandler: @escaping (Result<[CategoryModel]>) -> Void) {
        requestForArray(target: .categories, completionHandler: completionHandler)
    }
    
}
