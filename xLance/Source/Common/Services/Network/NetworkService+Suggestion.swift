//
//  NetworkService+Suggestion.swift
//  xlance
//
//  Created by alobanov11 on 22/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

extension NetworkService {
    
    func fetchSuggestion(_ suggestionId: Int, completionHandler: @escaping (Result<SuggestionModel>) -> Void) {
        requestFor(target: .suggestion(id: suggestionId), completionHandler: completionHandler)
    }
    
    func createSuggestion(_ suggestion: SuggestionModel, completionHandler: @escaping (Result<SuggestionModel>) -> Void) {
        requestFor(target: .createSuggestion(suggestion), completionHandler: completionHandler)
    }
    
    func editSuggestion(_ suggestion: SuggestionModel, completionHandler: @escaping (Result<SuggestionModel>) -> Void) {
        requestFor(target: .editedSuggestion(suggestion), completionHandler: completionHandler)
    }
    
    func acceptSuggestion(_ suggestion: SuggestionModel, completionHandler: @escaping (Result<ProjectModel>) -> Void) {
        requestFor(target: .acceptSuggestion(id: suggestion.id), completionHandler: completionHandler)
    }
    
    func declineSuggestion(_ suggestionId: Int, completionHandler: @escaping (Result<Bool>) -> Void) {
        requestScalar(target: .declineSuggestion(id: suggestionId), completionHandler: completionHandler)
    }
    
    func fetchProposals(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void) {
        requestForArray(target: .proposals, completionHandler: completionHandler)
    }
    
    func fetchOffers(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void) {
        requestForArray(target: .offers, completionHandler: completionHandler)
    }
    
}
