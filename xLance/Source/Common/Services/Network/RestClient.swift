//
//  XlanceNetworking.swift
//  xlance
//
//  Created by alobanov11 on 18/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

enum RestClient {
    
    case login(name: String, password: String)
    case logout
    case forgot(email: String)
    case register(data: AuthModel)
    case changePassword(id: Int, password: String, new: String, confirm: String)
    
    case projects(index: Int, size: Int)
    case freelancersProjects
    case project(id: Int)
    
    case suggestion(id: Int)
    case createSuggestion(_ suggestion: SuggestionModel)
    case editedSuggestion(_ suggestion: SuggestionModel)
    case acceptSuggestion(id: Int)
    case declineSuggestion(id: Int)
    
    case offers
    case proposals
    
    case categories
    case statuses(params: [String: Any])
    
    case messages(conversationId: Int)
    
}

// MARK: - TargetType Protocol Implementation
extension RestClient: TargetType, AccessTokenAuthorizable {
    var baseURL: URL { return URL(string: CONSTANTS.xlance_full_url_with_port)! }
    var authorizationType: AuthorizationType {
        return .bearer
    }
    var path: String {
        switch self {
            
        case .login(_, _):
            return "/auth/login"
            
        case .logout:
            return "/api/profiles/logout"
            
        case .forgot(_):
            return "/api/profiles/forgotPassword"
        case .register(_):
            return "/api/profiles/register"
        case .changePassword(_, _, _, _):
            return "/api/profiles/update/changePassword"
            
        case .projects(_, _):
            return "/api/projects/filtered"
        case .freelancersProjects:
            return "/api/projects/freelancersProjects"
        case .project(let id):
            return "/api/projects/\(id)"
            
        case .createSuggestion(_):
            return "/api/suggestions"
        case .editedSuggestion(_):
            return "/api/suggestions"
        case .suggestion(let id):
            return "/api/suggestions/\(id)"
        case .acceptSuggestion(let id):
            return "/api/suggestions/\(id)/actions/accept"
        case .declineSuggestion(let id):
            return "/api/suggestions/\(id)/actions/decline"
            
        case .offers:
            return "/api/suggestions/offers"
        case .proposals:
            return "/api/suggestions/proposals"
            
        case .categories:
            return "/api/categories"
            
        case .statuses(_):
            return "/api/profiles/statuses"
            
        case .messages(let conversationId):
            return "/api/messages/\(conversationId)"
            
        }
    }
    var method: Moya.Method {
        switch self {
            
        case .login,
             .logout,
             .forgot,
             .register,
             .createSuggestion:
            return .post
            
        case .changePassword,
             .editedSuggestion,
             .acceptSuggestion,
             .declineSuggestion:
            return .put
            
        case .freelancersProjects,
             .projects,
             .offers,
             .proposals,
             .project,
             .suggestion,
             .categories,
             .statuses,
             .messages:
            return .get
            
        }
    }
    var task: Task {
        switch self {
            
        case .login(let name, let password):
            return .requestParameters(parameters: ["username": name,
                                                   "password": password],
                                      encoding: JSONEncoding.default)
        case .forgot(let email):
            return .requestData(email.data(using: .utf8) ?? Data())
            
        case .register(let data):
            return .requestParameters(parameters: data.makeDictionaryWithValues(),
                                      encoding: JSONEncoding.default)
            
        case .changePassword(let id, let password, let new, let confirm):
            return .requestParameters(parameters: ["id": "\(id)",
                "password": password,
                "new": new,
                "confirm": confirm],
                                      encoding: JSONEncoding.default)
            
        case .projects(let index, let size):
            return .requestParameters(parameters: [//"showAccepted":"true",
                                                   //"showDeclined":"true",
                                                   //"showWaitingForAgreement": "true",
                                                   "pageIndex": index,
                                                   "pageSize": size,
                                                   "sortBy": "NEWEST_FIRST"],
                                      encoding: URLEncoding.default)
            
        case .createSuggestion(let suggestion):
            return .requestParameters(parameters: suggestion.toJSON(),
                                      encoding: JSONEncoding.default)
            
        case .editedSuggestion(let suggestion):
            return .requestParameters(parameters: suggestion.toJSON(),
                                      encoding: JSONEncoding.default)
            
        case .statuses(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case .logout,
             .freelancersProjects,
             .offers,
             .proposals,
             .project,
             .acceptSuggestion,
             .declineSuggestion,
             .suggestion,
             .categories,
             .messages:
            return .requestPlain
            
        }
    }
    var sampleData: Data {
        switch self {
        case .login,
             .logout,
             .forgot,
             .register,
             .changePassword,
             .offers,
             .proposals,
             .projects,
             .freelancersProjects,
             .project,
             .createSuggestion,
             .editedSuggestion,
             .acceptSuggestion,
             .declineSuggestion,
             .suggestion,
             .categories,
             .statuses,
             .messages:
            return "".utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
