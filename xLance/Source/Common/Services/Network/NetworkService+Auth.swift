//
//  NetworkService+Auth.swift
//  xlance
//
//  Created by alobanov11 on 22/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

extension NetworkService {
    
    func login(name: String, password: String, completionHandler: @escaping (Result<LoginResponse>) -> Void) {
        requestFor(target: .login(name: name, password: password), completionHandler: completionHandler)
    }
    
    func logout(completionHandler: @escaping (Result<Bool>) -> Void) {
        requestScalar(target: .logout, completionHandler: completionHandler)
    }
    
    func forgot(email: String, completionHandler: @escaping (Result<Bool>) -> Void) {
        requestScalar(target: .forgot(email: email), completionHandler: completionHandler)
    }
    
    func register(data: AuthModel, completionHandler: @escaping (Result<Bool>) -> Void) {
        requestScalar(target: .register(data: data), completionHandler: completionHandler)
    }
    
    func changePassword(id: Int, old: String, new: String, confirm: String, completionHandler: @escaping (Result<Bool>) -> Void) {
        requestScalar(target: .changePassword(id: id,
                                              password: old,
                                              new: new,
                                              confirm: confirm),
                      completionHandler: completionHandler)
    }
    
}
