//
//  NetworkService.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON
import ObjectMapper

typealias JSONObject = Any

class NetworkService: Networking {
    
    var token: String? {
        didSet {
            if token == oldValue { return }
            var plugins: [PluginType] = []
//            plugins.append(NetworkLoggerPlugin(verbose: true))
            if let token = token {
                plugins.append(AccessTokenPlugin(tokenClosure: { token })) // it is struct
            }
            self.networkProvider = MoyaProvider<RestClient>(plugins: plugins)
        }
    }
    
    lazy var networkProvider: MoyaProvider<RestClient> = {
        var plugins: [PluginType] = []
        //plugins.append(NetworkLoggerPlugin(verbose: true))
        return MoyaProvider<RestClient>(plugins: plugins)
    }()
    
    func request(_ target: RestClient, completionHandler: @escaping (Result<JSONObject?>) -> Void) {
        networkProvider.request(target) { result in
            switch result {
                
            case let .success(response):
                let data = response.data
                let statusCode = response.statusCode
                
                if statusCode >= 200 && statusCode <= 300 {
                    let json = try? response.mapJSON()
                    completionHandler(.Success(json))
                    return
                }
                
                if let message = JSON(data)["message"].string {
                    completionHandler(.Error(message.localized))
                } else {
                    completionHandler(.Error("SERVER_ERROR.INTERNAL_SERVER_ERROR".localized))
                }
                
            case let .failure(error):
                completionHandler(.Error(error.localizedDescription))
                
            }
        }
    }
    
    func requestScalar(target: RestClient, completionHandler: @escaping (Result<Bool>) -> Void) {
        request(target) { result in
            
            switch result {
            case .Success(_):
                DispatchQueue.main.async {
                    completionHandler(.Success(true))
                }
                
            case .Error(let error):
                completionHandler(.Error(error))
            }
        }
    }
    
    func requestFor<T>(target : RestClient, completionHandler: @escaping (Result<T>) -> Void) where T: BaseMappable {
        request(target) { result in
            switch result {
            case .Success(let jsonObject):
                guard let result = Mapper<T>().map(JSONObject: jsonObject) else {
                    completionHandler(.Error("Failure data"))
                    return
                }
                DispatchQueue.main.async {
                    completionHandler(.Success(result))
                }
                
            case .Error(let error):
                completionHandler(.Error(error))
            }
        }
    }
    
    func requestForArray<T>(target: RestClient, completionHandler: @escaping (Result<[T]>) -> Void) where T: BaseMappable {
        request(target) { result in
            switch result {
            case .Success(let jsonObject):
                guard let result = Mapper<T>().mapArray(JSONObject: jsonObject) else {
                    completionHandler(.Error("Failure data"))
                    return
                }
                DispatchQueue.main.async {
                    completionHandler(.Success(result))
                }
                
            case .Error(let error):
                completionHandler(.Error(error))
            }
        }
    }
    
}
