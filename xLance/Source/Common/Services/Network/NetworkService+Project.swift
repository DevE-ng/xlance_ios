//
//  NetworkService+Projects.swift
//  xlance
//
//  Created by alobanov11 on 22/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

extension NetworkService {
    
    func fetchProjects(index: Int, size: Int, completionHandler: @escaping (Result<ProjectsResponse>) -> Void) {
        requestFor(target: .projects(index: index, size: size), completionHandler: completionHandler)
    }
    
    func fetchProject(_ id: Int, completionHandler: @escaping (Result<ProjectModel>) -> Void) {
        requestFor(target: .project(id: id), completionHandler: completionHandler)
    }
    
    func fetchFreelancersProjects(completionHandler: @escaping (Result<[ProjectModel]>) -> Void) {
        requestForArray(target: .freelancersProjects, completionHandler: completionHandler)
    }
    
    func fetchStatuses(profiles: [ProfileModel], completionHandler: @escaping (Result<[StatusModel]>) -> Void) {
        var params = [String: String]()
        for profile in profiles { params[profile.id.string] = profile.rabbitmqHash.string }
        requestForArray(target: .statuses(params: params), completionHandler: completionHandler)
    }
    
    func fetchMessages(conversationId: Int, completionHandler: @escaping (Result<[MessageModel]>) -> Void) {
        requestForArray(target: .messages(conversationId: conversationId), completionHandler: completionHandler)
    }
    
}
