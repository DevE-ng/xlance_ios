//
//  CommunicatorService+IM.swift
//  xlance
//
//  Created by Anton Lobanov on 31/07/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChatService: Chating {
    
    var profile: ProfileModel? {
        didSet {
            if profile == oldValue { return }
            createSocket()
        }
    }
    
    var socket: XlanceIMable?
    weak var delegate: ChattingDelegate?
    
    // Public
    
    func subscribeOn(project: ProjectModel, delegate: ChattingDelegate) {
        guard let profile = profile else { return }
        let queue = "conversation_\(project.conversationId!)_user_\(profile.id)_\(profile.rabbitmqHash).queue"
        socket?.subscribe(queue: queue)
        self.delegate = delegate
        setupCallbacks()
    }
    
    func subscribeOff(project: ProjectModel) {
        guard let profile = profile else { return }
        let queue = "conversation_\(project.conversationId!)_user_\(profile.id)_\(profile.rabbitmqHash).queue"
        self.delegate = nil
        socket?.unsubscribe(queue: queue)
    }
    
    func sendMessage(text: String, project: ProjectModel) {
        guard let profile = profile else { return }
        let destination = "/exchange/conversation.gate/\(project.conversationKey!)"
        var headers = [String: String]()
        PublishMessageResponse(from: profile, project: project).toJSON().forEach {
            if let value = $0.value as? String {
                headers[$0.key] = value
            }
        }
        socket?.publishMessage(text: text, destination: destination, headers: headers)
    }
    
    // Private
    
    private func createSocket() {
        guard let profile = profile else {
            socket = nil
            return
        }
        socket = XlanceIM(url: CONSTANTS.xlance_stomp_url,
                          headers: ["login": profile.login, "passcode": profile.passcode, "host": "chatRooms"])
    }
    
    private func setupCallbacks() {
        socket?.onReceiveMessage = { [weak self] (message: String?, headers: [String: String]?) in
            guard
                let id = headers?["id"],
                let message = message,
                let userId = headers?["from"] else {
                return
            }
            self?.delegate?.didReceiveMessage(MessageSimpleModel(id: id.int, message: message, userId: userId.int))
        }
    }
    
}
