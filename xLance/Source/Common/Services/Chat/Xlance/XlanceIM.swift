//
//  XlanceStomp.swift
//  xlance
//
//  Created by alobanov11 on 28/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import SwiftyJSON

class XlanceIM: XlanceIMable {
    
    var onReceiveMessage: ((_ message: String?, _ headers: [String: String]?) -> Void)?
    var onDisconnect: (() -> Void)?
    
    private let socket = StompClientLib()
    private var request: URLRequest
    private let connectionHeaders: [String: String]
    private var isConnected: Bool = false
    private var subscribeQueue: String? = nil
        
    init(url: String, headers: [String: String]) {
        print("#XlanceIM init \(headers)")
        request = URLRequest(url: URL(string: url)!)
        connectionHeaders = headers
        socket.openSocketWithURLRequest(request: request as NSURLRequest,
                                        delegate: self,
                                        connectionHeaders: connectionHeaders)
    }
    
    deinit {
        socket.disconnect()
        print("#XlanceIM deinit")
    }
    
    func subscribe(queue: String) {
        if isConnected {
            print("#XlanceIM subscribe \(queue)")
            socket.subscribe(destination: queue)
        } else {
            subscribeQueue = queue
        }
    }
    
    func unsubscribe(queue: String) {
        print("#XlanceIM unsubscribe \(queue)")
        socket.unsubscribe(destination: queue)
    }
    
    func publishMessage(text: String, destination: String, headers: [String: String]) {
        socket.sendMessage(message: text,
                           toDestination: destination,
                           withHeaders: headers,
                           withReceipt: nil)
    }
    
}

// MARK: - StompClientLibDelegate
extension XlanceIM: StompClientLibDelegate {
    
    func stompClientDidConnect(client: StompClientLib!) {
        isConnected = true
        if let queue = subscribeQueue {
            subscribe(queue: queue)
            subscribeQueue = nil
        }
        socket.reconnect(request: request as NSURLRequest,
                         delegate: self,
                         connectionHeaders: connectionHeaders)
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessage message: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("#XlanceIM message")
        onReceiveMessage?(message, header)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("#XlanceIM disconnect")
        onDisconnect?()
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("#XlanceIM receipt \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("#XlanceIM error \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        print("#XlanceIM ping")
    }
    
}
