//
//  XlanceIMable.swift
//  xlance
//
//  Created by alobanov11 on 01/07/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

protocol XlanceIMable: class {
    
    /// Receive message callback(message, headers, destination)
    var onReceiveMessage: ((_ message: String?, _ headers: [String: String]?) -> Void)? { get set }
    var onDisconnect: (() -> Void)? { get set }
    
    func subscribe(queue: String)
    func unsubscribe(queue: String)
    func publishMessage(text: String, destination: String, headers: [String: String])
        
}
