//
//  MainCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol MainCoordinatorDelegate: class {
    func userPerformedLogout(coordinator: BaseCoordinator)
}
protocol MainCoordinatorFactory: ProjectListCoordinatorFactory, ChatListCoordinatorFactory, ProfileCoordinatorFactory {
    func makeMainView(coordinator: MainCoordinatable) -> Presentable
}

class MainCoordinator: BaseCoordinator {
    
    weak var delegate: MainCoordinatorDelegate?
    var factory: MainCoordinatorFactory
    
    private let isAnonymous: Bool
    
    init(isAnonymous: Bool, factory: MainCoordinatorFactory, router: Routable) {
        self.isAnonymous = isAnonymous
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showMain()
    }
    
}

// MARK: - MainCoordinatable
extension MainCoordinator: MainCoordinatable {
    
}

// MARK: - ProjectListCoordinatorDelegate
extension MainCoordinator: ProjectListCoordinatorDelegate {
    func didUpdateProject(_ project: ProjectModel) {
        let _ = childCoordinators.map {
            ($0.value as? ProjectCoordinatorUpdating)?.didUpdateProject(project)
        }
    }
}

// MARK: - ProfileCoordinatorDelegate
extension MainCoordinator: ProfileCoordinatorDelegate {
    func userPerformedLogout() {
        router.dismiss()
        childCoordinators.removeAll()
        delegate?.userPerformedLogout(coordinator: self)
    }
}

// MARK: - ChatListCoordinatorDelegate
extension MainCoordinator: ChatListCoordinatorDelegate {
    
}

fileprivate extension MainCoordinator {
    
    func showMain() {
        let view = factory.makeMainView(coordinator: self)
        prepareView(view)
        router.present(view)
    }
    
    func prepareView(_ view: Presentable) {
        let tabBar = view.toPresent as? UITabBarController
        var tabs = Dictionary(uniqueKeysWithValues: (tabBar?.viewControllers ?? []).enumerated().map({ ($0, $1) }))
        
        for (index, tab) in tabs {
        
            if
                let navVC = tab as? UINavigationController,
                let _ = navVC.viewControllers.first as? ProjectListViewProtocol {
                if CurrentUser.isEmployer {
                    tabs.removeValue(forKey: index)
                } else {
                    prepareProjectListView(isMyProjects: false, navVC: navVC)
                }
            }
            
            if
                let navVC = tab as? UINavigationController,
                let _ = navVC.viewControllers.first as? MyProjectListViewProtocol {
                if isAnonymous {
                    tabs.removeValue(forKey: index)
                } else {
                    prepareProjectListView(isMyProjects: true, navVC: navVC)
                }
            }
            
            if
                let navVC = tab as? UINavigationController,
                let _ = navVC.viewControllers.first as? ChatListViewProtocol {
                if isAnonymous {
                    tabs.removeValue(forKey: index)
                } else {
                    prepareChatListView(navVC: navVC)
                }
            }
            
            if
                let navVC = tab as? UINavigationController,
                let _ = navVC.viewControllers.first as? ProfileViewProtocol {
                prepareProfileView(navVC: navVC)
            }
            
        }
        
        tabBar?.setViewControllers(tabs.sorted(by: { $0.key < $1.key }).map({ $0.value }), animated: false)
    }
    
    func prepareProjectListView(isMyProjects: Bool, navVC: UINavigationController) {
        let coordinator = ProjectListCoordinator(isMyProjects: isMyProjects, factory: factory,
                                                 router: BaseRouter(navigationController: navVC))
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    func prepareChatListView(navVC: UINavigationController) {
        let coordinator = ChatListCoordinator(factory: factory,
                                              router: BaseRouter(navigationController: navVC))
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    func prepareProfileView(navVC: UINavigationController) {
        let coordinator = ProfileCoordinator(profile: CurrentUser.profile,
                                             factory: factory,
                                             router: BaseRouter(navigationController: navVC))
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
}
