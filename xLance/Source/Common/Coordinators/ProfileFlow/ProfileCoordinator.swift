//
//  ProfileCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProfileCoordinatorDelegate: class {
    func userPerformedLogout()
}

protocol ProfileCoordinatorFactory: AuthCoordinatorFactory {
    func makeProfileView(profile: ProfileModel?, router: Routable, coordinator: ProfileCoordinatable) -> BaseViewProtocol
}

class ProfileCoordinator: BaseCoordinator {
    
    weak var delegate: ProfileCoordinatorDelegate?
    var factory: ProfileCoordinatorFactory
    
    private var profile: ProfileModel?
    
    init(profile: ProfileModel?, factory: ProfileCoordinatorFactory, router: Routable) {
        self.profile = profile
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showProfile()
    }
    
}

extension ProfileCoordinator: ProfileCoordinatable {
    
    func changePasswordHandler() {
        showChangePassword()
    }
    
    func userPerformedLogout() {
        delegate?.userPerformedLogout()
    }
    
}

extension ProfileCoordinator: AuthCoordinatorDelegate {
    
    func userPerformedAuthentication(isAnonymous: Bool, coordinator: BaseCoordinator) {}
    
    func userPerformedChangePassword(coordinator: BaseCoordinator) {
        removeChildCoordinator(coordinator)
        router.pop()
    }
    
}

fileprivate extension ProfileCoordinator {
    
    func showProfile() {
        let view = factory.makeProfileView(profile: profile, router: router, coordinator: self)
        if (router.rootModule is ProfileViewProtocol) == false {
            router.push(view)
        }
    }
    
    func showChangePassword() {
        let coordinator = AuthCoordinator(startScene: .changePassword,
                                          factory: factory,
                                          router: router)
        coordinator.delegate = self
        addChildCoordinator(coordinator)
        coordinator.start()
    }
    
}
