//
//  SuggestionCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol SuggestionEditCoordinatorDelegate: BaseCoordinatableDelegate {
    func sendProposalHandler(_ suggestion: SuggestionModel, coordinator: BaseCoordinator)
}

protocol SuggestionEditCoordinatorFactory {
    func makeSuggestionEditView(suggestion: SuggestionModel, coordinator: SuggestionEditCoordinatable) -> BaseViewProtocol
    func makeMilestoneEditView(milestone: MilestoneModel, project: ProjectModel, coordinator: MilestoneEditCoordinatable) -> BaseViewProtocol
    func makeTaskEditView(task: TaskModel, project: ProjectModel, coordinator: TaskEditCoordinatable) -> BaseViewProtocol
    func makeMilestoneListView(milestones: [MilestoneModel], selectedMilestoneId: Int?, coordinator: MilestoneListCoordinatable) -> BaseViewProtocol
}

class SuggestionEditCoordinator: BaseCoordinator {
    
    weak var delegate: SuggestionEditCoordinatorDelegate?
    var factory: SuggestionEditCoordinatorFactory
    
    private var suggestion: SuggestionModel
    
    init(suggestion: SuggestionModel, factory: SuggestionEditCoordinatorFactory, router: Routable) {
        self.suggestion = suggestion
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showSuggestionEdit()
    }
    
    // MARK: SuggestionEditCoordinatable
    
    var onResetToDefault: CompletionSuggestionBlock?
    
    func sendProposalHandler(_ suggestion: SuggestionModel) {
        delegate?.sendProposalHandler(suggestion, coordinator: self)
    }
    
    func resetToDefaultHandler() {
        onResetToDefault?(suggestion)
    }
    
    func addTaskHandler(_ task: TaskModel) {
        showTask(task, project: suggestion.project)
    }
    
    func editTaskHandler(_ task: TaskModel) {
        showTask(task, project: suggestion.project)
    }
    
    func addMilestoneHandler(_ milestone: MilestoneModel) {
        showMilestone(milestone, project: suggestion.project)
    }
    
    func editMilestoneHandler(_ milestone: MilestoneModel) {
        showMilestone(milestone, project: suggestion.project)
    }
    
    // MARK: MilestoneEditCoordinatable & TaskEditCoordinatable
    
    var onUpdateProject: CompletionProjectBlock?
    var onUpdateMilestone: CompletionMilestoneBlock?
    
    func didUpdateProject(_ project: ProjectModel) {
        onUpdateProject?(project)
        suggestion.project = project
        router.pop()
    }
    
    func selectMilestoneHandler(milestones: [MilestoneModel], selectedMilestoneId: Int?) {
        showMilestoneList(milestones, selectedMilestoneId: selectedMilestoneId)
    }
    
    // MARK: MilestoneListCoordinatable
    
    func didSelectMilestone(_ milestone: MilestoneModel) {
        onUpdateMilestone?(milestone)
        router.pop()
    }
    
}

extension SuggestionEditCoordinator: SuggestionEditCoordinatable {}
extension SuggestionEditCoordinator: MilestoneEditCoordinatable {}
extension SuggestionEditCoordinator: TaskEditCoordinatable {}
extension SuggestionEditCoordinator: MilestoneListCoordinatable {}

fileprivate extension SuggestionEditCoordinator {
    
    func showSuggestionEdit() {
        let view = factory.makeSuggestionEditView(suggestion: suggestion, coordinator: self)
        view.onBack = { [unowned self] in
            self.delegate?.finish(self)
        }
        router.push(view)
    }
    
    func showMilestone(_ milestone: MilestoneModel, project: ProjectModel) {
        let view = factory.makeMilestoneEditView(milestone: milestone, project: project, coordinator: self)
        router.push(view)
    }
    
    func showTask(_ task: TaskModel, project: ProjectModel) {
        let view = factory.makeTaskEditView(task: task, project: project, coordinator: self)
        router.push(view)
    }
    
    func showMilestoneList(_ milestones: [MilestoneModel], selectedMilestoneId: Int?) {
        let view = factory.makeMilestoneListView(milestones: milestones, selectedMilestoneId: selectedMilestoneId, coordinator: self)
        router.push(view)
    }
    
}
