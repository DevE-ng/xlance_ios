//
//  SuggestionCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol SuggestionDetailsCoordinatorDelegate: BaseCoordinatableDelegate {
    func acceptNewTermsHandler(_ project: ProjectModel, coordinator: BaseCoordinator)
    func sendProposalHandler(_ suggestion: SuggestionModel, coordinator: BaseCoordinator)
    func declineHandler(_ project: ProjectModel, coordinator: BaseCoordinator)
}

protocol SuggestionDetailsCoordinatorFactory: SuggestionEditCoordinatorFactory {
    func makeSuggestionDetailsView(suggestion: SuggestionModel, coordinator: SuggestionDetailsCoordinatable) -> BaseViewProtocol
}

class SuggestionDetailsCoordinator: BaseCoordinator {
    
    weak var delegate: SuggestionDetailsCoordinatorDelegate?
    var factory: SuggestionDetailsCoordinatorFactory
    
    private var suggestion: SuggestionModel
    
    init(suggestion: SuggestionModel, factory: SuggestionDetailsCoordinatorFactory, router: Routable) {
        self.suggestion = suggestion
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showSuggestionDetails()
    }
    
}

extension SuggestionDetailsCoordinator: SuggestionDetailsCoordinatable {
    
    func acceptNewTermsHandler(_ project: ProjectModel) {
        delegate?.acceptNewTermsHandler(project, coordinator: self)
    }
    
    func editOfferHandler(_ suggestion: SuggestionModel) {
        showSuggestionEdit(suggestion)
    }
    
    func declineHandler(_ project: ProjectModel) {
        delegate?.declineHandler(project, coordinator: self)
    }
    
}

extension SuggestionDetailsCoordinator: SuggestionEditCoordinatorDelegate {
    
    func sendProposalHandler(_ suggestion: SuggestionModel, coordinator: BaseCoordinator) {
        delegate?.sendProposalHandler(suggestion, coordinator: self)
        removeChildCoordinator(coordinator)
    }
    
}

fileprivate extension SuggestionDetailsCoordinator {
    
    func showSuggestionDetails() {
        let view = factory.makeSuggestionDetailsView(suggestion: suggestion, coordinator: self)
        view.onBack = { [unowned self] in
            self.delegate?.finish(self)
        }
        router.push(view)
    }
    
    func showSuggestionEdit(_ suggestion: SuggestionModel) {
        let coordinator = SuggestionEditCoordinator(suggestion: suggestion,
                                                    factory: factory,
                                                    router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
}
