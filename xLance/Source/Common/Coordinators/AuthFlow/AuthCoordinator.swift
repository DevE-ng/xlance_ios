//
//  AuthCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol AuthCoordinatorDelegate: BaseCoordinatableDelegate {
    func userPerformedAuthentication(isAnonymous: Bool, coordinator: BaseCoordinator)
    func userPerformedChangePassword(coordinator: BaseCoordinator)
}

protocol AuthCoordinatorFactory {
    func makeAuthView(startScene: AuthScene, coordinator: AuthCoordinatable) -> BaseViewProtocol
}

class AuthCoordinator: BaseCoordinator {
    
    weak var delegate: AuthCoordinatorDelegate?
    var factory: AuthCoordinatorFactory
    private let startScene: AuthScene
    
    init(startScene: AuthScene, factory: AuthCoordinatorFactory, router: Routable) {
        self.startScene = startScene
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showAuth()
    }
    
}

// MARK: - AuthCoordinatable
extension AuthCoordinator: AuthCoordinatable {
    
    func userPerformedAuthentication() {
        userPerformedAuthentication(isAnonymous: false)
    }
    
    func userPerformedAuthentication(isAnonymous: Bool) {
        delegate?.userPerformedAuthentication(isAnonymous: isAnonymous, coordinator: self)
    }
    
    func userPerformedChangePassword() {
        delegate?.userPerformedChangePassword(coordinator: self)
    }
    
}

// MARK: - Private
fileprivate extension AuthCoordinator {
    
    func showAuth() {
        let view = factory.makeAuthView(startScene: startScene, coordinator: self)
        view.onBack = { [unowned self] in
            self.delegate?.finish(self)
        }
        if startScene == .login {
            router.setRoot(view)
        } else {
            router.push(view)
        }
    }
    
}
