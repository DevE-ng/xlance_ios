//
//  ChatDetailsCoordinator.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ChatDetailsCoordinatorDelegate: class {}

protocol ChatDetailsCoordinatorFactory {
    func makeChatDetailsView(project: ProjectModel, coordinator: ChatDetailsCoordinatable) -> BaseViewProtocol
    func makeCallView(user: ProfileModel, coordinator: CallCoordinatable) -> BaseViewProtocol
}

class ChatDetailsCoordinator: BaseCoordinator {
    
    weak var delegate: ChatDetailsCoordinatorDelegate?
    var factory: ChatDetailsCoordinatorFactory
    
    private var project: ProjectModel
    
    init(project: ProjectModel, factory: ChatDetailsCoordinatorFactory, router: Routable) {
        self.project = project
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showChatDetails()
    }
    
}

extension ChatDetailsCoordinator: ChatDetailsCoordinatable {
    func callHandler(user: ProfileModel) {
        showCall(user: user)
    }
}

extension ChatDetailsCoordinator: CallCoordinatable {
    func didFinishCall() {
        router.dismiss()
    }
}

fileprivate extension ChatDetailsCoordinator {
    
    func showChatDetails() {
        let view = factory.makeChatDetailsView(project: project, coordinator: self)
        router.push(view)
    }
    
    func showCall(user: ProfileModel) {
        let view = factory.makeCallView(user: user, coordinator: self)
        router.present(view)
    }
    
}

