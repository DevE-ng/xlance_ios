//
//  ChatListCoordinator.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ChatListCoordinatorDelegate: class {
}

protocol ChatListCoordinatorFactory: ChatDetailsCoordinatorFactory {
    func makeChatListView(coordinator: ChatListCoordinatable, router: Routable) -> BaseViewProtocol
}

class ChatListCoordinator: BaseCoordinator {
    
    weak var delegate: ChatListCoordinatorDelegate?
    var factory: ChatListCoordinatorFactory
    
    init(factory: ChatListCoordinatorFactory, router: Routable) {
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showChatList()
    }
    
}

// MARK: - ChatListCoordinatable
extension ChatListCoordinator: ChatListCoordinatable {
    func didSelectProject(_ project: ProjectModel) {
        showChatDetails(project: project)
    }
}

// MARK: - ChatDetailsCoordinatorDelegate
extension ChatListCoordinator: ChatDetailsCoordinatorDelegate {
    
}

fileprivate extension ChatListCoordinator {
    
    func showChatList() {
        let view = factory.makeChatListView(coordinator: self, router: router)
        if (router.rootModule is ChatListViewProtocol) == false {
            router.push(view)
        }
    }
    
    func showChatDetails(project: ProjectModel) {
        let coordinator = ChatDetailsCoordinator(project: project, factory: factory, router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
}
