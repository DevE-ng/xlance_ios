//
//  CallCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 24/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol CallCoordinatorDelegate: class {
    func didFinishCall(coordinator: BaseCoordinator)
}

protocol CallCoordinatorFactory {
    func makeCallView(user: ProfileModel, coordinator: CallCoordinatable) -> BaseViewProtocol
}

class CallCoordinator: BaseCoordinator {
    
    weak var delegate: CallCoordinatorDelegate?
    var factory: CallCoordinatorFactory
    
    private var user: ProfileModel
    
    init(user: ProfileModel, factory: CallCoordinatorFactory, router: Routable) {
        self.user = user
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showCall()
    }
    
}

extension CallCoordinator: CallCoordinatable {
    func didFinishCall() {
        router.dismiss()
        delegate?.didFinishCall(coordinator: self)
    }
}

fileprivate extension CallCoordinator {
    
    func showCall() {
        let view = factory.makeCallView(user: user, coordinator: self)
        router.present(view)
    }
    
}
