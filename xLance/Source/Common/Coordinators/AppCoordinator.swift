//
//  AppCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol AppCoordinatorFactory: MainCoordinatorFactory {
    func makeLaunchScreenView(coordinator: LaunchScreenCoordinatable) -> BaseViewProtocol
}

class AppCoordinator: BaseCoordinator {
    
    var factory: AppCoordinatorFactory
    
    init(factory: AppCoordinatorFactory, router: Routable) {
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showLaunchScreen()
    }
    
}

// MARK: - LaunchScreenCoordinatable
extension AppCoordinator: LaunchScreenCoordinatable {
    
    func userIsAuthenticated() {
        perform(#selector(showMainSelector), with: nil, afterDelay: 0)
        perform(#selector(showAuthSelector), with: nil, afterDelay: 1)
    }
    
    func userIsAnonymous() {
        perform(#selector(showMainAnonymousSelector), with: nil, afterDelay: 0)
        perform(#selector(showAuthSelector), with: nil, afterDelay: 1)
    }
    
    func userNeedsToAuthenticate() {
        showAuth()
    }
    
}


// MARK: - AuthCoordinatorDelegate
extension AppCoordinator: AuthCoordinatorDelegate {
    func userPerformedChangePassword(coordinator: BaseCoordinator) {}
    
    func userPerformedAuthentication(isAnonymous: Bool, coordinator: BaseCoordinator) {
        showMain(isAnonymous: isAnonymous)
    }
}

// MARK: - MainCoordinatorDelegate
extension AppCoordinator: MainCoordinatorDelegate {
    func userPerformedLogout(coordinator: BaseCoordinator) {
        removeChildCoordinator(coordinator)
    }
}

// MARK: - Private
fileprivate extension AppCoordinator {
    
    func showLaunchScreen() {
        let view = factory.makeLaunchScreenView(coordinator: self)
        router.push(view, animated: false)
    }
    
    func showAuth() {
        let coordinator = AuthCoordinator(startScene: .login,
                                          factory: factory,
                                          router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    func showMain(isAnonymous: Bool = false) {
        let coordinator = MainCoordinator(isAnonymous: isAnonymous,
                                          factory: factory,
                                          router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    @objc func showMainAnonymousSelector() {
        showMain(isAnonymous: true)
    }
    
    @objc func showMainSelector() {
        showMain()
    }
    
    @objc func showAuthSelector() {
        showAuth()
    }
    
}
