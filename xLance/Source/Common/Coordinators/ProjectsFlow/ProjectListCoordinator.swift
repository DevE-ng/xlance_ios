//
//  ProjectsCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProjectListCoordinatorDelegate: class {
    func didUpdateProject(_ project: ProjectModel)
}

protocol ProjectListCoordinatorFactory: ProjectDetailsCoordinatorFactory {
    func makeProjectListView(router: Routable, coordinator: ProjectListCoordinatable) -> BaseViewProtocol
    func makeMyProjectListView(router: Routable, coordinator: ProjectListCoordinatable) -> BaseViewProtocol
}

protocol ProjectCoordinatorUpdating {
    func didUpdateProject(_ project: ProjectModel)
}

class ProjectListCoordinator: BaseCoordinator, ProjectCoordinatorUpdating {
    
    weak var delegate: ProjectListCoordinatorDelegate?
    var factory: ProjectListCoordinatorFactory
    
    private var isMyProjects: Bool
    
    init(isMyProjects: Bool, factory: ProjectListCoordinatorFactory, router: Routable) {
        self.isMyProjects = isMyProjects
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        if isMyProjects {
            showMyProjectList()
        } else {
            showProjectList()
        }
    }
    
    // MARK: - ProjectCoordinatorUpdating
    
    func didUpdateProject(_ project: ProjectModel) {
        
        if isMyProjects && project.status != .accepted {
            childCoordinators.removeAll()
            perform(#selector(popToRootSelector), with: nil, afterDelay: 0)
        }
        onUpdateProject?(project)
    }

    // MARK: - ProjectListCoordinatable
    
    var onUpdateProject: ((ProjectModel) -> Void)?
    
    func didSelectProject(_ project: ProjectModel) {
        showProjectDetails(project)
    }
    
    // MARK: - MyProjectListCoordinatable
    
    
    
    // MARK: - ProjectDetailsCoordinatorDelegate
    
    func acceptNewTermsHandler(_ project: ProjectModel) {
        delegate?.didUpdateProject(project)
    }
    
    func sendProposalHandler(_ project: ProjectModel) {
        delegate?.didUpdateProject(project)
    }
    
    func declineHandler(_ project: ProjectModel) {
        delegate?.didUpdateProject(project)
    }
    
}

extension ProjectListCoordinator: ProjectListCoordinatable {}
extension ProjectListCoordinator: ProjectDetailsCoordinatorDelegate {}

fileprivate extension ProjectListCoordinator {
    
    func showProjectList() {
        let view = factory.makeProjectListView(router: router, coordinator: self)
        if (router.rootModule is ProjectListViewProtocol) == false {
            router.push(view)
        }
    }
    
    func showMyProjectList() {
        let view = factory.makeMyProjectListView(router: router, coordinator: self)
        if (router.rootModule is MyProjectListViewProtocol) == false {
            router.push(view)
        }
    }
    
    func showProjectDetails(_ project: ProjectModel) {
        let coordinator = ProjectDetailsCoordinator(project: project, factory: factory, router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    @objc func popToRootSelector() {
        router.popToRoot(animated: false)
    }
    
}
