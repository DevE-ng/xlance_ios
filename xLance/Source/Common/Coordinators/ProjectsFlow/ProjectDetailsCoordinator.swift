//
//  ProjectCoordinator.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ProjectDetailsCoordinatorDelegate: BaseCoordinatableDelegate {
    func acceptNewTermsHandler(_ project: ProjectModel)
    func sendProposalHandler(_ project: ProjectModel)
    func declineHandler(_ project: ProjectModel)
}

protocol ProjectDetailsCoordinatorFactory: SuggestionDetailsCoordinatorFactory {
    func makeProjectDetailsView(project: ProjectModel, coordinator: ProjectDetailsCoordinatable) -> BaseViewProtocol
}

class ProjectDetailsCoordinator: BaseCoordinator {
    
    weak var delegate: ProjectDetailsCoordinatorDelegate?
    var factory: ProjectDetailsCoordinatorFactory
    
    private var project: ProjectModel
    
    init(project: ProjectModel, factory: ProjectDetailsCoordinatorFactory, router: Routable) {
        self.project = project
        self.factory = factory
        super.init(router: router)
    }
    
    override func start() {
        showProjectDetails()
    }
    
    // MARK: - ProjectDetailsCoordinatable
    
    var onUpdateProject: CompletionProjectBlock?
    
    func sendProposalHandler(_ project: ProjectModel) {
        delegate?.sendProposalHandler(project)
    }
    
    func editTermsHandler(_ suggestion: SuggestionModel) {
        showSuggestionEdit(suggestion)
    }
    
    func viewConditionHandler(_ suggestion: SuggestionModel) {
        showSuggestionDetails(suggestion)
    }
    
    func declineHandler(_ project: ProjectModel) {
        delegate?.declineHandler(project)
    }
    
    // MARK: - SuggestionDetailsCoordinatorDelegate
    
    func acceptNewTermsHandler(_ project: ProjectModel, coordinator: BaseCoordinator) {
        delegate?.acceptNewTermsHandler(project)
        removeChildCoordinator(coordinator)
        router.pop()
    }
    
    func declineHandler(_ project: ProjectModel, coordinator: BaseCoordinator) {
        delegate?.declineHandler(project)
        removeChildCoordinator(coordinator)
        router.pop()
    }
    
    // MARK: - SuggestionDetailsCoordinatorDelegate
    
    func sendProposalHandler(_ suggestion: SuggestionModel, coordinator: BaseCoordinator) {
        project.suggestions.removeAll()
        project.suggestions.append(suggestion)
        
        onUpdateProject?(project)
        delegate?.sendProposalHandler(project)
        
        removeChildCoordinator(coordinator)
        
        if let projectDetailsView = router.modules.filter({ $0 is ProjectDetailsViewProtocol }).last {
            router.popTo(projectDetailsView)
        }
    }
    
}


extension ProjectDetailsCoordinator: ProjectDetailsCoordinatable {}
extension ProjectDetailsCoordinator: SuggestionDetailsCoordinatorDelegate {}
extension ProjectDetailsCoordinator: SuggestionEditCoordinatorDelegate {}

fileprivate extension ProjectDetailsCoordinator {
    
    func showProjectDetails() {
        let view = factory.makeProjectDetailsView(project: project, coordinator: self)
        view.onBack = { [unowned self] in
            self.delegate?.finish(self)
        }
        router.push(view)
    }
    
    func showSuggestionDetails(_ suggestion: SuggestionModel) {
        let coordinator = SuggestionDetailsCoordinator(suggestion: suggestion,
                                                       factory: factory,
                                                       router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
    func showSuggestionEdit(_ suggestion: SuggestionModel) {
        let coordinator = SuggestionEditCoordinator(suggestion: suggestion,
                                                    factory: factory,
                                                    router: router)
        addChildCoordinator(coordinator)
        coordinator.delegate = self
        coordinator.start()
    }
    
}

