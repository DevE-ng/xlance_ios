//
//  NotificationCenterWrapper.swift
//  xlance
//
//  Created by Anton Lobanov on 23/07/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let login                               = Notification.Name("login")
    static let logout                              = Notification.Name("logout")
}

class NotificationCenterWrapper {
    
    static func subscribeLogin(completionHandler: @escaping CompletionBlock) -> NotificationToken {
        return notificationCenter.observe(name: .login, object: nil, queue: .main) { notification in
            completionHandler()
        }
    }
    
    static func postLogin() {
        notificationCenter.post(name: .login, object: nil, userInfo: nil)
    }
    
    static func subscribeLogout(completionHandler: @escaping CompletionBlock) -> NotificationToken {
        return notificationCenter.observe(name: .logout, object: nil, queue: .main) { notification in
            completionHandler()
        }
    }
    
    static func postLogout() {
        notificationCenter.post(name: .logout, object: nil, userInfo: nil)
    }
    
}

// MARK: - Static methods
extension NotificationCenterWrapper {
    private static var notificationCenter: NotificationCenter {
        return NotificationCenter.default
    }
    
    static func add(_ observer: Any, selector: Selector, name: Notification.Name, object: Any? = nil) {
        notificationCenter.addObserver(observer, selector: selector, name: name, object: object)
    }
    
    static func remove(_ observer: Any, name: Notification.Name, object: Any? = nil) {
        notificationCenter.removeObserver(observer, name: name, object: object)
    }
    
    static func add(_ observer: Any, notifications: [Notification.Name: Selector]) {
        notifications.forEach { (name, selector) in
            add(observer, selector: selector, name: name)
        }
    }
    
    static func remove(_ observer: Any, notifications: [Notification.Name]) {
        notifications.forEach { name in
            remove(observer, name: name)
        }
    }
}

/// Convenience wrapper for addObserver(forName:object:queue:using:)
/// that returns our custom NotificationToken.
extension NotificationCenter {
    func observe(name: NSNotification.Name?, object obj: Any?,
                 queue: OperationQueue?, using block: @escaping (Notification) -> ())
        -> NotificationToken
    {
        let token = addObserver(forName: name, object: obj, queue: queue, using: block)
        return NotificationToken(notificationCenter: self, token: token)
    }
}

/// Wraps the observer token received from
/// NotificationCenter.addObserver(forName:object:queue:using:)
/// and unregisters it in deinit.
final class NotificationToken: NSObject {
    let notificationCenter: NotificationCenter
    let token: Any
    
    init(notificationCenter: NotificationCenter = .default, token: Any) {
        self.notificationCenter = notificationCenter
        self.token = token
    }
    
    deinit {
        notificationCenter.removeObserver(token)
    }
    
    func instantiate() {}
    
}
