//
//  Constatns.swift
//
//  Created by alobanov11 on 28/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import UIKit

// MARK:- Typealiases
typealias CompletionBlock               = ()                    -> Void
typealias CompletionAuthBlock        = (AuthModel)                    -> Void
typealias CompletionProjectBlock        = (ProjectModel)                    -> Void
typealias CompletionSuggestionBlock     = (SuggestionModel)     -> Void
typealias CompletionMilestoneBlock      = (MilestoneModel)      -> Void
typealias CompletionTaskBlock           = (TaskModel)           -> Void

// MARK:- Storyboards enum
enum Storyboards: String {
    
    case launchScreen           = "LaunchScreen"
    case auth                   = "Auth"
    case main                   = "Main"
    case projectList            = "ProjectList"
    case projectDetails         = "ProjectDetails"
    case myProjectList          = "MyProjectList"
    case profile                = "Profile"
    
    case suggestionDetails      = "SuggestionDetails"
    case suggestionEdit         = "SuggestionEdit"
    case taskEdit               = "TaskEdit"
    case milestoneList          = "MilestoneList"
    case milestoneEdit          = "MilestoneEdit"
    
    case chatList               = "ChatList"
    case chatDetails            = "ChatDetails"
    case call                   = "Call"
    
}

// MARK:- PersistantKeys enum
enum ModifyStatus {
    
    case none
    
    case created
    case edited
    case deleted
    
    case moved
    
    case increased
    case decreased
    
}

// MARK:- Result
enum Result<T> {
    
    var value: T? {
        switch self {
        case .Success(let value):
            return value
        default: return nil
        }
    }
    
    case Success(T)
    case Error(String)
}

//
struct CONSTANTS {
    
    static let keychain_service       = "ru.e-ngineers.xlance"
    
    static let turn_url               = "turn:80.79.246.114:3478"
    static let turn_name              = "xlance"
    static let turn_password          = "xlance"
    
    static let xlance                           = "xlance.e-ngineers.ru"
    static let xlance_url                       = "\(CONSTANTS.xlance):9001"
    static let xlance_full_url_with_port        = "https://\(CONSTANTS.xlance_url)"
    static let xlance_full_url_without_port     = "https://\(CONSTANTS.xlance)"
    static let xlance_socket_url                = "wss://\(CONSTANTS.xlance_url)/rtc?token="
    static let openvidu_socket_url              = "wss://openvidu.e-ngineers.ru:4443/openvidu"
    static let xlance_stomp_url                 = "wss://\(CONSTANTS.xlance):15678/ws"
    
    static let contentLayerName       = "contentLayerName"
    
}
