//
//  Sessions.swift
//
//  Created by alobanov11 on 28/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import Foundation
import KeychainAccess

struct CurrentUser {
    
    fileprivate static let keychain = Keychain(service: CONSTANTS.keychain_service)
    fileprivate static let UserDefaultsStandart = UserDefaults.standard
    static var profile: ProfileModel?

}


// MARK: - Fields
extension CurrentUser {
    
    static var isLogged: Bool {
        return self.profile != nil || isAnonymous ? true : false
    }
    
    static var isAnonymous: Bool {
        get {
            return UserDefaultsStandart.bool(forKey: PersistantKeys.isAnonymous.rawValue)
        }
        set {
            UserDefaultsStandart.set(newValue, forKey: PersistantKeys.isAnonymous.rawValue)
            UserDefaultsStandart.synchronize()
        }
    }
    
    static var username: String? {
        get {
            return stringFromKeychain(key: .userName)
        }
        set {
            setOrRemoveStringToKeychain(newValue, key: .userName)
        }
    }
    
    static var password: String? {
        get {
            return stringFromKeychain(key: .userPassword)
        }
        set {
            setOrRemoveStringToKeychain(newValue, key: .userPassword)
        }
    }
    
    static var token: String? {
        get {
            return stringFromKeychain(key: .userToken)
        }
        set {
            setOrRemoveStringToKeychain(newValue, key: .userToken)
        }
    }
    
    static var isEmployer: Bool {
        return CurrentUser.profile?.role == .employer
    }
    
    static var isFreelancer: Bool {
        return CurrentUser.profile?.role == .freelancer
    }
    
    static var roleSuffix: String {
        return CurrentUser.profile?.role.rawValue ?? ""
    }
    
}

// MARK: - Public
extension CurrentUser {
    static func loginAnonymous() {
        isAnonymous = true
        profile = nil
        username = nil
        password = nil
        token = nil
    }
    static func login(_ data: LoginResponse, name: String, pass: String) {
        isAnonymous = false
        profile = data.profile
        username = name
        password = pass
        token = data.token
        NotificationCenterWrapper.postLogin()
    }
    static func logout() {
        isAnonymous = false
        profile = nil
        username = nil
        password = nil
        token = nil
        NotificationCenterWrapper.postLogout()
    }
}


// MARK: - Private
fileprivate extension CurrentUser {
    
    enum PersistantKeys: String {
        
        case isAnonymous                  = "cUserAnonymous"
        case userName                     = "cUserName"
        case userPassword                 = "cUserPassword"
        case userToken                    = "cUserToken"
        
    }
    
    static func stringFromKeychain(key: PersistantKeys) -> String? {
        return try? keychain.get(key.rawValue)
    }
    
    static func setOrRemoveStringToKeychain(_ value: String?, key: PersistantKeys) {
        if let value = value {
            try? keychain.set(value, key: key.rawValue)
        } else {
            try? keychain.remove(key.rawValue)
        }
    }
    
}
