//
//  SuggestionModel.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct SuggestionModel {
    
    var id: Int
    var fakeId: Int?
    var project: ProjectModel!
    var projectId: Int?
    var currency: CurrencyModel
    var conversation: ConversationModel
    var status: SuggestionStatus
    var startDate: Date
    var tillDate: Date
    var budget: Decimal
    var packageChange: Int
    var answers: String?
    var paymentMethod: PaymentMethod
    var hoursPerWeek: Int
    var paymentPeriod: Int
    var freelancer: ProfileModel
    var histories: [SuggestionHistoryModel] = []
    
    init(model: ProjectModel, freelancer: ProfileModel) {
        self.id = -UUID().hashValue
        self.fakeId = self.id
        self.project = model
        self.projectId = model.id
        self.currency = model.currency
        self.conversation = ConversationModel()
        self.status = .proposal
        self.startDate = model.startDate
        self.tillDate = model.tillDate
        self.budget = model.budget
        self.packageChange = -1
        self.paymentMethod = model.paymentMethod
        self.hoursPerWeek = model.hoursPerWeek
        self.paymentPeriod = model.paymentPeriod
        self.freelancer = freelancer
        
        var addedHistory = SuggestionHistoryModel(model: self)
        addedHistory.status = .changed
        histories.append(addedHistory)
    }
    
}

extension SuggestionModel: ImmutableMappable {
    init(map: Map) throws {
        id                  = try map.value("id")
        project             = try? map.value("project")
        projectId           = try? map.value("projectId")
        currency            = try map.value("currency")
        conversation        = try map.value("conversation")
        status              = try map.value("type")
        startDate           = Date.iso8601Formatter.date(from: try map.value("startDate"))!
        tillDate            = Date.iso8601Formatter.date(from: try map.value("tillDate"))!
        budget              = try map.value("budget", using: NSDecimalNumberTransform()) as Decimal
        packageChange       = try map.value("packageChange")
        answers             = try map.value("answers")
        paymentMethod       = try map.value("paymentMethod")
        hoursPerWeek        = try map.value("hoursPerWeek")
        paymentPeriod       = try map.value("paymentPeriod")
        histories           = try! map.value("histories")
        freelancer          = try map.value("freelancer")
    }
    func mapping(map: Map) {
        id                  >>> map["id"]
        if id < 0 {
        id                  >>> map["fakeId"]
        }
        project             >>> map["project"]
        projectId           >>> map["projectId"]
        currency            >>> map["currency"]
        conversation        >>> map["conversation"]
        status              >>> map["type"]
        startDate.iso8601   >>> map["startDate"]
        tillDate.iso8601    >>> map["tillDate"]
        budget              >>> map["budget"]
        packageChange       >>> map["packageChange"]
        answers             >>> map["answers"]
        paymentMethod       >>> map["paymentMethod"]
        hoursPerWeek        >>> map["hoursPerWeek"]
        paymentPeriod       >>> map["paymentPeriod"]
        histories           >>> map["histories"]
        freelancer          >>> map["freelancer"]
    }
}

extension SuggestionModel {
    
    /// WARNING: force
    var actualHistory: SuggestionHistoryModel {
        get {
            let index = histories.lastIndex()!
            return histories[index]
        }
        set {
            let index = histories.lastIndex()!
            histories[index] = newValue
        }
    }
    
    /// WARNING: force
    var firstHistory: SuggestionHistoryModel {
        get {
            return histories[0]
        }
        set {
            histories[0] = newValue
        }
    }
    
    /// WARNING: force
    var secondHistory: SuggestionHistoryModel {
        get {
            return histories[1]
        }
        set {
            histories[1] = newValue
        }
    }
    
}

/// Clear history with non actual packageVersion
extension SuggestionModel {
    
    mutating func prepareForView() {
        project.prepareForView(packageChange: packageChange)
        prepareSelfForView()
    }
    
    private mutating func prepareSelfForView() {
        if
            (histories.count == 2 && actualHistory.packageVersion == packageChange) ||
            (histories.count == 1) && actualHistory.status == .current { return }
        guard let indexOldHistory = histories.map({ $0.packageVersion }).enumerated().filter({ $0.element != packageChange }).first?.offset else { return }
        let _ = histories.remove(at: indexOldHistory)
    }
    
}

/// Prepare histories to edit
extension SuggestionModel {
    
    mutating func prepareForEdited() {
        project.prepareForEdited()
        prepareSelfForEdited()
    }
    
    private mutating func prepareSelfForEdited() {
        if histories.isEmpty {
            var addedHistory = SuggestionHistoryModel(model: self)
            addedHistory.status = .changed
            histories = [addedHistory, addedHistory]
            return
        }
        if histories.count == 2 {
            let _ = histories.remove(at: 0)
        }
        var currentHistory = createCurrentHistory()
        if actualHistory.id != 0 {
            actualHistory.status = .current
        } else {
            currentHistory.suggestionId = id
        }
        histories.append(currentHistory)
    }
    
    private func createCurrentHistory() -> SuggestionHistoryModel {
        return SuggestionHistoryModel(model: histories.last!)
    }
    
}

/// Clear current histories
extension SuggestionModel {
    
    mutating func prepareForSend() {
        let maxProjectPackageVersion = project.prepareForSend()
        let maxSuggestionPackageVersion = prepareSelfForSend()
        let maxPackageVersion = [maxProjectPackageVersion, maxSuggestionPackageVersion].max()!
        
        if packageChange == -1 {
            packageChange = 2
        } else if maxPackageVersion == 0 {
            packageChange += 1
        } else if maxPackageVersion > packageChange {
            packageChange = maxPackageVersion
        }
        
        updateActualHistoryVersion()
        provideSuggestionId()
        
        if project.suggestions.isEmpty {
            project.suggestions.append(self)
        }
    }
    
    private mutating func prepareSelfForSend() -> Int {
        if histories.count < 2 { return -1 }
        if !secondHistory.equalTo(firstHistory) && secondHistory.status == .current {
            secondHistory.status = .changed
        }
        histories.removeAll(where: { $0.status == .current })
        return histories.map({ $0.packageVersion }).max() ?? -1
    }
    
    private mutating func provideSuggestionId() {
        for (index, _) in project.milestones.enumerated() {
            for (indexHistory, _) in project.milestones[index].histories.enumerated() {
                project.milestones[index].histories[indexHistory].suggestionId = self.id
            }
            for (indexTask, _) in project.milestones[index].tasks.enumerated() {
                for (indexHistory, _) in project.milestones[index].tasks[indexTask].histories.enumerated() {
                    project.milestones[index].tasks[indexTask].histories[indexHistory].suggestionId = self.id
                }
            }
        }
        for (indexTask, _) in project.tasks.enumerated() {
            for (indexHistory, _) in project.tasks[indexTask].histories.enumerated() {
                project.tasks[indexTask].histories[indexHistory].suggestionId = self.id
            }
        }
    }
    
}

extension SuggestionModel {
    
    private mutating func updateActualHistoryVersion() {
        project.updateActualHistoryVersion(packageChange)
        updateSelfActualHistoryVersion()
    }
    
    private mutating func updateSelfActualHistoryVersion() {
        if histories.isEmpty { return }
        if id < 0 {
            if histories.count == 2 {
                actualHistory.packageVersion = 2
            } else {
                actualHistory.packageVersion = 0
            }
            packageChange = 1
        }
        else {
            actualHistory.packageVersion = packageChange
            packageChange -= 1
        }
    }
    
}
