//
//  HistoryStatus.swift
//  xlance
//
//  Created by alobanov11 on 15/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

enum ProjectStatus: String {
    case none
    case waitingForAccept = "COMMON.WAITING_FOR_ANSWERS"
    case waitingForAgreement = "COMMON.WAITING_FOR_AGREEMENT"
    case accepted = "COMMON.ACCEPTED"
    
    var localized: String {
        return self.rawValue.localized
    }
    
}

enum PaymentMethod: String {
    case fixed = "F"
    case hourly = "H"
    
    var localized: String {
        switch self {
        case .hourly: return "PROJECT.PAY_HOURLY".localized
        case .fixed: return "PROJECT.PAY_FIXED".localized
        }
    }
    
}

enum HistoryStatus: String {
    case current = "CUR"
    case added = "ADD"
    case deleted = "DEL"
    case changed = "CHA"
    case arc = "ARC"
}

enum SuggestionStatus: String {
    case offer = "O"
    case proposal = "P"
}

enum ProfileStatus: String {
    case online = "ONLINE"
    case offline = "OFFLINE"
}

enum MessageStatus: String {
    case rv = "RV"
    case pu = "PU"
    case dl = "DL"
    case sn = "SN"
}
