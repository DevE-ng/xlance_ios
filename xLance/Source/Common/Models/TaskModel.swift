//
//  TaskModel.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct TaskModel {
    
    var id: Int
    var index: Int = 0
    var name: String
    var description: String
    var duration: Int
    var milestoneId: Int?
    var projectId: Int
    var changeVersion: Int?
    var histories: [TaskHistoryModel] = []
    
    init(projectId: Int) {
        self.id = -UUID().hashValue
        self.index = -1
        self.name = "Created task"
        self.description = ""
        self.duration = 0
        self.milestoneId = nil
        self.projectId = projectId
        self.changeVersion = 0
        
        var addedHistory = TaskHistoryModel()
        addedHistory.status = .added
        histories.append(addedHistory)
    }
    
}

extension TaskModel: ImmutableMappable {
    init(map: Map) throws {
        id                  = try map.value("id")
        name                = try map.value("name")
        description         = (try? map.value("description")) ?? ""
        duration            = try map.value("duration")
        milestoneId         = try? map.value("milestoneId")
        projectId           = try map.value("projectId")
        changeVersion       = try? map.value("changeVersion")
        histories           = (try? map.value("histories")) ?? []
    }
    func mapping(map: Map) {
        id              >>> map["id"]
        name            >>> map["name"]
        description     >>> map["description"]
        duration        >>> map["duration"]
        milestoneId     >>> map["milestoneId"]
        projectId       >>> map["projectId"]
        changeVersion   >>> map["changeVersion"]
        histories       >>> map["histories"]
    }
}

extension TaskModel {
    
    var isAdded: Bool {
        return histories.filter({ $0.status == .added }).count > 0
    }
    
    var isDeleted: Bool {
        return histories.filter({ $0.status == .deleted }).count > 0
    }
    
    /// WARNING: force
    var actualHistory: TaskHistoryModel {
        get {
            let index = histories.lastIndex()!
            return histories[index]
        }
        set {
            let index = histories.lastIndex()!
            histories[index] = newValue
        }
    }
    
    /// WARNING: force
    var firstHistory: TaskHistoryModel {
        get {
            return histories[0]
        }
        set {
            histories[0] = newValue
        }
    }
    
    /// WARNING: force
    var secondHistory: TaskHistoryModel {
        get {
            return histories[1]
        }
        set {
            histories[1] = newValue
        }
    }
    
}

/// Clear history with non actual packageVersion
extension TaskModel {
    
    mutating func prepareForView(packageChange: Int) {
        prepareSelfForView(packageChange: packageChange)
    }
    
    private mutating func prepareSelfForView(packageChange: Int) {
        if
            (histories.count == 2 && actualHistory.packageVersion == packageChange) ||
            (histories.count == 1) && actualHistory.status == .current { return }
        guard let indexOldHistory = histories.map({ $0.packageVersion }).enumerated().filter({ $0.element != packageChange }).first?.offset else { return }
        let _ = histories.remove(at: indexOldHistory)
    }
    
}

/// Prepare histories to edit
extension TaskModel {
    
    mutating func makeDeleted() {
        actualHistory.status = .deleted
    }
    
    mutating func prepareForEdited() {
        prepareSelfForEdited()
    }
    
    private mutating func prepareSelfForEdited() {
        if histories.isEmpty {
            let currentHistory = TaskHistoryModel(model: self)
            histories = [currentHistory, currentHistory]
            return
        }
        if histories.count == 2 {
            let _ = histories.remove(at: 0)
        }
        let currentHistory = createCurrentHistory()
        histories.append(currentHistory)
    }
    
    private func createCurrentHistory() -> TaskHistoryModel {
        return TaskHistoryModel(model: histories.last!)
    }
    
}

/// Clear current histories
extension TaskModel {
    
    /// return max packageVersion or -1
    mutating func prepareForSend() -> Int {
        if histories.count < 2 { return -1 }
        if !secondHistory.equalTo(firstHistory) && secondHistory.status == .current {
            secondHistory.status = .changed
        }
        histories.removeAll(where: { $0.status == .current })
        return histories.map({ $0.packageVersion }).max() ?? -1
    }
    
}

extension TaskModel {
    
    mutating func updateActualHistoryVersion(_ packageVersion: Int) {
        if histories.count > 0 {
            actualHistory.packageVersion = packageVersion
        }
    }
    
}

extension TaskModel: Equatable {
    
    static func == (lhs: TaskModel, rhs: TaskModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}
