//
//  LoginResponse.swift
//  xlance
//
//  Created by alobanov11 on 18/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct LoginResponse {
    
    var token: String
    var profile: ProfileModel!
    var profileJson: String
    
}

extension LoginResponse: ImmutableMappable {
    init(map: Map) throws {
        token = try map.value("token")
        profileJson = try map.value("profile")
        profile = ProfileModel(jsonString: profileJson)
    }
}
