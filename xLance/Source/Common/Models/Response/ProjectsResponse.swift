//
//  ProjectsResponse.swift
//  xlance
//
//  Created by alobanov11 on 18/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProjectsResponse {
    
    var projects: [ProjectModel]
    
}

extension ProjectsResponse: ImmutableMappable {
    init(map: Map) throws {
        projects = try map.value("projects")
    }
}
