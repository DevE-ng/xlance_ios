//
//  PublishMessageResponse.swift
//  xlance
//
//  Created by alobanov11 on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct PublishMessageResponse {
    
    var id: Int = Int(Date().timestamp)
    var from: Int
    var fromEmail: String?
    var fromHash: Int
    var event: Event
    var to: [Conversation]
    
    init(from profile: ProfileModel, project: ProjectModel) {
        self.from = profile.id
        self.fromEmail = profile.email
        self.fromHash = profile.rabbitmqHash
        self.event = .init(nick: nil,
                           targetId: project.id,
                           date: Date().iso8601,
                           from: profile.id,
                           to: project.companion!.id,
                           type: "MESSAGE",
                           action: "SEND",
                           hierarchyReference: .init(jobId: project.masterJob.id,
                                                     projectId: project.id,
                                                     suggestionId: project.suggestion?.id,
                                                     conversationId: project.conversationId,
                                                     taskId: nil,
                                                     documentId: nil,
                                                     profileId: nil,
                                                     suggestionType: "O",
                                                     milestoneId: nil,
                                                     invoiceId: nil),
                           targetName: project.name)
        self.to = [.init(id: project.companion!.id, hash: project.companion!.rabbitmqHash)]
    }
    
    struct Conversation {
        var id: Int
        var hash: Int
    }
    
    struct Event {
        var nick: String?
        var targetId: Int
        var date: String
        var from: Int
        var to: Int
        var type: String = "MESSAGE"
        var action: String = "SEND"
        var hierarchyReference: HirerachyReference
        var targetName: String = "dev"
        
        struct HirerachyReference {
            var jobId: Int
            var projectId: Int
            var suggestionId: Int?
            var conversationId: Int?
            var taskId: String?
            var documentId: String?
            var profileId: String?
            var suggestionType: String = "O"
            var milestoneId: String?
            var invoiceId: Int?
        }
        
    }
    
}

extension PublishMessageResponse: ImmutableMappable {
    init(map: Map) throws {
        event = try map.value("event")
        from = try map.value("from")
        fromEmail = try map.value("fromEmail")
        fromHash = try map.value("fromHash")
        id = try map.value("id")
        to = try map.value("to")
    }
    func mapping(map: Map) {
        event.toJSONString() >>> map["event"]
        from.string >>> map["from"]
        fromEmail >>> map["fromEmail"]
        fromHash.string >>> map["fromHash"]
        id.string >>> map["id"]
        to.toJSONString() >>> map["to"]
    }
}

extension PublishMessageResponse.Conversation: ImmutableMappable {
    init(map: Map) throws {
        id = try map.value("id")
        hash = try map.value("hash")
    }
    func mapping(map: Map) {
        id >>> map["id"]
        hash >>> map["hash"]
    }
}

extension PublishMessageResponse.Event: ImmutableMappable {
    init(map: Map) throws {
        nick = try map.value("nick")
        targetId = try map.value("targetId")
        date = try map.value("date")
        from = try map.value("from")
        to = try map.value("to")
        hierarchyReference = try map.value("hierarchyReference")
    }
    func mapping(map: Map) {
        nick >>> map["nick"]
        targetId >>> map["targetId"]
        date >>> map["date"]
        from >>> map["from"]
        to >>> map["to"]
        type >>> map["type"]
        action >>> map["action"]
        targetName >>> map["targetName"]
        hierarchyReference >>> map["hierarchyReference"]
    }
}

extension PublishMessageResponse.Event.HirerachyReference: ImmutableMappable {
    init(map: Map) throws {
        jobId = try map.value("jobId")
        projectId = try map.value("projectId")
        suggestionId = try map.value("suggestionId")
        conversationId = try map.value("conversationId")
        taskId = try map.value("taskId")
        documentId = try map.value("documentId")
        profileId = try map.value("profileId")
        milestoneId = try map.value("milestoneId")
        invoiceId = try map.value("invoiceId")
    }
    func mapping(map: Map) {
        jobId >>> map["jobId"]
        projectId >>> map["projectId"]
        suggestionId >>> map["suggestionId"]
        conversationId >>> map["conversationId"]
        taskId >>> map["taskId"]
        documentId >>> map["documentId"]
        profileId >>> map["profileId"]
        suggestionType >>> map["suggestionType"]
        milestoneId >>> map["milestoneId"]
        invoiceId >>> map["invoiceId"]
    }
}
