//
//  RoomResponse.swift
//  xlance
//
//  Created by alobanov11 on 24/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

struct RoomResponse {
    
    let token: String
    let session: String
    
}
