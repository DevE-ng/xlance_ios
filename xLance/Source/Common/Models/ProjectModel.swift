//
//  ProjectModel.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProjectModel {

    // Required fields
    var id: Int
    var startDate: Date
    var tillDate: Date
    var budget: Decimal
    var name: String
    var hoursPerWeek: Int
    var paymentPeriod: Int
    var paymentMethod: PaymentMethod
    var currency: CurrencyModel
    var masterJob: MasterJobModel
    var milestones: [MilestoneModel]
    var tasks: [TaskModel]
    var suggestions: [SuggestionModel]
    var conversation: ConversationModel?
    var categories: [CategoryModel]
    var freelancer: ProfileModel?
    
    
    // Created fields
    var online: Bool = false
    var unreadMessagesCount: Int = 0
    
}

extension ProjectModel: ImmutableMappable {
    init(map: Map) throws {
        id                  = try map.value("id")
        startDate           = Date.iso8601Formatter.date(from: try map.value("startDate"))!
        tillDate            = Date.iso8601Formatter.date(from: try map.value("tillDate"))!
        budget              = try map.value("budget", using: NSDecimalNumberTransform()) as Decimal
        name                = try map.value("name")
        hoursPerWeek        = try map.value("hoursPerWeek")
        paymentPeriod       = try map.value("paymentPeriod")
        paymentMethod       = try map.value("paymentMethod")
        currency            = try map.value("currency")
        masterJob           = try map.value("masterJob")
        milestones          = try map.value("milestones")
        tasks               = try map.value("tasks")
        suggestions         = (try? map.value("suggestions")) ?? []
        conversation        = try? map.value("conversation")
        categories          = try map.value("categories")
        freelancer          = try? map.value("freelancer")
        for (index, _) in suggestions.enumerated() {
            suggestions[index].project = self // provide project
        }
        for (index, _) in milestones.enumerated() {
            milestones[index].index = index // provide index
            milestones[index].name = "Milestone #\(index + 1)" // provide index
        }
    }
    func mapping(map: Map) {
        id                              >>> map["id"]
        startDate.iso8601               >>> map["startDate"]
        tillDate.iso8601                >>> map["tillDate"]
        budget                          >>> map["budget"]
        name                            >>> map["name"]
        hoursPerWeek                    >>> map["hoursPerWeek"]
        paymentPeriod                   >>> map["paymentPeriod"]
        paymentMethod                   >>> map["paymentMethod"]
        currency.key                    >>> map["currency"]
        masterJob                       >>> map["masterJob"]
        milestones                      >>> map["milestones"]
        tasks                           >>> map["tasks"]
        []                              >>> map["suggestions"]
        categories                      >>> map["categories"]
    }
}

extension ProjectModel {
    
    var suggestion: SuggestionModel? {
        return suggestions.first
    }
    
    var status: ProjectStatus {
        switch true {
        case conversation != nil: return .accepted
        case suggestion?.status == .proposal: return .waitingForAgreement
        case suggestion?.status == .offer: return .waitingForAccept
        default: return .none
        }
    }
    
    var mainCategory: CategoryModel? {
        return categories.sorted(by: { $0 > $1 }).first
    }
    
    func milestoneNameById(_ id: Int?) -> String? {
        guard let id = id else { return nil }
        return milestones.filter({ $0.id == id }).first?.name
    }
    
    func isValidMilestone(_ milestone: MilestoneModel) -> Bool {
        return !(milestone.actualHistory.daysForReview < 3 || milestone.actualHistory.daysForReview > 7)
    }
    
    /// WARNING: force
    func isValidTask(_ task: TaskModel) -> Bool {
        if paymentMethod == .fixed && task.actualHistory.milestoneId == nil { return false }
        return !task.actualHistory.name.isEmpty &&
               task.actualHistory.duration > 0
    }
    
    var movedMilestoneTasks: [TaskModel] {
        let allTasks = milestones.map({ $0.tasks.map({ $0 }) }).flatMap({ $0 })
        return allTasks.filter({ $0.histories.count == 2 }).filter({ $0.firstHistory.milestoneId != $0.secondHistory.milestoneId })
    }
    
    mutating func updateOrAddMilestone(_ newMilestone: MilestoneModel) {
        if let index = milestones.indexForElement(newMilestone) {
            milestones[index] = newMilestone
        } else {
            milestones.append(newMilestone)
        }
    }
    
    /// WARNING: force
    mutating func updateOrAddTask(_ newTask: TaskModel) {
        if paymentMethod == .fixed {
            let allTasks = milestones.map({ $0.tasks.map({ $0 }) }).flatMap({ $0 })
            let oldTask = allTasks.filter({ $0.id == newTask.id }).first
            let indexOriginalMilestone = milestones.map({ $0.id }).enumerated().filter({ $0.element == oldTask?.actualHistory.milestoneId }).first?.offset
            let indexMovedMilestone = milestones.map({ $0.id }).enumerated().filter({ $0.element == newTask.actualHistory.milestoneId }).first?.offset
            // added
            if oldTask == nil {
                milestones[indexMovedMilestone!].tasks.append(newTask)
            }
            // not moved
            else if indexOriginalMilestone == indexMovedMilestone {
                let indexTask = milestones[indexOriginalMilestone!].tasks.indexForElement(newTask)
                milestones[indexOriginalMilestone!].tasks[indexTask!] = newTask
            }
            // moved
            else {
                let indexOriginalTask = milestones[indexOriginalMilestone!].tasks.indexForElement(oldTask!)
                milestones[indexOriginalMilestone!].tasks.remove(at: indexOriginalTask!)
                milestones[indexMovedMilestone!].tasks.append(newTask)
            }
        } else {
            if let indexTask = tasks.indexForElement(newTask) {
                tasks[indexTask] = newTask
            } else {
                tasks.append(newTask)
            }
        }
    }
    
    mutating func deleteMilestone(_ milestone: MilestoneModel) {
        guard let index = milestones.indexForElement(milestone) else { return }
        milestones[index].makeDeleted()
    }
    
    mutating func deleteTask(_ task: TaskModel) {
        switch paymentMethod {
        case .hourly:
            guard let index = tasks.indexForElement(task) else { return }
            tasks[index].makeDeleted()
            
        case .fixed:
            for (indexMilestone, milestone) in milestones.enumerated() {
                guard let index = milestone.tasks.indexForElement(task) else { return }
                milestones[indexMilestone].tasks[index].makeDeleted()
            }
            
        }
    }
    
    var companion: ProfileModel? {
        switch CurrentUser.profile?.role {
        
        case .freelancer?:
            return masterJob.owner
            
        case .employer?:
            if let suggestion = suggestion {
                return suggestion.freelancer
            } else {
                return freelancer
            }
            
        default:
            return nil
        }
    }
    
    var conversationId: Int? {
        var conversationId: Int?
        if let suggestion = suggestion {
            conversationId = suggestion.conversation.id
        } else if let conversation = conversation {
            conversationId = conversation.id
        }
        return conversationId
    }
    
    var conversationKey: String? {
        var conversationKey: String?
        if let suggestion = suggestion {
            conversationKey = suggestion.conversation.routingKey
        } else if let conversation = conversation {
            conversationKey = conversation.routingKey
        }
        return conversationKey
    }
    
}

/// Clear history with non actual packageVersion
extension ProjectModel {
    
    mutating func prepareForView(packageChange: Int) {
        prepareMilestonesForView(packageChange: packageChange)
        prepareTasksForView(packageChange: packageChange)
        prepareSelfForView(packageChange: packageChange)
    }
    
    private mutating func prepareMilestonesForView(packageChange: Int) {
        rearrangeMilestoneMovedTasks()
        var copyMilestones = [MilestoneModel]()
        milestones.forEach({
            var copyMilestone = $0
            copyMilestone.prepareForView(packageChange: packageChange)
            copyMilestones.append(copyMilestone)
        })
        self.milestones = copyMilestones
    }
    
    private mutating func prepareTasksForView(packageChange: Int) {
        var copyTasks = [TaskModel]()
        tasks.forEach({
            var copyTask = $0
            copyTask.prepareForView(packageChange: packageChange)
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
    }
    
    private mutating func prepareSelfForView(packageChange: Int) {
    }
    
}

/// Prepare histories to edit
extension ProjectModel {
    
    mutating func prepareForEdited() {
        prepareMilestonesForEdited()
        prepareTasksForEdited()
        prepareSelfForEdited()
    }
    
    private mutating func prepareMilestonesForEdited() {
        rearrangeMilestoneMovedTasks()
        var copyMilestones = [MilestoneModel]()
        milestones.forEach({
            if $0.isDeleted { return }
            var copyMilestone = $0
            copyMilestone.prepareForEdited()
            copyMilestones.append(copyMilestone)
        })
        self.milestones = copyMilestones
    }
    
    private mutating func prepareTasksForEdited() {
        var copyTasks = [TaskModel]()
        tasks.forEach({
            if $0.isDeleted { return }
            var copyTask = $0
            copyTask.prepareForEdited()
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
    }
    
    private mutating func prepareSelfForEdited() {
    }
    
    private mutating func rearrangeMilestoneMovedTasks() {
        for task in movedMilestoneTasks {
            for (index, milestone) in milestones.enumerated() {
                if milestone.id == task.firstHistory.milestoneId, let indexTask = milestone.tasks.indexForElement(task) {
                    milestones[index].tasks.remove(at: indexTask)
                }
                if milestone.id == task.secondHistory.milestoneId {
                    milestones[index].tasks.append(task)
                }
            }
        }
    }
    
}

/// Clear current histories
extension ProjectModel {
    
    /// return max packageVersion or -1
    mutating func prepareForSend() -> Int {
        let maxProjectPackageVersion = prepareSelfForSend()
        let maxTaskPackageVersion = prepareTasksForSend()
        let maxMilestonePackageVersion = prepareMilestonesForSend()
        return [maxProjectPackageVersion, maxTaskPackageVersion, maxMilestonePackageVersion].max()!
    }
    
    /// return max packageVersion or -1
    private mutating func prepareSelfForSend() -> Int {
        return -1
    }
    
    /// return max packageVersion or -1
    private mutating func prepareMilestonesForSend() -> Int {
        var maxPackageVersion = -1
        var copyMilestones = [MilestoneModel]()
        milestones.forEach({
            var copyMilestone = $0
            let packageVersion = copyMilestone.prepareForSend()
            if packageVersion > maxPackageVersion { maxPackageVersion = packageVersion }
            copyMilestones.append(copyMilestone)
        })
        self.milestones = copyMilestones
        distributePaymentPercentage()
        unRearrangeMilestoneMovedTasks()
        return maxPackageVersion
    }
    
    /// return max packageVersion or -1
    private mutating func prepareTasksForSend() -> Int {
        var maxPackageVersion = -1
        var copyTasks = [TaskModel]()
        
        tasks.forEach({
            var copyTask = $0
            let packageVersion = copyTask.prepareForSend()
            if packageVersion > maxPackageVersion { maxPackageVersion = packageVersion }
            copyTasks.append(copyTask)
        })
        
        self.tasks = copyTasks
        
        return maxPackageVersion
    }
    
    private mutating func distributePaymentPercentage() {
        let modifiedMilestones = milestones.map({ $0.histories.filter({ $0.status == .added || $0.status == .deleted }) }).flatMap({ $0 })
        if modifiedMilestones.isEmpty { return }
        let actualMilestonesCount = milestones.filter({ $0.histories.filter({ $0.status == .deleted }).count == 0 }).count
        let paymentPercentage = 100 / actualMilestonesCount
        for (index, _) in milestones.enumerated() {
            if milestones[index].histories.isEmpty {
                var changedHistory = MilestoneHistoryModel(model: milestones[index])
                changedHistory.status = .changed
                changedHistory.paymentPercentage = paymentPercentage
                milestones[index].histories.append(changedHistory)
            } else {
                for (indexHistory, _) in milestones[index].histories.enumerated() {
                    milestones[index].histories[indexHistory].paymentPercentage = paymentPercentage
                }
            }
        }
    }
    
    private mutating func unRearrangeMilestoneMovedTasks() {
        for task in movedMilestoneTasks {
            for (index, milestone) in milestones.enumerated() {
                if milestone.id == task.secondHistory.milestoneId, let indexTask = milestone.tasks.indexForElement(task) {
                    milestones[index].tasks.remove(at: indexTask)
                }
                if milestone.id == task.firstHistory.milestoneId {
                    milestones[index].tasks.append(task)
                }
            }
        }
    }
    
}

extension ProjectModel {
    
    mutating func updateActualHistoryVersion(_ packageVersion: Int) {
        updateSelfActualHistoryVersion(packageVersion)
        updateMilestonesActualHistoryVersion(packageVersion)
        updateTasksActualHistoryVersion(packageVersion)
    }
    
    private mutating func updateSelfActualHistoryVersion(_ packageVersion: Int) {
    }
    
    private mutating func updateMilestonesActualHistoryVersion(_ packageVersion: Int) {
        var copyMilestones = [MilestoneModel]()
        milestones.forEach({
            var copyMilestone = $0
            copyMilestone.updateActualHistoryVersion(packageVersion)
            copyMilestones.append(copyMilestone)
        })
        self.milestones = copyMilestones
    }
    
    private mutating func updateTasksActualHistoryVersion(_ packageVersion: Int) {
        var copyTasks = [TaskModel]()
        tasks.forEach({
            var copyTask = $0
            copyTask.updateActualHistoryVersion(packageVersion)
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
    }
    
}

extension ProjectModel: Equatable {
    
    static func == (lhs: ProjectModel, rhs: ProjectModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}
