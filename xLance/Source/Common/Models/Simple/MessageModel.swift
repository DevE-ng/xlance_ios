//
//  MessageModel.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct MessageModel {
    
    var id: Int
    var file: String?
    var fileId: Int?
    var key: String
    var message: String
    var sentAt: Date
    var sentFrom: ProfileSimpleModel
    var status: MessageStatus
 
    static func makeFrom(_ model: MessageSimpleModel) -> MessageModel {
        return .init(id: model.id,
                     file: nil,
                     fileId: nil,
                     key: "",
                     message: model.message,
                     sentAt: Date(),
                     sentFrom: .init(id: model.userId, nickName: "", firstName: "", lastName: ""),
                     status: .pu)
    }
    
}

struct MessageSimpleModel {
    var id: Int
    var message: String
    var userId: Int
    
}

extension MessageModel: ImmutableMappable {
    init(map: Map) throws {
        id = try map.value("id")
        file = try? map.value("file")
        fileId = try? map.value("fileId")
        key = try map.value("key")
        message = try map.value("message")
        sentAt = Date.iso8601Formatter.date(from: try map.value("sentAt"))!
        sentFrom = try map.value("sentFrom")
        status = try map.value("status")
    }
    func mapping(map: Map) {
        id >>> map["id"]
        file >>> map["file"]
        fileId >>> map["fileId"]
        key >>> map["key"]
        message >>> map["message"]
        sentAt >>> map["sentAt"]
        sentFrom >>> map["sentFrom"]
        status >>> map["status"]
    }
}
