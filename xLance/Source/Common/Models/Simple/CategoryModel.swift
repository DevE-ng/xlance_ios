//
//  CategoryModel.swift
//  xlance
//
//  Created by Anton Lobanov on 12/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

struct CategoryModel {
    
    var id: Int
    var code: String
    var key: String
    var name: String
    var position: Int
    
    init?(model: JSON) {
        guard
            let id = model["id"].int,
            let code = model["code"].string,
            let key = model["key"].string,
            let name = model["name"].string,
            let position = model["position"].int else {
            return nil
        }
        self.id = id
        self.code = code
        self.key = key
        self.name = name
        self.position = position
    }
    
}

extension CategoryModel: ImmutableMappable {
    init(map: Map) throws {
        id = try map.value("id")
        name = try map.value("name")
        code = try map.value("code")
        key = try map.value("key")
        position = try map.value("position")
    }
    func mapping(map: Map) {
        id          >>> map["id"]
        name        >>> map["name"]
        code        >>> map["code"]
        key         >>> map["key"]
        position    >>> map["position"]
    }
}

extension CategoryModel: Comparable {
    
    static func < (lhs: CategoryModel, rhs: CategoryModel) -> Bool {
        return lhs.position < rhs.position
    }
    
    static func == (lhs: CategoryModel, rhs: CategoryModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}
