//
//  Conversation.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct ConversationModel {
    
    var id: Int
    var key: String
    var routingKey: String
    
    init() {
        self.id = 0
        self.key = ""
        self.routingKey = ""
    }
    
}

extension ConversationModel: ImmutableMappable {
    init(map: Map) throws {
        id = try map.value("id")
        key = try map.value("key")
        routingKey = try map.value("routingKey")
    }
    func mapping(map: Map) {
        id >>> map["id"]
        key >>> map["key"]
        routingKey >>> map["routingKey"]
    }
}
