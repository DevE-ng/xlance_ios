//
//  StatusModel.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct StatusModel {
    
    var profileId: Int
    var status: ProfileStatus
    
}

extension StatusModel {
    var online: Bool {
        return status == .online
    }
}

extension StatusModel: ImmutableMappable {
    init(map: Map) throws {
        profileId = try map.value("profileId")
        status = try map.value("status")
    }
    func mapping(map: Map) {
        profileId >>> map["profileId"]
        status >>> map["status"]
    }
}
