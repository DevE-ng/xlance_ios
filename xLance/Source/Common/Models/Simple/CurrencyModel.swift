//
//  CurrencyModel.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct CurrencyModel {
    
    var id: Int
    var key: String
    var code: String
    
}

extension CurrencyModel: ImmutableMappable {
    init(map: Map) throws {
        id = try map.value("id")
        key = try map.value("key")
        code = try map.value("code")
    }
    func mapping(map: Map) {
        id >>> map["id"]
        key >>> map["key"]
        code >>> map["code"]
    }
}
