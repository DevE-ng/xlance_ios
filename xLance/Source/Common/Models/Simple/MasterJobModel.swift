//
//  MasterJobModel.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct MasterJobModel {
    
    var id: Int
    var name: String
    var owner: ProfileModel
    
}

extension MasterJobModel: ImmutableMappable {
    init(map: Map) throws {
        id = try map.value("id")
        name = try map.value("name")
        owner = try map.value("owner")
    }
    func mapping(map: Map) {
        id          >>> map["id"]
        name        >>> map["name"]
        owner       >>> map["owner"]
    }
}

