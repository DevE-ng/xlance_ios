//
//  ProfileSimpleModel.swift
//  xlance
//
//  Created by Anton Lobanov on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProfileSimpleModel {
    
    var id: Int
    var nickName: String
    var firstName: String
    var lastName: String
    
}

extension ProfileSimpleModel {
    
    var fullName: String {
        return firstName + " " + lastName
    }
    
}


extension ProfileSimpleModel: ImmutableMappable {
    init(map: Map) throws {
        id              = try map.value("id")
        nickName        = try map.value("nickName")
        firstName       = try map.value("firstName")
        lastName        = try map.value("lastName")
    }
    func mapping(map: Map) {
        id              >>> map["id"]
        nickName        >>> map["nickName"]
        firstName       >>> map["firstName"]
        lastName        >>> map["lastName"]
    }
}
