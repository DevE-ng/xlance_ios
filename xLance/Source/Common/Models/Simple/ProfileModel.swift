//
//  XProfileModel.swift
//  xlance
//
//  Created by Anton Lobanov on 08/08/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

struct ProfileModel {
    
    var id: Int
    var email: String?
    var nickName: String
    var firstName: String
    var lastName: String
    var role: Role
    var rabbitmqHash: Int
    var rabbitmqKey: String?
    var categories: [CategoryModel] = []
    
    init?(jsonString: String) {
        let json = JSON(parseJSON: jsonString)
        guard
            let id = json["id"].int,
            let email = json["email"].string,
            let nickName = json["nickName"].string,
            let firstName = json["firstName"].string,
            let lastName = json["lastName"].string,
            let role = json["role"].string,
            let rabbitmqHash = json["rabbitmqHash"].int,
            let rabbitmqKey = json["rabbitmqKey"].string else {
                return nil
        }
        self.id = id
        self.email = email
        self.nickName = nickName
        self.firstName = firstName
        self.lastName = lastName
        self.role = Role.init(rawValue: role)!
        self.rabbitmqHash = rabbitmqHash
        self.rabbitmqKey = rabbitmqKey
        if let categories = json["categories"].array {
            self.categories = categories.compactMap({ CategoryModel(model: $0) })
        }
    }
    
}

extension ProfileModel {
    
    enum Role: String, CaseIterable, Encodable {
        case employer = "EP"
        case freelancer = "FL"
        
        var localized: String {
            switch self {
            case .employer: return "PROFILE.EMPLOYER".localized
            case .freelancer: return "PROFILE.FREELANCER".localized
            }
        }
        
    }
    
    var fullName: String {
        return firstName + " " + lastName
    }
    
    var login: String {
        return email ?? ""
    }
    
    var passcode: String {
        return rabbitmqKey ?? ""
    }
    
}


extension ProfileModel: ImmutableMappable {
    init(map: Map) throws {
        id              = try map.value("id")
        email           = try? map.value("email")
        nickName        = try map.value("nickName")
        firstName       = try map.value("firstName")
        lastName        = try map.value("lastName")
        role            = try map.value("role")
        rabbitmqHash    = try map.value("rabbitmqHash")
        rabbitmqKey     = try? map.value("rabbitmqKey")
        categories      = (try? map.value("categories")) ?? []
    }
    func mapping(map: Map) {
        id              >>> map["id"]
        email           >>> map["email"]
        nickName        >>> map["nickName"]
        firstName       >>> map["firstName"]
        lastName        >>> map["lastName"]
        role            >>> map["role"]
        rabbitmqHash    >>> map["rabbitmqHash"]
        rabbitmqKey     >>> map["rabbitmqKey"]
        []              >>> map["categories"]
    }
}

extension ProfileModel: Equatable {
    
}
