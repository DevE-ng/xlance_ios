//
//  MilestoneModel.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct MilestoneModel {
    
    var id: Int
    var projectId: Int
    var index: Int = 0
    var name: String = ""
    var startDate: Date
    var tillDate: Date
    var daysForReview: Int
    var paymentPercentage: Int
    var changeVersion: Int?
    var tasks: [TaskModel]
    var histories: [MilestoneHistoryModel] = []
    
    init(index: Int, projectId: Int) {
        self.id = -UUID().hashValue
        self.projectId = projectId
        self.index = index
        self.name = "\("COMMON.MILESTONE".localized) #\(index)"
        self.startDate = Date()
        self.tillDate = Date()
        self.daysForReview = 3
        self.paymentPercentage = 1
        self.changeVersion = 0
        self.tasks = []
        
        var addedHistory = MilestoneHistoryModel(model: self)
        addedHistory.status = .added
        histories.append(addedHistory)
    }
    
}

extension MilestoneModel: ImmutableMappable {
    init(map: Map) throws {
        id                      = try map.value("id")
        projectId               = try map.value("projectId")
        startDate               = Date.iso8601Formatter.date(from: try map.value("startDate"))!
        tillDate                = Date.iso8601Formatter.date(from: try map.value("tillDate"))!
        daysForReview           = try map.value("daysForReview")
        paymentPercentage       = try map.value("paymentPercentage")
        changeVersion           = try? map.value("changeVersion")
        tasks                   = try map.value("tasks")
        histories               = (try? map.value("histories")) ?? []
    }
    func mapping(map: Map) {
        id                  >>> map["id"]
        projectId           >>> map["projectId"]
        startDate.iso8601   >>> map["startDate"]
        tillDate.iso8601    >>> map["tillDate"]
        daysForReview       >>> map["daysForReview"]
        paymentPercentage   >>> map["paymentPercentage"]
        changeVersion       >>> map["changeVersion"]
        tasks               >>> map["tasks"]
        histories           >>> map["histories"]
    }
}

extension MilestoneModel {
    
    var paymentDate: Date {
        return Calendar.current.date(byAdding: .day, value: daysForReview, to: tillDate)!
    }
    
    var isAdded: Bool {
        return histories.filter({ $0.status == .added }).count > 0
    }
    
    var isDeleted: Bool {
        return histories.filter({ $0.status == .deleted }).count > 0
    }
    
    /// WARNING: force
    var actualHistory: MilestoneHistoryModel {
        get {
            let index = histories.lastIndex()!
            return histories[index]
        }
        set {
            let index = histories.lastIndex()!
            histories[index] = newValue
        }
    }
    
    /// WARNING: force
    var firstHistory: MilestoneHistoryModel {
        get {
            return histories[0]
        }
        set {
            histories[0] = newValue
        }
    }
    
    /// WARNING: force
    var secondHistory: MilestoneHistoryModel {
        get {
            return histories[1]
        }
        set {
            histories[1] = newValue
        }
    }
    
}

/// Clear history with non actual packageVersion
extension MilestoneModel {
    
    mutating func prepareForView(packageChange: Int) {
        prepareTasksForView(packageChange: packageChange)
        prepareSelfForView(packageChange: packageChange)
    }
    
    private mutating func prepareTasksForView(packageChange: Int) {
        var copyTasks = [TaskModel]()
        tasks.forEach({
            var copyTask = $0
            copyTask.prepareForView(packageChange: packageChange)
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
    }
    
    private mutating func prepareSelfForView(packageChange: Int) {
        if
            (histories.count == 2 && actualHistory.packageVersion == packageChange) ||
            (histories.count == 1) && actualHistory.status == .current { return }
        guard let indexOldHistory = histories.map({ $0.packageVersion }).enumerated().filter({ $0.element != packageChange }).first?.offset else { return }
        let _ = histories.remove(at: indexOldHistory)
    }
    
}

/// Prepare histories to edit
extension MilestoneModel {
    
    mutating func makeDeleted() {
        actualHistory.status = .deleted
    }
    
    mutating func prepareForEdited() {
        prepareTasksForEdited()
        prepareSelfForEdited()
    }
    
    private mutating func prepareTasksForEdited() {
        var copyTasks = [TaskModel]()
        tasks.forEach({
            if $0.isDeleted { return }
            var copyTask = $0
            copyTask.prepareForEdited()
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
    }
    
    private mutating func prepareSelfForEdited() {
        if histories.isEmpty {
            let currentHistory = MilestoneHistoryModel(model: self)
            histories = [currentHistory, currentHistory]
            return
        }
        if histories.count == 2 {
            let _ = histories.remove(at: 0)
        }
        let currentHistory = createCurrentHistory()
        histories.append(currentHistory)
    }
    
    private func createCurrentHistory() -> MilestoneHistoryModel {
        return MilestoneHistoryModel(model: histories.last!)
    }
    
}

/// Clear current histories
extension MilestoneModel {
    
    /// return max packageVersion or -1
    mutating func prepareForSend() -> Int {
        let maxTaskPackageVersion = prepareTasksForSend()
        let maxMilestonePackageVersion = prepareSelfForSend()
        return [maxTaskPackageVersion, maxMilestonePackageVersion].max()!
    }
    
    /// return max packageVersion or -1
    private mutating func prepareSelfForSend() -> Int {
        if histories.count < 2 { return -1 }
        if !secondHistory.equalTo(firstHistory) && secondHistory.status == .current {
            secondHistory.status = .changed
        }
        histories.removeAll(where: { $0.status == .current })
        return histories.map({ $0.packageVersion }).max() ?? -1
    }
    
    /// return max task packageVersion or -1
    private mutating func prepareTasksForSend() -> Int {
        var maxPackageVersion = -1
        var copyTasks = [TaskModel]()
        tasks.forEach({
            var copyTask = $0
            let packageVersion = copyTask.prepareForSend()
            if packageVersion > maxPackageVersion { maxPackageVersion = packageVersion }
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
        return maxPackageVersion
    }
    
}

extension MilestoneModel {
    
    mutating func updateActualHistoryVersion(_ packageVersion: Int) {
        updateSelfActualHistoryVersion(packageVersion)
        updateTasksActualHistoryVersion(packageVersion)
    }
    
    private mutating func updateSelfActualHistoryVersion(_ packageVersion: Int) {
        if histories.count > 0 {
            actualHistory.packageVersion = packageVersion
        }
    }
    
    private mutating func updateTasksActualHistoryVersion(_ packageVersion: Int) {
        var copyTasks = [TaskModel]()
        tasks.forEach({
            var copyTask = $0
            copyTask.updateActualHistoryVersion(packageVersion)
            copyTasks.append(copyTask)
        })
        self.tasks = copyTasks
    }
    
}

extension MilestoneModel: Equatable {
    
    static func == (lhs: MilestoneModel, rhs: MilestoneModel) -> Bool {
        return lhs.id == rhs.id
    }
    
}
