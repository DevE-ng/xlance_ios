//
//  SuggestionHistoryModel.swift
//  xlance
//
//  Created by alobanov11 on 15/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct SuggestionHistoryModel {
    
    var id: Int
    var status: HistoryStatus
    var changeVersion: Int
    var parentId: Int
    var packageVersion: Int
    var suggestionId: Int
    var budget: Decimal
    var startDate: Date
    var tillDate: Date
    var paymentMethod: PaymentMethod
    var hoursPerWeek: Int
    var paymentPeriod: Int
    var currency: CurrencyModel
    
    /// current
    init(model: SuggestionModel) {
        self.id = 0
        self.status = .current
        self.changeVersion = 0
        self.parentId = model.id < 0 ? 0 : model.id
        self.packageVersion = 0
        self.suggestionId = model.id < 0 ? 0 : model.id
        self.budget = model.budget
        self.startDate = model.startDate
        self.tillDate = model.tillDate
        self.paymentMethod = model.paymentMethod
        self.hoursPerWeek = model.hoursPerWeek
        self.paymentPeriod = model.paymentPeriod
        self.currency = model.currency
    }
    
    /// current by history
    init(model: SuggestionHistoryModel) {
        self.id = 0
        self.status = .current
        self.changeVersion = model.changeVersion
        self.parentId = model.parentId
        self.packageVersion = model.packageVersion + 1
        self.suggestionId = model.suggestionId
        self.budget = model.budget
        self.startDate = model.startDate
        self.tillDate = model.tillDate
        self.paymentMethod = model.paymentMethod
        self.hoursPerWeek = model.hoursPerWeek
        self.paymentPeriod = model.paymentPeriod
        self.currency = model.currency
    }
    
}

extension SuggestionHistoryModel: ImmutableMappable {
    init(map: Map) throws {
        id                      = try map.value("id")
        status                  = try map.value("status")
        changeVersion           = try map.value("changeVersion")
        parentId                = try map.value("parentId")
        packageVersion          = try map.value("packageVersion")
        suggestionId            = try map.value("suggestionId")
        budget                  = try map.value("budget", using: NSDecimalNumberTransform()) as Decimal
        startDate               = Date.iso8601Formatter.date(from: try map.value("startDate"))!
        tillDate                = Date.iso8601Formatter.date(from: try map.value("tillDate"))!
        paymentMethod           = try map.value("paymentMethod")
        hoursPerWeek            = try map.value("hoursPerWeek")
        paymentPeriod           = try map.value("paymentPeriod")
        currency                = try map.value("currency")
    }
    func mapping(map: Map) {
        id                      >>> map["id"]
        status                  >>> map["status"]
        changeVersion           >>> map["changeVersion"]
        parentId                >>> map["parentId"]
        packageVersion          >>> map["packageVersion"]
        suggestionId            >>> map["suggestionId"]
        budget                  >>> map["budget"]
        startDate.iso8601       >>> map["startDate"]
        tillDate.iso8601        >>> map["tillDate"]
        paymentMethod           >>> map["paymentMethod"]
        hoursPerWeek            >>> map["hoursPerWeek"]
        paymentPeriod           >>> map["paymentPeriod"]
        currency.key            >>> map["currency"]
    }
}

extension SuggestionHistoryModel {
    
    func equalTo(_ new: SuggestionHistoryModel) -> Bool {
        return
            budget == new.budget &&
            startDate == new.startDate &&
            tillDate == new.tillDate &&
            hoursPerWeek == new.hoursPerWeek &&
            paymentPeriod == new.paymentPeriod &&
            status == new.status
    }
    
}
