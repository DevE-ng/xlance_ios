//
//  TaskHistoryModel.swift
//  xlance
//
//  Created by alobanov11 on 15/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct TaskHistoryModel {
    
    var id: Int
    var status: HistoryStatus
    var changeVersion: Int
    var parentId: Int
    var packageVersion: Int
    var suggestionId: Int
    var name: String
    var description: String
    var duration: Int
    var milestoneId: Int?
    
    /// new task
    init() {
        self.id = 0
        self.status = .added
        self.changeVersion = 0
        self.parentId = 0
        self.packageVersion = 0
        self.suggestionId = -1
        self.name = ""
        self.description = ""
        self.duration = 0
        self.milestoneId = -1
    }
    
    /// current
    init(model: TaskModel) {
        self.id = 0
        self.status = .current
        self.changeVersion = model.changeVersion ?? 0
        self.parentId = model.id
        self.packageVersion = 0
        self.suggestionId = -1
        self.name = model.name
        self.description = model.description
        self.duration = model.duration
        self.milestoneId = model.milestoneId
    }
    
    /// current by last history
    init(model: TaskHistoryModel) {
        self.id = 0
        self.status = .current
        self.changeVersion = model.changeVersion
        self.parentId = model.parentId
        self.packageVersion = model.packageVersion + 1
        self.suggestionId = model.suggestionId
        self.name = model.name
        self.description = model.description
        self.duration = model.duration
        self.milestoneId = model.milestoneId
    }
    
}

extension TaskHistoryModel: ImmutableMappable {
    init(map: Map) throws {
        id              = try map.value("id")
        status          = try map.value("status")
        changeVersion   = try map.value("changeVersion")
        parentId        = try map.value("parentId")
        packageVersion  = try map.value("packageVersion")
        suggestionId    = try map.value("suggestionId")
        name            = try map.value("name")
        description     = (try? map.value("description")) ?? ""
        duration        = try map.value("duration")
        milestoneId     = try map.value("milestoneId")
    }
    func mapping(map: Map) {
        id              >>> map["id"]
        status          >>> map["status"]
        changeVersion   >>> map["changeVersion"]
        parentId        >>> map["parentId"]
        packageVersion  >>> map["packageVersion"]
        suggestionId    >>> map["suggestionId"]
        name            >>> map["name"]
        description     >>> map["description"]
        duration        >>> map["duration"]
        milestoneId     >>> map["milestoneId"]
    }
}

extension TaskHistoryModel {
    
    func equalTo(_ new: TaskHistoryModel) -> Bool {
        return
                suggestionId == new.suggestionId &&
                name == new.name &&
                description == new.description &&
                duration == new.duration &&
                milestoneId == new.milestoneId &&
                status == new.status
    }
    
}
