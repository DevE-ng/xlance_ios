//
//  MilestoneHistoryModel.swift
//  xlance
//
//  Created by alobanov11 on 15/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

struct MilestoneHistoryModel {
    
    var id: Int
    var status: HistoryStatus
    var changeVersion: Int
    var parentId: Int
    var packageVersion: Int
    var suggestionId: Int
    var startDate: Date
    var tillDate: Date
    var daysForReview: Int
    var paymentPercentage: Int
    
    // new
    init() {
        self.id = 0
        self.status = .added
        self.changeVersion = 0
        self.parentId = -1
        self.packageVersion = 0
        self.suggestionId = -1
        self.startDate = Date()
        self.tillDate = Date()
        self.daysForReview = 3
        self.paymentPercentage = 100
    }
    
    /// current
    init(model: MilestoneModel) {
        self.id = 0
        self.status = .current
        self.changeVersion = model.changeVersion ?? 0
        self.parentId = model.id
        self.packageVersion = 0
        self.suggestionId = -1
        self.startDate = model.startDate
        self.tillDate = model.tillDate
        self.daysForReview = model.daysForReview
        self.paymentPercentage = model.paymentPercentage
    }
    
    /// current by last history
    init(model: MilestoneHistoryModel) {
        self.id = 0
        self.status = .current
        self.changeVersion = model.changeVersion
        self.parentId = model.parentId
        self.packageVersion = model.packageVersion + 1
        self.suggestionId = model.suggestionId
        self.startDate = model.startDate
        self.tillDate = model.tillDate
        self.daysForReview = model.daysForReview
        self.paymentPercentage = model.paymentPercentage
    }
    
}

extension MilestoneHistoryModel: ImmutableMappable {
    init(map: Map) throws {
        id                  = try map.value("id")
        status              = try map.value("status")
        changeVersion       = try map.value("changeVersion")
        parentId            = try map.value("parentId")
        packageVersion      = try map.value("packageVersion")
        suggestionId        = try map.value("suggestionId")
        startDate           = Date.iso8601Formatter.date(from: try map.value("startDate"))!
        tillDate            = Date.iso8601Formatter.date(from: try map.value("tillDate"))!
        daysForReview       = try map.value("daysForReview")
        paymentPercentage   = try map.value("paymentPercentage")
    }
    func mapping(map: Map) {
        id                  >>> map["id"]
        status              >>> map["status"]
        changeVersion       >>> map["changeVersion"]
        parentId            >>> map["parentId"]
        packageVersion      >>> map["packageVersion"]
        suggestionId        >>> map["suggestionId"]
        startDate.iso8601   >>> map["startDate"]
        tillDate.iso8601    >>> map["tillDate"]
        daysForReview       >>> map["daysForReview"]
        paymentPercentage   >>> map["paymentPercentage"]
    }
}

extension MilestoneHistoryModel {
    
    var paymentDate: Date {
        return Calendar.current.date(byAdding: .day, value: daysForReview, to: tillDate)!
    }
    
}

extension MilestoneHistoryModel {
    
    func equalTo(_ new: MilestoneHistoryModel) -> Bool {
        return
            startDate == new.startDate &&
            tillDate == new.tillDate &&
            daysForReview == new.daysForReview &&
            paymentPercentage == new.paymentPercentage &&
            status == new.status
    }
    
}
