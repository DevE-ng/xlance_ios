//
//  ProjectsWorker.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol SuggestionWorker {
    func fetchSuggestion(id: Int, completionHandler: @escaping (Result<SuggestionModel>) -> Void)
    func fetchOffers(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void)
    func fetchProposals(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void)
    func sendProposal(_ project: ProjectModel, completionHandler: @escaping (Result<ProjectModel>) -> Void)
    func declineSuggestion(id: Int, completionHandler: @escaping (Result<Bool>) -> Void)
    
    func makeProjects(from suggestions: [SuggestionModel]) -> [ProjectModel]
}

struct SuggestionWorkerImp {
    let networkProvider: Networking
}

extension SuggestionWorkerImp: SuggestionWorker {
    
    func fetchOffers(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void) {
        networkProvider.fetchOffers(completionHandler: completionHandler)
    }
    
    func fetchProposals(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void) {
        networkProvider.fetchProposals(completionHandler: completionHandler)
    }
    
    func fetchSuggestion(id: Int, completionHandler: @escaping (Result<SuggestionModel>) -> Void) {
        networkProvider.fetchSuggestion(id, completionHandler: completionHandler)
    }
    
    func declineSuggestion(id: Int, completionHandler: @escaping (Result<Bool>) -> Void) {
        networkProvider.declineSuggestion(id, completionHandler: completionHandler)
    }
    
    func sendProposal(_ project: ProjectModel, completionHandler: @escaping (Result<ProjectModel>) -> Void) {
        var copyProject = project
        var suggestion = SuggestionModel(model: project, freelancer: CurrentUser.profile!)
        suggestion.prepareForSend()
        networkProvider.createSuggestion(suggestion) {
            switch $0 {
                
            case .Success(let suggestion):
                copyProject.suggestions.append(suggestion)
                completionHandler(.Success(copyProject))
                
            case .Error(let error):
                completionHandler(.Error(error))
                
            }
        }
    }
    
    func makeProjects(from suggestions: [SuggestionModel]) -> [ProjectModel] {
        var projects = [ProjectModel]()
        for suggestion in suggestions {
            var project: ProjectModel! = suggestion.project
            project.suggestions.append(suggestion)
            projects.append(project)
        }
        return projects
    }
    
}
