//
//  MyProjectsWorker.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol MyProjectsWorker {
    func fetchProjectsStatuses(_ projects: [ProjectModel], completionHandler: @escaping (Result<[StatusModel]>) -> Void)
    func fetchMyProjects(completionHandler: @escaping ([ProjectModel]) -> Void)
}

class MyProjectsWorkerImp {
    
    let projectsWorker: ProjectsWorker
    let suggestionsWorker: SuggestionWorker
    
    private var projects = [ProjectModel]()
    private var group = DispatchGroup()
    
    init(projectsWorker: ProjectsWorker, suggestionsWorker: SuggestionWorker) {
        self.projectsWorker = projectsWorker
        self.suggestionsWorker = suggestionsWorker
    }
    
}

// MARK: - MyProjectListWorker
extension MyProjectsWorkerImp: MyProjectsWorker {
    
    func fetchProjectsStatuses(_ projects: [ProjectModel], completionHandler: @escaping (Result<[StatusModel]>) -> Void) {
        projectsWorker.fetchProjectsStatuses(projects, completionHandler: completionHandler)
    }
    
    func fetchMyProjects(completionHandler: @escaping ([ProjectModel]) -> Void) {
        projects.removeAll()
        
        let queue = DispatchQueue.global(qos: .userInteractive)
        
        queue.async(group: group) { [weak self] in
            guard let `self` = self else { return }
            self.group.enter()
            self.projectsWorker.fetchAcceptedProjects(completionHandler: self.handleFetchAcceptedProjects)
        }
        
        queue.async(group: group) { [weak self] in
            guard let `self` = self else { return }
            self.group.enter()
            self.suggestionsWorker.fetchOffers(completionHandler: self.handleFetchOffers)
        }
        
        queue.async(group: group) { [weak self] in
            guard let `self` = self else { return }
            self.group.enter()
            self.suggestionsWorker.fetchProposals(completionHandler: self.handleFetchProposals)
        }
        
        group.notify(queue: .main) { [weak self] in
            guard let `self` = self else { return }
            completionHandler(self.projects)
        }
    }
    
}

// MARK: - Private
fileprivate extension MyProjectsWorkerImp {
    
    func handleFetchAcceptedProjects(_ result: Result<[ProjectModel]>) {
        group.leave()
        guard let projects = result.value else { return }
        self.projects += projects
    }
    
    func handleFetchOffers(_ result: Result<[SuggestionModel]>) {
        group.leave()
        guard let suggestions = result.value else { return }
        self.projects += suggestionsWorker.makeProjects(from: suggestions)
    }
    
    func handleFetchProposals(_ result: Result<[SuggestionModel]>) {
        group.leave()
        guard let suggestions = result.value else { return }
        self.projects += suggestionsWorker.makeProjects(from: suggestions)
    }
    
}
