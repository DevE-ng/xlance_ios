//
//  Worker.swift
//  xlance
//
//  Created by Anton Lobanov on 05/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ProjectsWorker {
    func fetchProjectsStatuses(_ projects: [ProjectModel], completionHandler: @escaping (Result<[StatusModel]>) -> Void)
    func fetchProjects(index: Int, size: Int, completionHandler: @escaping (Result<[ProjectModel]>) -> Void)
    func fetchAcceptedProjects(completionHandler: @escaping (Result<[ProjectModel]>) -> Void)
}

struct ProjectsWorkerImp {
    let networkProvider: Networking
}

extension ProjectsWorkerImp: ProjectsWorker {
    
    func fetchProjectsStatuses(_ projects: [ProjectModel], completionHandler: @escaping (Result<[StatusModel]>) -> Void) {
        let profiles = projects.compactMap({ $0.companion })
        networkProvider.fetchStatuses(profiles: profiles, completionHandler: completionHandler)
    }
    
    func fetchProjects(index: Int, size: Int, completionHandler: @escaping (Result<[ProjectModel]>) -> Void) {
        networkProvider.fetchProjects(index: index, size: size) {
            switch $0 {
                
            case .Success(let data):
                completionHandler(.Success(data.projects))
                
            case .Error(let error):
                completionHandler(.Error(error))
                
            }
        }
    }
    
    func fetchAcceptedProjects(completionHandler: @escaping (Result<[ProjectModel]>) -> Void) {
        switch CurrentUser.isFreelancer {
        
        case true:
            networkProvider.fetchFreelancersProjects(completionHandler: completionHandler)
            
        case false:
            completionHandler(.Success([]))
            
        }
    }
    
}
