//
//  DependencyProvider.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

final class ModulesFactory {
    
    var networkProvider: Networking
    var chatProvider: Chating
    var callProvider: Calling
    
    init(networkProvider: Networking, chatProvider: Chating, callProvider: Calling) {
        self.networkProvider = networkProvider
        self.chatProvider = chatProvider
        self.callProvider = callProvider
    }
    
}

extension ModulesFactory: AppCoordinatorFactory {
    func makeLaunchScreenView(coordinator: LaunchScreenCoordinatable) -> BaseViewProtocol {
        let configurator = LaunchScreenConfigurator(networkProvider: networkProvider,
                                                    coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}


// MARK: - AuthCoordinatorFactory
extension ModulesFactory: AuthCoordinatorFactory {
    func makeAuthView(startScene: AuthScene, coordinator: AuthCoordinatable) -> BaseViewProtocol {
        let configurator = AuthConfigurator(scene: startScene,
                                            networkProvider: networkProvider,
                                            coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}

// MARK: - MainCoordinatorFactory
extension ModulesFactory: MainCoordinatorFactory {
    func makeMainView(coordinator: MainCoordinatable) -> Presentable {
        let configurator = MainConfigurator(coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}

// MARK: - ProjectListCoordinatorFactory
extension ModulesFactory: ProjectListCoordinatorFactory {
    func makeProjectListView(router: Routable, coordinator: ProjectListCoordinatable) -> BaseViewProtocol {
        let configurator = ProjectListConfigurator(networkProvider: networkProvider, coordinator: coordinator)
        if let vc = router.rootModule as? ProjectListViewController {
            configurator.configure(viewController: vc)
            return vc
        } else {
            let vc = configurator.create()
            return vc
        }
    }
    
    func makeMyProjectListView(router: Routable,
                               coordinator: ProjectListCoordinatable) -> BaseViewProtocol {
        let configurator = MyProjectListConfigurator(networkProvider: networkProvider, coordinator: coordinator)
        if let vc = router.rootModule as? MyProjectListViewController {
            configurator.configure(viewController: vc)
            return vc
        } else {
            let vc = configurator.create()
            return vc
        }
    }
}

// MARK: - ProjectDetailsCoordinatorFactory
extension ModulesFactory: ProjectDetailsCoordinatorFactory {
    func makeProjectDetailsView(project: ProjectModel, coordinator: ProjectDetailsCoordinatable) -> BaseViewProtocol {
        let configurator = ProjectDetailsConfigurator(project: project,
                                                      networkProvider: networkProvider,
                                                      coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}

// MARK: - SuggestionDetailsCoordinatorFactory
extension ModulesFactory: SuggestionDetailsCoordinatorFactory {
    func makeSuggestionDetailsView(suggestion: SuggestionModel, coordinator: SuggestionDetailsCoordinatable) -> BaseViewProtocol {
        let configurator = SuggestionDetailsConfigurator(suggestion: suggestion,
                                                         networkProvider: networkProvider,
                                                         coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}

// MARK: - SuggestionEditCoordinatorFactory
extension ModulesFactory: SuggestionEditCoordinatorFactory {
    func makeSuggestionEditView(suggestion: SuggestionModel, coordinator: SuggestionEditCoordinatable) -> BaseViewProtocol {
        let configurator = SuggestionEditConfigurator(suggestion: suggestion,
                                                      networkProvider: networkProvider,
                                                      coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
    
    func makeMilestoneEditView(milestone: MilestoneModel, project: ProjectModel, coordinator: MilestoneEditCoordinatable) -> BaseViewProtocol {
        let configurator = MilestoneEditConfigurator(milestone: milestone, project: project,
                                                     networkProvider: networkProvider,
                                                     coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
    
    func makeTaskEditView(task: TaskModel, project: ProjectModel, coordinator: TaskEditCoordinatable) -> BaseViewProtocol {
        let configurator = TaskEditConfigurator(task: task, project: project,
                                                networkProvider: networkProvider,
                                                coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
    
    func makeMilestoneListView(milestones: [MilestoneModel], selectedMilestoneId: Int?,
                           coordinator: MilestoneListCoordinatable) -> BaseViewProtocol
    {
        let configurator = MilestoneListConfigurator(milestones: milestones,
                                                     selectedMilestoneId: selectedMilestoneId,
                                                     coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}

// MARK: - ProfileCoordinatorFactory
extension ModulesFactory: ProfileCoordinatorFactory {
    func makeProfileView(profile: ProfileModel?, router: Routable,
                         coordinator: ProfileCoordinatable) -> BaseViewProtocol {
        let configurator = ProfileConfigurator(profile: profile,
                                               networkProvider: networkProvider,
                                               coordinator: coordinator)
        if let vc = router.rootModule as? ProfileViewController {
            configurator.configure(viewController: vc)
            return vc
        } else {
            let vc = configurator.create()
            return vc
        }
    }
}

// MARK: - CallCoordinatorFactory
extension ModulesFactory: CallCoordinatorFactory {
    func makeCallView(user: ProfileModel, coordinator: CallCoordinatable) -> BaseViewProtocol {
        let configurator = CallConfigurator(user: user,
                                            callProvider: callProvider,
                                            networkProvider: networkProvider,
                                            coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}

// MARK: - ChatListCoordinatorFactory
extension ModulesFactory: ChatListCoordinatorFactory {
    func makeChatListView(coordinator: ChatListCoordinatable, router: Routable) -> BaseViewProtocol {
        let configurator = ChatListConfigurator(networkProvider: networkProvider, coordinator: coordinator)
        if let vc = router.rootModule as? ChatListViewController {
            configurator.configure(viewController: vc)
            return vc
        } else {
            let vc = configurator.create()
            return vc
        }
    }
}

// MARK: - ChatDetailsCoordinatorFactory
extension ModulesFactory: ChatDetailsCoordinatorFactory {
    func makeChatDetailsView(project: ProjectModel, coordinator: ChatDetailsCoordinatable) -> BaseViewProtocol {
        let configurator = ChatDetailsConfigurator(project: project,
                                                   chatProvider: chatProvider,
                                                   networkProvider: networkProvider,
                                                   coordinator: coordinator)
        let vc = configurator.create()
        return vc
    }
}
