//
//  BasePresenterInput.swift
//
//  Created by alobanov11 on 28/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import Foundation

protocol BasePresenterOutput: Presentable {}

protocol BasePresenterInput {
    func viewDidLoad()
}
