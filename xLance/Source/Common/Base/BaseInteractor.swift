//
//  BaseInteractor.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol BaseInteractorOutput: class {}
protocol BaseInteractorInput {}
