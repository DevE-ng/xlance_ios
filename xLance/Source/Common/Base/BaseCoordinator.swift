//
//  BaseViewRoutable.swift
//
//  Created by alobanov11 on 28/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import UIKit

class BaseCoordinator: NSObject {
    
    let identifier = UUID()
    let router: Routable
    var childCoordinators: [UUID: BaseCoordinator] = [:]
    
    init(router: Routable) {
        self.router = router
    }
        
    func start() {
        fatalError("Method start can implemented")
    }
    
    func addChildCoordinator(_ coordinator: BaseCoordinator) {
        childCoordinators[coordinator.identifier] = coordinator
    }
    
    func removeChildCoordinator(_ coordinator: BaseCoordinator) {
        guard !childCoordinators.isEmpty else { return }
        childCoordinators.removeValue(forKey: coordinator.identifier)
    }
    
}

// MARK: - BaseCoordinatableDelegate
protocol BaseCoordinatableDelegate: class {
    func finish(_ coordinator: BaseCoordinator)
}

extension BaseCoordinatableDelegate where Self: BaseCoordinator {
    func finish(_ coordinator: BaseCoordinator) {
        removeChildCoordinator(coordinator)
    }
}
