//
//  BaseViewRoutable.swift
//
//  Created by alobanov11 on 28/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import UIKit

class BaseCoordinator: NSObject {
    
}


// MARK: - BaseCoordinatable
protocol BaseCoordinator: Routable, NSObjectProtocol {
    var childCoordinators: [BaseCoordinator] { get set }
    func start()
}

extension BaseCoordinator {
    
    func addChildCoordinator(_ coordinator: BaseCoordinator) {
        for element in childCoordinators {
            if element === coordinator {
                return
            }
        }
        childCoordinators.append(coordinator)
    }
    
    func removeChildCoordinator(_ coordinator: BaseCoordinator) {
        guard !childCoordinators.isEmpty else { return }
        for (index, element) in childCoordinators.enumerated() {
            if element === coordinator {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
}

// MARK: - BaseCoordinatableDelegate
protocol BaseCoordinatableDelegate: class {
    func finish(_ coordinator: BaseCoordinator)
}

extension BaseCoordinatableDelegate where Self: BaseCoordinator {
    
    func finish(_ coordinator: BaseCoordinator) {
        removeChildCoordinator(coordinator)
    }
    
}
