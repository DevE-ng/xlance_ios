//
//  BaseRouter.swift
//  xlance
//
//  Created by alobanov11 on 29/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

final class BaseRouter: Routable {
    
    var rootModule: Presentable? {
        return navigationController?.viewControllers.first
    }
    
    var modules: [Presentable] {
        return navigationController?.viewControllers ?? []
    }
    
    private weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func dismiss() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func present(_ module: Presentable) {
        present(module, animated: true)
    }
    
    func present(_ module: Presentable, animated: Bool) {
        guard let vc = module.toPresent else { return }
        navigationController?.present(vc, animated: animated, completion: nil)
    }
    
    func push(_ module: Presentable) {
        push(module, animated: true)
    }
    
    func push(_ module: Presentable, animated: Bool) {
        guard let vc = module.toPresent else { return }
        navigationController?.pushViewController(vc, animated: animated)
    }
    
    func pop() {
        navigationController?.popViewController(animated: true)
    }
    
    func popTo(_ module: Presentable) {
        popTo(module, animated: true)
    }
    
    func popTo(_ module: Presentable, animated: Bool) {
        guard let vc = module.toPresent else { return }
        navigationController?.popToViewController(vc, animated: animated)
    }
    
    func popToRoot() {
        popToRoot(animated: true)
    }
    
    func popToRoot(animated: Bool) {
        navigationController?.popToRootViewController(animated: animated)
    }
    
    func setRoot(_ module: Presentable) {
        setRoot(module, animated: true)
    }
    
    func setRoot(_ module: Presentable, animated: Bool) {
        guard let vc = module.toPresent else { return }
        navigationController?.setViewControllers([vc], animated: animated)
    }
    
}
