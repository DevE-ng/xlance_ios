//
//  BaseConfigurable.swift
//
//  Created by alobanov11 on 28/05/2019.
//  Copyright © 2019 alobanov11. All rights reserved.
//

import UIKit

protocol BaseViewConfigurable {
    associatedtype T = UIViewController
    func create() -> T
    func configure(viewController: T)
}
