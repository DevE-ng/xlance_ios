//
//  BaseViewController.swift
//  xlance
//
//  Created by alobanov11 on 14/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol BaseViewProtocol: NSObjectProtocol, Presentable {
    var onBack: CompletionBlock? { get set }
}

extension BaseViewController: BaseViewProtocol { }
extension BaseTableViewController: BaseViewProtocol { }
extension BaseTabBarViewController: BaseViewProtocol { }

class BaseViewController: UIViewController {
    var onBack: CompletionBlock?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            onBack?()
        }
    }
}

class BaseTableViewController: UITableViewController {
    var onBack: CompletionBlock?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            onBack?()
        }
    }
}

class BaseTabBarViewController: UITabBarController {
    var onBack: CompletionBlock?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            onBack?()
        }
    }
}
