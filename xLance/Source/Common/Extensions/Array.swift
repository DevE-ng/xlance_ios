//
//  Array.swift
//  ToDoSwipe
//
//  Created by alobanov11 on 07/08/2019.
//  Copyright © 2019 Anton Lobanov. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    func indexForElement(_ element: Element) -> Int? {
        for (i, e) in self.enumerated() {
            if element == e { return i }
        }
        return nil
    }
    
    func selectedItems(from anotherArray: [Element]) -> [Int] {
        var indicies = [Int]()
        for (index, element) in self.enumerated() {
            if anotherArray.contains(element) {
                indicies.append(index)
            }
        }
        return indicies
    }
    
    func selectedItems(indicies: [Int]) -> [Element] {
        var elements = [Element]()
        for (index, element) in self.enumerated() {
            if indicies.contains(index) {
                elements.append(element)
            }
        }
        return elements
    }
    
}

extension Array {
    
    func lastIndex() -> Int? {
        return self.indices.last
    }
    
    func filteredByType<T>(_: T.Type) -> T? {
        return compactMap({ (element) in
            return element as? T
        }).last
    }
    
    static func commonElements<T: Sequence, U: Sequence>(_ lhs: T, _ rhs: U) -> [T.Iterator.Element]
        where T.Iterator.Element: Equatable, T.Iterator.Element == U.Iterator.Element {
            var common: [T.Iterator.Element] = []
            
            for lhsItem in lhs {
                for rhsItem in rhs {
                    if lhsItem == rhsItem {
                        common.append(lhsItem)
                    }
                }
            }
            return common
    }
    
}
