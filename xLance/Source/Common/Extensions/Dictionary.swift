//
//  Dictionary.swift
//  xlance
//
//  Created by Anton Lobanov on 09/07/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

extension Dictionary {
    
    subscript(i: Int) -> (key: Key, value: Value) {
        return self[index(startIndex, offsetBy: i)]
    }
    
}
