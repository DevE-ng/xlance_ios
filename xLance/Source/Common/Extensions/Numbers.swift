//
//  Double.swift
//  xlance
//
//  Created by Anton Lobanov on 16/07/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

extension Double {
    
    var string: String {
        return "\(self)"
    }
    
}

extension Int {
    
    var string: String {
        return "\(self)"
    }
    
}

extension Decimal {
    
    var string: String {
        return "\(self)"
    }
    
    mutating func round(_ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) {
        var localCopy = self
        NSDecimalRound(&self, &localCopy, scale, roundingMode)
    }
    
    func rounded(_ scale: Int, _ roundingMode: NSDecimalNumber.RoundingMode) -> Decimal {
        var result = Decimal()
        var localCopy = self
        NSDecimalRound(&result, &localCopy, scale, roundingMode)
        return result
    }
    
}
