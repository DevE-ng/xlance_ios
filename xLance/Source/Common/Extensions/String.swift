//
//  String.swift
//  xlance
//
//  Created by alobanov11 on 17/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import UIKit

extension String {
    
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
    
    var decimal: Decimal {
        return Decimal(string: self) ?? 0
    }
    
    var double: Double {
        return Double(self) ?? 0
    }
    
    var int: Int {
        return Int(self) ?? 0
    }
    
    var fileName: String {
        return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
    }
    
    var fileExt: String {
        return NSURL(fileURLWithPath: self).pathExtension ?? ""
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localizedWithCount(_ count: Int) -> String {
        return String(format: NSLocalizedString(self, comment: ""), count)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont, minimumTextWrapWidth:CGFloat) -> CGFloat {
        
        var textWidth:CGFloat = minimumTextWrapWidth
        let incrementWidth:CGFloat = minimumTextWrapWidth * 0.1
        var textHeight:CGFloat = self.height(withConstrainedWidth: textWidth, font: font)
        
        //Increase width by 10% of minimumTextWrapWidth until minimum width found that makes the text fit within the specified height
        while textHeight > height {
            textWidth += incrementWidth
            textHeight = self.height(withConstrainedWidth: textWidth, font: font)
        }
        
        return ceil(textWidth)
    }
    
}
