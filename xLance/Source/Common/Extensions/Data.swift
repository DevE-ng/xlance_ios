//
//  Data.swift
//  xlance
//
//  Created by Anton Lobanov on 19/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

extension Data {
    
    func convertToDictionary() -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
}
