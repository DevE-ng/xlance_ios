//
//  UIViewController+Create.swift
//  Odeon
//
//  Created by Sherlock, James on 24/12/2018.
//  Copyright © 2018 Sherlouk. All rights reserved.
//

import UIKit

import SwiftMessages

protocol StoryboardLoadable {
    static var storyboardName: Storyboards { get }
    static var storyboardId: String? { get }
}

extension StoryboardLoadable where Self: UIViewController {
    
    static var storyboardId: String? {
        return nil
    }
    
    static func create() -> Self {
        
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        
        if let identifier = storyboardId {
            guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? Self else {
                fatalError("Failed to load or cast view controller with identifier: \(identifier)")
            }
            
            return viewController
        }
        
        guard let viewController = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("Failed to load or cast initial view controller in storyboard: \(storyboardName)")
        }
        
        return viewController
        
    }
    
}

extension UIViewController {
    
    /// add tap gesture recognizer on view - endEditing(true)
    func addTapGestureWithEndEditing() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler(_:)))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc
    fileprivate func tapGestureHandler(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func setStatusBarColors(textColor: UIColor, backgroundColor: UIColor) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = backgroundColor
            statusBar.setValue(textColor, forKey: "foregroundColor")
        }
    }
    
}

public extension UIViewController
{
    func startAvoidingKeyboard()
    {
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(onKeyboardFrameWillChangeNotificationReceived(_:)),
                         name: UIResponder.keyboardWillChangeFrameNotification,
                         object: nil)
    }
    
    func stopAvoidingKeyboard()
    {
        NotificationCenter.default
            .removeObserver(self,
                            name: UIResponder.keyboardWillChangeFrameNotification,
                            object: nil)
    }
    
    @objc
    private func onKeyboardFrameWillChangeNotificationReceived(_ notification: Notification)
    {
        guard
            let userInfo = notification.userInfo,
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        
        let keyboardFrameInView = view.convert(keyboardFrame, from: nil)
        let safeAreaFrame = view.safeAreaLayoutGuide.layoutFrame.insetBy(dx: 0, dy: -additionalSafeAreaInsets.bottom)
        let intersection = safeAreaFrame.intersection(keyboardFrameInView)
        
        let keyboardAnimationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]
        let animationDuration: TimeInterval = (keyboardAnimationDuration as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        UIView.animate(withDuration: animationDuration,
                       delay: 0,
                       options: animationCurve,
                       animations: {
                        self.additionalSafeAreaInsets.bottom = intersection.height
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
