//
//  UINavigationController.swift
//  xlance
//
//  Created by Anton Lobanov on 16/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func findVC<T: UIViewController>(_ type: T.Type) -> T? {
        return viewControllers.filteredByType(type)
    }
    
}
