//
//  UILabel.swift
//  xlance
//
//  Created by Anton Lobanov on 03/09/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

extension UILabel {
    
    static func setTextColor(_ labels: [UILabel], color: UIColor) {
        let _ = labels.map({ $0.textColor = color })
    }
    
}
