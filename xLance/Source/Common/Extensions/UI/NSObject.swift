//
//  ClassNameProtocol.swift
//  xlance
//
//  Created by alobanov11 on 17/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import Foundation

protocol ClassNameProtocol {
    static var className: String { get }
    var className: String { get }
}

extension ClassNameProtocol {
    static var className: String {
        return String(describing: self)
    }
    
    var className: String {
        return type(of: self).className
    }
}

extension NSObject: ClassNameProtocol {}
