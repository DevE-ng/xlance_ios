//
//  Modifiable.swift
//  xlance
//
//  Created by alobanov11 on 15/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol ModifiableView {
    var savedDefaultColor: UIColor? { get set }
    func updateModifyStatus<T: Comparable>(old: T, new: T)
    func updateModifyStatus(_ status: ModifyStatus)
    func setModifyColor(_ color: UIColor)
}

extension ModifiableView {
    
    func updateModifyStatus<T: Comparable>(old: T, new: T) {
        switch true {
        case old > new: updateModifyStatus(.decreased)
        case old < new: updateModifyStatus(.increased)
        default: updateModifyStatus(.none)
        }
    }
    
    func updateModifyStatus(_ status: ModifyStatus) {
        switch status {
        case .created, .increased: setModifyColor(Theme.successColor)
        case .deleted, .decreased: setModifyColor(Theme.dangerColor)
        case .edited, .moved: setModifyColor(Theme.warningColor)
        default: setModifyColor(savedDefaultColor ?? .black)
        }
    }
    
}
