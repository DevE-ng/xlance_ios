//
//  Networking.swift
//  xlance
//
//  Created by Anton Lobanov on 09/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation
import ObjectMapper

protocol Networking: class {
    
    var token: String? { get set }
    
    func login(name: String, password: String, completionHandler: @escaping (Result<LoginResponse>) -> Void)
    func logout(completionHandler: @escaping (Result<Bool>) -> Void)
    func forgot(email: String, completionHandler: @escaping (Result<Bool>) -> Void)
    func register(data: AuthModel, completionHandler: @escaping (Result<Bool>) -> Void)
    func changePassword(id: Int, old: String, new: String, confirm: String, completionHandler: @escaping (Result<Bool>) -> Void)
    
    func fetchProjects(index: Int, size: Int, completionHandler: @escaping (Result<ProjectsResponse>) -> Void)
    func fetchFreelancersProjects(completionHandler: @escaping (Result<[ProjectModel]>) -> Void)
    func fetchProject(_ id: Int, completionHandler: @escaping (Result<ProjectModel>) -> Void)
    
    func fetchSuggestion(_ suggestionId: Int, completionHandler: @escaping (Result<SuggestionModel>) -> Void)
    func createSuggestion(_ suggestion: SuggestionModel, completionHandler: @escaping (Result<SuggestionModel>) -> Void)
    func editSuggestion(_ suggestion: SuggestionModel, completionHandler: @escaping (Result<SuggestionModel>) -> Void)
    func acceptSuggestion(_ suggestion: SuggestionModel, completionHandler: @escaping (Result<ProjectModel>) -> Void)
    func declineSuggestion(_ suggestionId: Int, completionHandler: @escaping (Result<Bool>) -> Void)
    
    func fetchProposals(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void)
    func fetchOffers(completionHandler: @escaping (Result<[SuggestionModel]>) -> Void)
    
    func fetchStatuses(profiles: [ProfileModel], completionHandler: @escaping (Result<[StatusModel]>) -> Void)
    
    func fetchMessages(conversationId: Int, completionHandler: @escaping (Result<[MessageModel]>) -> Void)
    func fetchCategories(completionHandler: @escaping (Result<[CategoryModel]>) -> Void)
    
}
