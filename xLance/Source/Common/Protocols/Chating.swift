//
//  Chating.swift
//  xlance
//
//  Created by alobanov11 on 26/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import Foundation

protocol ChattingDelegate: class {
    func didReceiveMessage(_ message: MessageSimpleModel)
}

protocol Chating {
    var profile: ProfileModel? { get set }
    func subscribeOn(project: ProjectModel, delegate: ChattingDelegate)
    func subscribeOff(project: ProjectModel)
    func sendMessage(text: String, project: ProjectModel)
}
