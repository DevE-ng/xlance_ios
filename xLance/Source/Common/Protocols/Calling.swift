//
//  OpenVidu.swift
//  xlance
//
//  Created by alobanov11 on 13/06/2019.
//  Copyright © 2019 Engineers. All rights reserved.
//

import UIKit

protocol CallingDelegate: class {
    func addLocalView(view: UIView)
    func localVideoSize() -> CGRect
    func countRemoteViews() -> Int
    func createRemoteView() -> (index: Int, view: UIView)
    func disconnect()
}

protocol Calling: class {
    var token: String? { get set }
    var delegate: CallingDelegate? { get set }
    
    func call(user: String, completionHandler: @escaping (Result<Bool>) -> Void)
    func finishCall()
    
    func toggleVideo() -> Bool?
    func toggleAudio() -> Bool?
    func toggleSound() -> Bool?
}
