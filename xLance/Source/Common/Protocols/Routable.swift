//
//  Routable.swift
//  xlance
//
//  Created by alobanov11 on 22/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit

protocol Routable {
    
    var rootModule: Presentable? { get }
    var modules: [Presentable] { get }
    
    func dismiss()
    
    func present(_ module: Presentable)
    func present(_ module: Presentable, animated: Bool)
    
    func push(_ module: Presentable)
    func push(_ module: Presentable, animated: Bool)
    
    func pop()
    
    func popTo(_ module: Presentable)
    func popTo(_ module: Presentable, animated: Bool)
    
    func popToRoot()
    func popToRoot(animated: Bool)
    
    func setRoot(_ module: Presentable)
    func setRoot(_ module: Presentable, animated: Bool)
    
}
