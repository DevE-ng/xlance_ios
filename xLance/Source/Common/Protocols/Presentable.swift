//
//  Presentable.swift
//  xlance
//
//  Created by alobanov11 on 22/08/2019.
//  Copyright © 2019 E-ngineers. All rights reserved.
//

import UIKit
import SwiftMessages
import NVActivityIndicatorView

protocol Presentable: class {
    var toPresent: UIViewController? { get }
    func startLoading()
    func stopLoading()
    func showInfoMessage(_ text: String)
    func showErrorMessage(_ text: String)
}

extension UIViewController: NVActivityIndicatorViewable {}
extension UIViewController: Presentable {
    
    var toPresent: UIViewController? {
        return self
    }
    
    func startLoading() {
        let size: CGSize = .init(width: 40, height: 40)
        let font: UIFont = .systemFont(ofSize: 10, weight: .light)
        let type: NVActivityIndicatorType = .lineScale
        let color: UIColor = Theme.successColor
        let backgroundColor: UIColor = .clear
        DispatchQueue.main.async {
            self.startAnimating(size,
                                messageFont: font,
                                type: type,
                                color: color,
                                backgroundColor: backgroundColor)
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
    
    func showInfoMessage(_ text: String) {
        showMessage(text, background: Theme.warningColor, color: .white)
    }
    
    func showErrorMessage(_ text: String) {
        showMessage(text, background: Theme.dangerColor, color: .white)
    }
    
    private func showMessage(_ text: String, background: UIColor, color: UIColor) {
        DispatchQueue.main.async {
            let status = MessageView.viewFromNib(layout: .statusLine)
            status.backgroundView.backgroundColor = background
            status.bodyLabel?.textColor = color
            status.configureContent(body: text)
            
            var statusConfig = SwiftMessages.defaultConfig
            statusConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            
            SwiftMessages.show(config: statusConfig, view: status)
        }
    }
    
}
