//
//  AppDelegate.swift
//  ToDoSwipe
//
//  Created by alobanov11 on 16/07/2019.
//  Copyright © 2019 Anton Lobanov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var loginNotificationToken: NotificationToken?
    var logoutNotificationToken: NotificationToken?
    
    lazy var rootViewController: UINavigationController = {
        let vc = UINavigationController()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        return vc
    }()
    
    lazy var appCoordinator: BaseCoordinator = {
        return AppCoordinator(factory: makeModulesFactory(),
                              router: BaseRouter(navigationController: rootViewController))
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        Theme.applyAppearanceStyles()
        appCoordinator.start()

        return true
    }


}

// MARK: - Private
fileprivate extension AppDelegate {
    
    func makeModulesFactory() -> AppCoordinatorFactory {
        let networkProvider = NetworkService()
        let chatProvider = ChatService()
        let callProvider = CallService()
        
        let factory = ModulesFactory(networkProvider: networkProvider,
                                     chatProvider: chatProvider,
                                     callProvider: callProvider)
        
        self.loginNotificationToken = NotificationCenterWrapper.subscribeLogin { [weak factory] in
            factory?.networkProvider.token = CurrentUser.token
            factory?.callProvider.token = CurrentUser.token
            factory?.chatProvider.profile = CurrentUser.profile
        }
        
        self.logoutNotificationToken = NotificationCenterWrapper.subscribeLogout { [weak factory] in
            factory?.networkProvider.token = nil
            factory?.callProvider.token = nil
            factory?.chatProvider.profile = nil
        }
        
        return factory
    }
    
}
